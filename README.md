## HIGHSCOPE-FE

### Site URL
- dev: [dev.highscope.gredu.co](https://dev.highscope.gredu.co/)
- qa: [qa.highscope.gredu.co](https://qa.highscope.gredu.co/)
- uat: [uat.highscope.gredu.co](https://uat.highscope.gredu.co/)

#### API Base_urls
- dev: dev.api.highscope.gredu.co
- qa: qa.api.highscope.gredu.co
- uat: uat.api.highscope.gredu.co
- prod: api.highscope.gredu.co