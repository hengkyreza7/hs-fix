import React, { Component } from 'react'
import classnames from 'classnames';
import { Select } from 'antd';

const style = require('./../../styles/profile/profile.css');
export class Search extends Component {
    render() {
        return (
            <div class="row">
                <div class="col-sm-12">
                    {/* <div class={classnames(style.admission_border, style.admission_border_applicant)}> */}
                        <div class="row">
                            <div class="col-sm-5">
                                <label className={classnames(style.label_header, style.label_header_content)}> Finish Applicant List</label>
                            </div>
                            <div class="col-sm-2">
                                <Select
                                    showSearch
                                    style={{ width: 150, marginRight: 20 }}
                                    placeholder="Result Date"
                                >
                                </Select>
                            </div>
                            <div class="col-sm-2">
                                <Select
                                    showSearch
                                    style={{ width: 150, marginRight: 20 }}
                                    placeholder="School Program"
                                >
                                </Select>
                            </div>
                            <div class="col-sm-2">
                                <Select
                                    showSearch
                                    style={{ width: 150, marginRight: 20 }}
                                    placeholder="Grade"
                                >
                                </Select>
                            </div>
                        </div>
                    {/* </div> */}
                </div>
            </div>
        )
    }
}
