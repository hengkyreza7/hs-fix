import React, { Component } from 'react'
import classnames from 'classnames';
const style = require('./../../styles/profile/profile.css');

export class AssessmentItem extends Component {
    render(){
        let color = ''
        let list = ''
        let warpper = ''

        if(this.props.color === "result"){
            color = style.background_result
        } else if(this.props.color === "interview"){
            color = style.background_interview
        } else if(this.props.color === "parent"){
            color = style.background_parent
        }

        if(this.props.values.length > 0){
            let length  = this.props.values.length
            let div = Math.floor((9/length))
            warpper  = "col-sm-"+div

            list = this.props.values.map(function (arr, i) {
                let label = arr['label'] || 'N/A'
                let value = arr['value'] || 'N/A'

                return (
                    <div class={warpper}>
                    <div className={style.admission_box_item}>
                   <div className={classnames(color, style.admission_box_title)}>
                           <span>{label}</span>
                       </div>
                       <div className={style.admission_box_title}>
                           <h4>{value}</h4>
                       </div>
                   </div>
                 </div>
                )
            })
        }
        return (
            <div>
                {list}
            </div>
        )
    }
}