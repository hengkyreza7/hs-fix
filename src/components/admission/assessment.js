import React, { Component } from 'react'
import { AssessmentItem } from '../../components/admission/assessment_item';

const style = require('./../../styles/profile/profile.css');

export class Assessment extends Component {
    render(){
        return (
            <div className={style.margin_4}>
                <div class="row">
                    <div class="col-sm-9">
                        <div className={style.assessment_border_title}>
                            <label>{this.props.title}</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <AssessmentItem 
                        color = {this.props.color} 
                        values = {this.props.values}
                    />
                </div>
            </div>
        )
    }
}