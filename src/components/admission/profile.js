import React, { Component } from 'react'
import classnames from 'classnames';

const style = require('./../../styles/profile/profile.css');

export class Profile extends Component {
    render(){
        return(
            <div className={style.margin_4}>
                <label>Result Date</label><span class={style.margin_left_4}>05 June 2018</span>
                <div className={style.border_item_list}>
                    <div className={style.margin_4}>
                        <div class="row">
                            <div class="col-sm-3">
                                <img className={classnames(style.pict_art, style.margin_left_2, style.margin_right_2, style.margin_bottom_2)} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2vSEyoHgQhQf9tAdQuig_F-gPNpUaKmW019kfKs2x3wCnzhe0" alt="" />
                            </div>
                            <div class="col-sm-4">
                                <label>Mario Noya</label>
                                <div>
                                    <span className={style.admission_profile}>Early Childhood Education Program</span>
                                </div>
                                <div>
                                    <span className={style.admission_profile}>Pre-Kindergarten</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label>Result Status</label>
                                <div>
                                    <span className={style.admission_profile}>Assessment Test</span>
                                </div>
                                <div>
                                    <span className={style.admission_profile}>Interview Test</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}