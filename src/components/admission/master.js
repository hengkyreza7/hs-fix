import React, { Component } from 'react';
import { Tabs, Input, Select, DatePicker } from 'antd';

const TabPane = Tabs.TabPane;
const Search = Input.Search;
const Option = Select.Option;
const { RangePicker } = DatePicker;

export default class transaction extends Component {
  render(){
    console.log(this.props.data);
    return false;

    return (
      <div>
        <TabPane tab="School Billing" key="1">
          <div>
            <Search
              placeholder="Find your transaction"
              onSearch={value => this.findOne(value)}
              style={{ width: 320, marginRight: 20, height: 48, marginTop: '0.5em' }}
            />

            <Select
              showSearch
              style={{ width: 200, marginRight: 20 }}
              placeholder="Sort by title "
              optionFilterProp="children"
              onChange={this.handleChange}
              onFocus={this.handleFocus}
              onBlur={this.handleBlur}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {this.state.subject.map(function (arr, i) {
                return <Option value={arr.value}>{arr.label}</Option>
              })
              }
            </Select>

            <Select
              showSearch
              style={{ width: 200, marginRight: 20 }}
              placeholder="Import from "
              optionFilterProp="children"
              onChange={this.handleChange}
              onFocus={this.handleFocus}
              onBlur={this.handleBlur}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {this.state.subject.map(function (arr, i) {
                return <Option value={arr.value}>{arr.label}</Option>
              })
              }
            </Select>
            <RangePicker onChange={this.onChangeDate} style={{ width: 'em' }} />

          </div>

          <table className="table table-bordered" style={margin}>
            <thead className="thead-dark" style={color}>
              <tr>
                <th className={style.fontTable} scope="col">No</th>
                <th className={style.fontTable} scope="col">Invoice Code</th>
                <th className={style.fontTable} scope="col">Payment Title</th>
                <th className={style.fontTable} scope="col">Participant</th>
                <th className={style.fontTable} scope="col">Import From</th>
                <th className={style.fontTable} scope="col">Fee</th>
                <th className={style.fontTable} scope="col">Due Date</th>
                <th className={style.fontTable} >Create On</th>
              </tr>
            </thead>
            <tbody>
              {
                this.state.data.payment_batchs.map(function (array, i) {
                  return <tr key={i}>
                    <td className={style.valueTable} >{i + 1}</td>
                    <td className={style.popCol} onClick={(e) => { this.toggleModal(array.id, e) }}><a> {array.batch_code} </a></td>

                    {
                      array.stoped !== 1 ? <td className={style.valueTable} >{array.payment_title}</td> :
                        <td className={style.valueTable} >{array.payment_title} <img src={Badge} alt="Badge" /></td>
                    }

                    <td className={style.valueTable} >{array.participant_count}</td>
                    <td className={style.valueTable} >{array.import_type}</td>
                    <td className={style.valueTable} >{this.formatPrice(array.gross_fee)}</td>
                    <td className={style.valueTable} >{dateFormat(array.due_date, "isoDate")}</td>
                    <td className={style.valueTable} >{array.created_at === null || array.created_at === '' ? 'N/A' : dateFormat(array.created_at, "isoDate")}</td>
                  </tr>
                }, this)
              }

            </tbody>
          </table>
          <div className={style.page}>
            <Pagination
              margin={2}
              page={page}
              count={Math.ceil(this.state.total_page)}
              onPageChange={this.handlePageChange}
            />
          </div>
        </TabPane>
      </div>
    )
  }
}