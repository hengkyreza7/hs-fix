import React, { Component } from 'react'
import classnames from 'classnames';
import { Link } from 'react-router-dom';

const style = require('./../../styles/profile/profile.css');

export class ItemList extends Component {
    render() {
        return (
            <div className={style.border_item_list}>
                <div className={classnames(style.margin_top_2, style.margin_left_2, style.margin_right_2, style.margin_bottom_2)}>
                    <div class="row">
                        <div class="col-sm-2">
                                <img className={classnames(style.pict_art, style.margin_left_2, style.margin_right_2, style.margin_bottom_2)} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2vSEyoHgQhQf9tAdQuig_F-gPNpUaKmW019kfKs2x3wCnzhe0" alt="" />
                            
                        </div>
                        <div class="col-sm-3">
                            <label>Mario Noya</label>
                            <div>
                                <span className={style.admission_profile}>Early Childhood Education Program</span>
                            </div>
                            <div>
                                <span className={style.admission_profile}>Pre-Kindergarten</span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>Result Status</label>
                            <div>
                                <span className={style.admission_profile}>Assessment Test</span>
                            </div>
                            <div>
                                <span className={style.admission_profile}>Interview Test</span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>Result Date</label>
                            <div>
                                <span className={style.admission_profile}>05 June 2018</span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div className={classnames(style.text_align_center, style.margin_top_4)}>
                                <Link to="/detail-admission" className={style.create_payment}>Create Payment</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}