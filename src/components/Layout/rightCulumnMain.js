import React, { Component } from 'react'
import style from './../../styles/dashboard/dashboard.css';

export default class componentName extends Component {
  render() {
    return (
      <div>
        <div className={style.baseBoxLeftUp} >
                <p className={style.txt}>Favorite Books</p>
                <div className={style.favBookCol}>
                    <p className={style.textFavBook}> 1.Pragmatik</p>
                </div>
                <div className={style.favBookCol}>
                     <p className={style.textFavBook}> 2. Alam dan Marga Satwa Amerika Utara</p>
                </div>
                <div className={style.favBookCol}>
                    <p className={style.textFavBook}> 3.Bayi Bayi Bersayap</p>
                </div>
                <div className={style.favBookCol}>
                     <p className={style.textFavBook}> 4. Matematika Dasar 5</p>
                </div>
                 <div className={style.favBookCol}>
                     <p className={style.textFavBook}> 5. RPUL</p>
                </div>
            </div>
            <div className={style.baseBoxLeftBottom} >
                <p className={style.txt}>Favorite Activities</p>
                <div className={style.favBookCol}>
                    <p className={style.textFavBook}> 1.Pragmatik</p>
                </div>
                <div className={style.favBookCol}>
                     <p className={style.textFavBook}> 2. Alam dan Marga Satwa Amerika Utara</p>
                </div>
                <div className={style.favBookCol}>
                    <p className={style.textFavBook}> 3.Bayi Bayi Bersayap</p>
                </div>
            </div>
      </div>
    )
  }
}
