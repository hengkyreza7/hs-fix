import React, { Component } from 'react'
import style from './../../styles/dashboard/dashboard.css';
import Select from 'react-select';
import { Input, Icon } from 'antd';





export default class componentName extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedOption: null,
            subject: [
                { value: '000', label: 'Computer science, information & general works' },
                { value: '100', label: 'Philosophy & psychology' },
                { value: '200', label: 'Religion' },
                { value: '300', label: 'Social sciences' },
                { value: '400', label: 'Language' },
                { value: '500', label: 'Science' },
                { value: '600', label: 'Technology' },
                { value: '700', label: 'Arts & recreation' },
                { value: '800', label: "Literature" },
                { value: '900', label: "History & geography" }
            ],
        }
    }
    render() {
        console.log(this.props.data)
        return (
            <div>
                <div className={style.baseBoxLeftUp} >
                    <div className={style.category} >
                        <p>category</p>
                        <Select
                            isClearable
                            className={style.customSelect}
                            value={this.props.data.select}
                            onChange={this.props.onChange}
                            options={this.state.subject}
                        />
                    </div>
                    <div className={style.category} >
                        <p>Find Item</p>
                        {/* <FormGroup className={style.sizeForm}>
                            <InputGroup>
                                <InputGroup.Button>
                                    <FormControl type="text" className={style.findButton} />
                                    <button  style={button} onClick={this.props.onSubmit}><img src={Arrow} alt="34" height="25px"  /></button>
                                </InputGroup.Button>
                            </InputGroup>
                        </FormGroup> */}
                        <div style={{ marginBottom: 16, width:290 }}>
                            <Input   value={this.props.data.text} onChange={this.props.onChangeForm} addonAfter={<Icon type="right" style={button}  onClick={this.props.onSubmit} />} />
                        </div>
                    </div>
                    {/* <div  className={style.category} >
                <p>Availaible</p>
                <Select
                    className={style.customSelect}
                    value={selectedOption}
                    onChange={this.handleChange}
                    options={options}
                />
            </div> */}
                    {/* <div className={style.category}>
               <input type="submit" className={style.btn} onClick={(e) => {this.createAction(e)}} value="Export Report"  /> 
             </div> */}
                </div>
                {/* <div className={style.baseBoxLeftBottom} >
                    <p className={style.txt}>Find Item</p>
                    <FormGroup className={style.sizeForm}>
                        <InputGroup>
                            <InputGroup.Button>
                                <FormControl type="text" className={style.findButton} />
                                <Button className={style.sizeBtn}><img src={Arrow} alt="34" height="25px" className={style.arrow} /></Button>
                            </InputGroup.Button>
                        </InputGroup>
                    </FormGroup>


                </div> */}
            </div>
        )
    }
}

const button = {
    cursor: 'pointer'
}
