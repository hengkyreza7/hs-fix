import React from 'react';
import PropTypes from 'prop-types';
// import style from './../../styles/scss/register.css';  
import Modal from 'react-awesome-modal';

class Modals extends React.Component {
  render() {
    // Render nothing if the "show" prop is false
    if (!this.props.show) {
      return null;
    }

    // The gray background
    const backdropStyles = {
      position: 'absolute',
      top: 0,
      bottom: 300,
      left: 0,
      right: 0,
      height: '100%',
      minHeight: 'auto',
      backgroundColor: 'rgba(0,0,0,0.3)',
      padding: 50
    };

    // The modal "window"
    const modalStyle = {
      backgroundColor: '#fff',
      boxShadow: '0 0 70 rgba(89, 89, 89, 0.5)',
      borderRadius: 10,
      position: 'absolute',
      top: '-50%',
      bottom: 300,
      left: '-17%',
      right: 0,
      padding: 50,
      width:'150%',
      maxWidth: 613,
      minHeight: 456,
      display: 'block',
      margin: '0 auto',
    };



    return (

      <Modal visible={this.props.show} width="400" height="300" effect="fadeInUp" onClickAway={() => this.props.onClose()}>
        <div className="backdrop" style={backdropStyles}  >
          <div className="modals" style={modalStyle}>
            {this.props.children}

           
          </div>
        </div>
      </Modal>
    );
  }
}

Modals.propTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool,
  onClickAway: PropTypes.func.isRequired,
  children: PropTypes.node
};

export default Modals;
