import React from 'react';
import PropTypes from 'prop-types';

class Modal extends React.Component {
  render() {
    // Render nothing if the "show" prop is false
    if(!this.props.show) {
        console.log(!this.props.show,"show")
        return null;
    }
    // The gray background
    const backdropStyles = {
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'rgba(0,0,0,0.3)',
      padding: 50
    };

    // The modal "window"
    const modalStyle = {
      backgroundColor: '#fff',
      boxShadow: '0 0 70 rgba(89, 89, 89, 0.5)',
      borderRadius: 10,
      position: 'absolute',
      top: 80,
      bottom: 300,
      left: 0,
      right: 0,
      padding: 50,
      maxWidth: 613,
      minHeight: 200,
      height: 300,
      margin: '0 auto',
    };

    return (
      <div className="backdrop" style={backdropStyles}>
        <div className="modals" style={modalStyle}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.node
};

export default Modal;
