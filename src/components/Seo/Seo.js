import React, { Component } from 'react';
import Helmet from 'react-helmet';

export default class Seo extends Component {


    generateTitle() {
        if(this.props.title === 'librarians'){
            const title = (
                <title>E-Library</title>
            )
            return title
        }else if(this.props.title === 'login'){
            const title = (
                <title>Login</title>
            )
            return title
        }else if(this.props.title === 'register'){
            const title = (
                <title>Registration Validation</title>
            )
            return title
        }else if(this.props.title === 'payment'){
            const title = (
                <title>Payment Management</title>
            )
            return title
        }
        
    }

    render() {
        return (
            <Helmet>
               {this.generateTitle()}
            </Helmet>
        )
    }
}