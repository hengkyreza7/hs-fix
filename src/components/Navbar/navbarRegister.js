export  class navbarRegister extends Component {

    render(){
        return (
            <Grid
            width="100%"
            height="100vh"
            templateColumns="repeat(2, 1fr)"
            autoRows="minmax(100px, auto)"
        >
            <GridItem  column="1">
                <LeftCol >
                    <div>
                        {title}
                    </div>

                </LeftCol>
            </GridItem>
            <GridItem column="2">
                <RightCol>
                    <img src={logo} alt="logo" />
                    <form>
                        <div>
                            <input type="text" value={userid} onChange={this.onChangeForm} required="required" />
                            <span className={style.highlight}></span><p></p>
                            <label>User Id</label>
                        </div>
                        <div>
                        <input type="date"  value={dob} data-date="datetime-local" data-date-format="DD MMMM YYYY" onChange={this.handleChange} className={style.input}/>
                            <span className={style.highlight}></span><p></p>
                            <label>Date of Birth</label>
                        </div>
                        <footer>
                            <button onClick={(e) => { this.crateAction(e) }}>Register </button>
                            <span>Have a Account?<Link to="/login">Login</Link></span>
                        </footer>
                    </form>
                </RightCol>
            </GridItem>
        </Grid>
        )
    }

}