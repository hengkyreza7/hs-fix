import React, { Component } from 'react'
import Logo from './../../styles/assets/HighScope.svg';
import { NavLink } from 'react-router-dom';
import PP from './../../styles/assets/user.svg';
import { Link } from 'react-router-dom';
import { Navbar } from './../../styles/navbar';
import Logout from './../../styles/assets/shutdown.png'
import style from './index.css'


export class NavbarMain extends Component {
    constructor(props) {
        super(props)
        this.state = {
            active: { mainPage: "Main Page", bookList: "" },
            width: window.innerWidth,
            menus: {
                mainPage: "",
                bookList: "",
                addBook: "",
                bookMng: ""
            }
        }
        this._handleClick = this._handleClick.bind(this);
        this._removeAllStore = this._removeAllStore.bind(this);
    }


    activeRoute(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? 'active' : '';
    }

    updateDimensions() {
        this.setState({ width: window.innerWidth });
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
        // console.log(this.props)

    }

    _handleClick(menuItem) {
        this.setState({ active: menuItem });
    }

    _removeAllStore() {
        localStorage.clear()
    }

    render() {
        const logo = {
            marginTop: "1%",
            marginLeft: "2%"
        }



        return (
            // <div>
            <Navbar>
                <img src={Logo} alt="logo" width="224px" height="70px" style={logo} />
                <div>
                    <img src={PP} alt="logo" width="34px" height="34px" />
                </div>
                <p> Inventory Management</p>
                <h5>
                    {localStorage.getItem("_username")}
                </h5>
                <div>
                    <Link to="/login"> <input type="image" alt="png" src={Logout} onClick={this._removeAllStore} /> </Link>
                </div>
                <div>
                    <div>
                        <header>
                            <NavLink
                                exact={true}
                                className={style.menus}
                                activeClassName={style.active}
                                to='/inventory-management'>Inventory Management
                                </NavLink>
                            <NavLink
                                exact={true}
                                className={style.menus}
                                activeClassName={style.active}
                                to='/book-list'>Inventory List
                                </NavLink>
                            <NavLink
                                exact={true}
                                className={style.menus}
                                activeClassName={style.active}
                                to='/add-item'>Add Inventory
                                </NavLink>
                        </header>
                    </div>
                </div>
            </Navbar>
            // </div>
        )
    }
}




export class NavbarRegister extends Component {

    render() {
        const logo = {
            marginTop: "1%",
            marginLeft: "2%"
        }

        return (
            <Navbar>
                <img src={Logo} alt="logo" width="224px" height="70px" style={logo} />

            </Navbar>
        )
    }

}


export class NavbarMainPayment extends Component {
    constructor(props) {
        super(props)
        this.state = {
            active: { mainPage: "Main Page", bookList: "" },
            width: window.innerWidth,
            menus: {
                mainPage: "",
                bookList: "",
                addBook: "",
                bookMng: ""
            }
        }
        this._handleClick = this._handleClick.bind(this);
        this._removeAllStore = this._removeAllStore.bind(this);
    }


    activeRoute(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? 'active' : '';
    }

    updateDimensions() {
        this.setState({ width: window.innerWidth });
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
        // console.log(this.props)

    }

    _handleClick(menuItem) {
        this.setState({ active: menuItem });
    }

    _removeAllStore() {
        localStorage.clear()
    }

    render() {
        const logo = {
            marginTop: "0.5%",
            marginLeft: "2%"
        }
        
        const margin = {
            marginLeft: '0%'
        }




        return (
            // <div>
            <Navbar>
                <img src={Logo} alt="logo" width="224px" height="70px" style={logo} />
                <div>
                    <img src={PP} alt="logo" width="34px" height="34px" />
                </div>
                <p> Payment Management</p>
                <h5>
                    {localStorage.getItem("_username")}
                </h5>
                <div>
                    <Link to="/login"> <input type="image" alt="png" src={Logout} onClick={this._removeAllStore} /> </Link>
                </div>
                <div>
                    <div>
                        <header style={margin} >
                            <NavLink
                                exact={true}
                                className={style.menus}
                                activeClassName={style.active}
                                to='/payment-transaction/1'>Transaction Management
                                </NavLink>
                            {/* <NavLink
                                exact={true}
                                className={style.menus}
                                activeClassName={style.active}
                                to='/admission'>Admission Management
                                </NavLink> */}
                            <NavLink
                                exact={true}
                                className={style.menus}
                                activeClassName={style.active}
                                to='/refund-transaction'>Refund Management
                                </NavLink>
                            <NavLink
                                exact={true}
                                className={style.menus}
                                activeClassName={style.active}
                                to='/registration'>Registration
                                </NavLink>
                        </header>
                    </div>
                </div>
            </Navbar>
            // </div>
        )
    }
}

