/* eslint-disable */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App/App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import { Link } from 'react-router-dom';

import ApiClient from './utils/ApiClient'
import configureStore from './redux-modules/create'
import 'react-table/react-table.css'
import './styles/global/react-table.css';
import './styles/global/css/bootstrap.min.css';
import './styles/global/colorGuide.css';
import './styles/global/payment.css';


// import './../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
// require('./../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css');
// import { setDefaultTranslations, setDefaultLanguage } from 'react-multi-lang'
// import en from './translation/en.json'
// setDefaultTranslations({en})
// setDefaultLanguage('en')

// Let the reducers handle initial state
var initialState = "E-Library"
initialState = window.DATA;
const client = new ApiClient();
const store = configureStore(client, initialState);
ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App initialData={window.DATA} />
    </BrowserRouter>
  </Provider>, document.getElementById('root')
);

registerServiceWorker();
