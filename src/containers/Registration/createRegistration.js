import React, { Component } from 'react';
import Swal from './../../components/Modal/swal';
import axios from 'axios';
import { apiPaymentHost } from './../../utils/constant';
import Check from './../../styles/assets/check.png';
import { NavbarMainPayment } from '../../components/Navbar/navbar';
import Select from 'react-select';
import { Card, CardHeader, CardBody } from "react-simple-card";
import { Flex } from './../../styles/pay';
import Cleave from 'cleave.js/react';
import { Radio, Input, Checkbox, Button,DatePicker } from 'antd';
const RadioGroup = Radio.Group;
const Search = Input.Search;



// var dateFormat = require('dateformat');

// import Dropzone from 'react-dropzone';
// import { Link } from 'react-router-dom';

// import DatePicker from 'react-date-picker';

const style = require('./../../styles/profile/profile.css');
const defaultCheckedList = ['Apple', 'Orange'];
const plainOptions = ['Apple', 'Pear', 'Orange'];


class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false,
            swal: false,
            files: [],
            id: this.props.match.params.id,
            payment_tp: "",
            isShow: false,
            payment_title: [],
            edu_list: [],
            level: [],
            student_name: [],
            isCheck: true,
            show: false,
            amount_fee: 0,
            halte: [],
            value: 0,
            user: [],
            nis: '',
            collect: [],
            name: '',
            isShows:false,
            checkedList: defaultCheckedList,
            indeterminate: true,
            checkAll: false,
            search: false,
            dueDate:'',
            subject: [
                { value: '000', label: 'Computer science, information & general works' },
                { value: '100', label: 'Philosophy & psychology' },
                { value: '200', label: 'Religion' },
                { value: '300', label: 'Social sciences' },
                { value: '400', label: 'Language' },
                { value: '500', label: 'Science' },
                { value: '600', label: 'Technology' },
                { value: '700', label: 'Arts & recreation' },
                { value: '800', label: "Literature" },
                { value: '900', label: "History & geography" }
            ],
            payment_type: [
                { value: 'school_bus', label: 'School Bus' },
                { value: 'edu_trip', label: 'Edu Trip' },
                { value: 'lunch', label: 'Lunch' },
                { value: 'other', label: 'Other' },
            ],
            student_from: [
                { value: 1, label: 'Import From Grade' },
                { value: 2, label: 'Specific student name/ID' },
                { value: 3, label: 'All Student' },
            ],

            type: {
                grade: '1',
                specific: '2',
                registration: '3'
            },
            checked: true,
            data: {
                title: "",
                subtitle: "",
                register_title: "",
                author: "",
                publisher: "",
                isbn: "",
                quota: "",
                series_title: "",
                summary: "",
                dewey: "",
                payment_tp: '',
                payment_tl: '',
                edu: '',
                grade_lvl: '',
                discription: '',
                fee: "",
                discount: '',
                afterDisc: '',
                dueDate: '',
                start_date:'',
                end_date:'',

            }

        }
        this.onChange = this.onChange.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
        this.handleBtnReady = this.handleBtnReady.bind(this)
        this.onDrop = this.onDrop.bind(this)
        this.handleDropdown = this.handleDropdown.bind(this)
        this.getDataStudent = this.getDataStudent.bind(this)
        this.generateFilter = this.generateFilter.bind(this)
        this.filter = this.filter.bind(this)
        this.getPaymentTitle = this.getPaymentTitle.bind(this)
        this.onChangeDiscount = this.onChangeDiscount.bind(this)
        this.handleRadio = this.handleRadio.bind(this)
        this.generateSearch = this.generateSearch.bind(this)
        this.getDataEduProgram = this.getDataEduProgram.bind(this)
        this.handleDropdownEducation = this.handleDropdownEducation.bind(this)
        this.getDataEduLevel = this.getDataEduLevel.bind(this)
        this.toggleCheckboxChange = this.toggleCheckboxChange.bind(this)
        this.typeFilter = this.typeFilter.bind(this)
        this.onCheckAllChange = this.onCheckAllChange.bind(this)
        this._handleSearch = this._handleSearch.bind(this)
        this._add = this._add.bind(this);
        this._remove = this._remove.bind(this);
        this.typeFilterRender = this.typeFilterRender.bind(this);
        this.generateFilterRender = this.generateFilterRender.bind(this)
        this._handleSearchFind = this._handleSearchFind.bind(this)
        this.generateSearchData = this.generateSearchData.bind(this)
        this.generateDate = this.generateDate.bind(this)
        this.onChangeDate = this.onChangeDate.bind(this)

        const token = localStorage.getItem("_token")
        if (token === null) {
            this.props.history.push('/login')
        }
    }
    componentWillMount() {
        this.selectedCheckboxes = new Set();
    }
    onCheckAllChange(e) {
        this.setState({
            checkedList: e.target.checked ? plainOptions : [],
            indeterminate: false,
            checkAll: e.target.checked,
        });
    }
    componentDidMount() {
        //did mount
        this.getDataEduProgram()

    }
    _add(user_id, name, nis, grade_name, grade_level, programe_name) {
        this.state.collect.push({
            user_id,
            name,
            nis,
            programe_name,
            grade_name,
            grade_level
        })

        this.setState(this.state)
        this.setState({
            show: true,
        })

        console.log("hello", this.state.collect)
    }

    _remove(name) {
        this._processLineRemove(name)

    }

    onChangeDate(date, dateString){
        console.log(dateString)
        this.setState({
            dueDate:dateString
        })
    }

    _handleSearch(value) {
        this.state.halte.push({
            name: value
        })
        this.setState(this.state)
        this.setState({
            name: value,
            isShows: true

        })
        console.log(this.state.halte, "this value")
        // this.getDataStudent(value)
    }
    _handleSearchFind(value) {
        console.log("value", value)
        this.setState({
            name: value,
            isShow: true

        })
        this.getDataStudent(value)
    }


    _processLineRemove(name) {
        var newlines = this.state.halte.filter(function (line) {
            return line.name !== name;
        });
        console.log("new line", newlines)
        this.setState({ halte: newlines });
    }

    formatPrice(price) {
        return new Intl.NumberFormat("ID", {
            style: "currency",
            currencyDisplay: "symbol",
            currency: "IDR"
        }).format(price);
    }
    onChange(e, prop) {
        e.preventDefault()
        if (this.state.id === undefined) {
            let dv = this.state.data
            dv[prop] = e.target.value
            this.setState({ data: dv })
        } else {
            let dv = this.state.data.inventory
            dv[prop] = e.target.value
            this.setState({ data: dv })
        }

    }
    onChangeDiscount(e, prop) {
        let dv = this.state.data;
        dv[prop] = e.target.value
        const pembilang = parseFloat(dv.undefined.replace(/,/g, ''))
        const discAmt = pembilang - e.target.value / 100 * pembilang
        // console.log(dv[],"disc")


        this.setState({ data: dv, amount_fee: discAmt, afterDisc: this.formatPrice(discAmt) })
    }

    handleDropdown(e, prop) {
        let dv = this.state.data
        dv[prop] = e !== null ? e : 0
        this.setState({ data: dv })
        this.getPaymentTitle()

    }
    handleDropdownEducation(e, prop) {
        let dv = this.state.data
        dv[prop] = e !== null ? e : 0
        this.setState({ data: dv })
        console.log(e !== null ? e : 0)
        this.getDataEduLevel()
    }

    handleDropdownPay(e, prop) {
        let dv = this.state.data
        dv[prop] = e !== null ? e : 0
        this.setState({ data: dv })
    }

    handleRadio(e, prop) {
        console.log('radio checked', e.target.value);
        this.setState({
            value: e.target.value,
        });

    }
    toggleModal() {
        this.setState({
            isOpen: !this.state.isOpen
        });

    }

    feeHandle(event) {
        this.setState({
            fee: this.formatPrice(event.target.value)
        })
    }

    getDataStudent(value) {
        let self = this
        console.log(this.state.value, "value", self.state.name)
        var url = ''
        if (this.state.value === 1) {
            url = `${apiPaymentHost}/payments/student_search?program_education=${self.state.data.edu.value}&grade_level=${self.state.data.grade_lvl.value}`;
        } else if (this.state.value === 2) {
            url = `${apiPaymentHost}/payments/student_search?type=specific&name=${value}`;
        } else {
            url = `${apiPaymentHost}/payments/student_search?type=specific&name=${value}`;
        }

        console.log(url)
        axios.request({
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                // let studentArr = [];
                // let user = [];
                // let student_name = [];
                // console.log("here", response.data.data.students)
                self.setState({
                    student_name: response.data.data.students
                })
                console.log(response.data.data, "hahahaha")
                // for (var i in response.data.data.students) {
                //     const datum = response.data.data.students[i]
                //     studentArr.push({ user: datum.user  })
                // }

                // for (var j in studentArr) {
                //     const datums = studentArr[j]
                //     user.push({ userArray: datums  })

                // }
                // // console.log("here", user)
                // for (var k in user) {
                //     const dat = user[k]
                //     console.log(dat.user,"fullname")
                //     student_name.push(dat.userArray)
                // }
                // console.log("herehrherhehr",student_name)


                // self.setState({
                //     student_name: student_name
                // })
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }
    getDataEduProgram() {
        let self = this
        axios.request({
            url: `${apiPaymentHost}/payments/transaction_new`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                console.log(response.data.data, "data")
                let edu_list = [];
                for (var i in response.data.data.import_type.education_list) {
                    const datum = response.data.data.import_type.education_list[i]
                    edu_list.push({ value: datum.key, label: datum.label })
                }
                self.setState({
                    edu_list: edu_list,
                })
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }

    getDataEduLevel() {
        let self = this
        axios.request({
            url: `${apiPaymentHost}/payments/get_list?type=${self.state.data.edu.value}&slug=program_education`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                console.log(response.data.data, "data")
                let level = [];
                for (var i in response.data.data.list) {
                    const datum = response.data.data.list[i]
                    level.push({ value: datum.key, label: datum.label })
                }
                self.setState({
                    level: level,
                })
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }



    getPaymentTitle() {
        let self = this
        axios.request({
            url: `${apiPaymentHost}/payments/get_list?type=${this.state.data.payment_tp.value}&slug=payment_type`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                let payment_title = [];
                for (var i in response.data.data.list) {
                    const datum = response.data.data.list[i]
                    payment_title.push({ value: datum.key, label: datum.label })
                }
                self.setState({
                    payment_title: payment_title,
                })
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }


    onDrop(files) {
        this.setState({
            files
        });
    }

    sendUrl() {
        setTimeout(() => {
            this.setState({
                swal: true
            })
        }, 500)

        setTimeout(() => {
            this.props.history.push({
                pathname: '/registration'
            })
        }, 2000)


    }
    handleBtnReady(e) {
        console.log("here date", this.state.collect);
        var model = {}
        var index = 0;
        var halte={}

        if (this.state.value === 1) {
            model = Object.assign({}, this.state.user)

        } else if (this.state.value === 2) {
            for (let i in this.state.collect) {
                const b = this.state.collect[i]
                console.log(b.user_id, "this B")
                model[index] = b.user_id
                index = index + 1
            }
        } else {

        }

        for (let i in this.state.halte) {
            const b = this.state.halte[i]
            halte[index] = b.name
            index = index + 1
        }
        console.log(halte, "hlt")
        console.log(this.state.data.payment_tp.value, "collection")
        console.log(this.state.data, "data")
        console.log(Object.assign({},halte),"halter")
        e.preventDefault()
        let self = this
        const refresh = () => {
            console.log("a")
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.sendUrl()
        }
        var type = ""
        if(this.state.value === 1){
            type = "grade"
        }else if(this.state.value === 2){
            type = "specific"
        }else{
            type= "all_student"
        }
        console.log(this.state.payment_tp.value === 'school_bus'?Object.assign({},this.state.halte):[],"halte check in")

        axios.request({
            url: `${apiPaymentHost}/registration_events/new`,
            headers: { 'Content-Type': 'application/json' },
            method: 'POST',
            data: {
                registration_event: {
                    title_type: this.state.data.payment_tp.value,
                    title: this.state.data.register_title,
                    description: this.state.data.discription,
                    amount_fee: parseFloat(this.state.data.fee.replace(/,/g, '')),
                    quota: this.state.data.quota,
                    registration_type: type,
                    closing_date: this.state.dueDate,
                    user_id: model,
                    sub_title:this.state.data.subtitile,
                    bus_stop: this.state.data.payment_tp.value === 'school_bus'?Object.assign({},halte):[]
                }

            }

        })
            .then(function (response) {
                refresh()

            })
            .catch(function (error) {
                // refresh()

                // this.setState({
                //     isOpen: !this.state.isOpen
                //   });

            });


    }
    filter(e) {
        e.preventDefault()
        this.setState({
            isShow: true
        })
        this.getDataStudent()
    }

    toggleCheckboxChange(id, e) {
        e.preventDefault()
        this.state.user.push(id)
        this.setState(this.state)
        console.log(this.state.user, "model")


    }
    // onChange(e) {
    //     console.log('checked = ', e.target.checked);
    //     this.setState({
    //         checked: e.target.checked,
    //     });
    // }

    typeFilter() {

        const model = []
        for (let i in this.state.student_name) {
            const b = this.state.student_name[i]
            model.push(b.user.full_name)
        }
        // const { isCheck } = this.state
        if (this.state.data.payment_tp.value === 'school_bus') {
            const filt = (
                <div>
                    <div className="col-md-12" >
                        <Search
                            placeholder="input search text"
                            enterButton="Find"
                            size="large"
                            onSearch={value => this._handleSearch(value)}
                        />
                    </div>

                </div>
            )
            return filt
        } else {
            const filt = (
                <div>
                </div>
            )
            return filt

        }
    }
    generateFilterRender() {
        const wr = {
            height: '150px',
            overflowY: 'scroll'
        }
        const border = {
            border: 'none'
        }
        // const btn = {
        //     marginLeft: '-0.3%'
        // }

        console.log("heawa", this.state.student_name)
        const model = []
        for (let i in this.state.student_name) {
            const b = this.state.student_name[i]
            model.push(b.user.full_name)
        }
        // const { isCheck } = this.state

        if (this.state.value === 1) {
            if (this.state.isShow === true) {
                const Content = (
                    <div class="col-md-12"  >
                        {/* <Checkbox checked={this.state.checked} onChange={this.onChange.bind(this)}>a</Checkbox> */}
                        <Card>
                            <CardHeader>Student of middle Schools</CardHeader>
                            <CardBody>
                                <div class="row row-flex row-flex-wrap" style={wr}>
                                    {
                                        this.state.student_name.map(function (array, i) {
                                            return <div class="col-md-4">
                                                <div class="panel panel-default flex-col" style={border}>
                                                    <div class="panel-body flex-grow" style={border}>
                                                        <Flex>
                                                            <div class="checkbox">
                                                                <Checkbox onChange={(e) => { this.toggleCheckboxChange(array.user_id, e) }} >{array.user.full_name}</Checkbox>
                                                                <p>{array.student.nis}</p>
                                                                <p>{array.student.grade_name} | {array.student.grade_level} | {array.student.programe_name}</p>

                                                            </div>
                                                        </Flex>
                                                    </div>
                                                </div>
                                            </div>
                                        }, this)
                                    }


                                </div>

                            </CardBody>
                        </Card>
                    </div>
                )
                return Content
            }
        } else if (this.state.value === 2) {
            if (this.state.isShow === true) {
                const Content = (
                    <div class="col-md-12"  >
                        <Card>
                            <CardHeader>Student of middle Schools</CardHeader>
                            <CardBody>
                                <div class="row row-flex row-flex-wrap" style={wr}>
                                    {
                                        this.state.student_name.map(function (array, i) {
                                            return <div class="col-md-4">
                                                <div class="panel panel-default flex-col" style={border}>
                                                    <div class="panel-body flex-grow" style={border}>
                                                        <Flex>
                                                            <div class="checkbox">
                                                                <p>{array.user.full_name}</p>
                                                                <p>{array.student.nis}</p>
                                                                <p>{array.student.grade_name} | {array.student.grade_level} | {array.student.programe_name}</p>
                                                                <Button style={{ marginLeft: '2em' }} onClick={(e) => { this._add(array.user_id, array.user.full_name, array.student.nis, array.student.grade_name, array.student.grade_level, array.student.programe_name) }} type="primary">Add</Button>
                                                            </div>
                                                        </Flex>
                                                    </div>
                                                </div>
                                            </div>
                                        }, this)
                                    }
                                </div>
                            </CardBody>
                        </Card>
                        {this.generateSearchData()}
                    </div>
                )
                return Content
            }
        } else {
            if (this.state.isShow === true) {
                const tempt = (

                    <div class="col-md-12"  >
                        {
                            this.state.collectType.map(function (arr, i) {
                                return <div class="col-md-4">
                                    <div class="panel panel-default flex-col" style={border}>
                                        <div class="panel-body flex-grow" style={border}>
                                            <Flex>
                                                <div class="checkbox">
                                                    {/* <a>{arr.name}</a> */}
                                                    {/* <Button shape="no" icon="close" onClick={(e) => { this._remove(arr.name) }} /> */}
                                                    <p style={{ marginTop: '0%', marginLeft: '0%', marginBottom: '0%' }} > {arr.name}</p>
                                                    {/* <p style={{ marginLeft: '25%' }}>{array.nis}</p>
                                                    <p style={{ marginLeft: '25%', marginTop: '-10%' }}>{array.grade_name} | {array.grade_level} | {array.programe_name}</p> */}
                                                </div>
                                            </Flex>
                                        </div>
                                    </div>
                                </div>
                            }, this)
                        }


                        {/* {this.generateSearch()} */}
                    </div>
                )

                return tempt
            }

        }
    }
    typeFilterRender() {
        // const wr = {
        //     height: '150px',
        //     overflowY: 'scroll'
        // }
        // const border = {
        //     border: 'none'
        // }
        const btn = {
            marginLeft: '-0.3%'
        }
        console.log("lo lo lo", this.state.type)
        const model = []
        for (let i in this.state.student_name) {
            const b = this.state.student_name[i]
            model.push(b.user.full_name)
        }
        // const { isCheck } = this.state
        if (this.state.value === 1) {
            const filt = (
                <div>
                    <div className="col-md-6" >
                        <label className={style.label} >Education Program</label>
                        <Select
                            className={style.selectCategory}
                            value={this.state.edu}
                            onChange={(e) => this.handleDropdownEducation(e, 'edu')}
                            options={this.state.edu_list}
                        />
                    </div>
                    <div className="col-md-4" >
                        <label className={style.label} >Level</label>
                        <Select
                            className={style.selectCategory}
                            value={this.state.grade_lvl}
                            onChange={(e) => this.handleDropdown(e, 'grade_lvl')}
                            options={this.state.level}
                        />
                    </div>
                    <div className="col-md-12" >
                        <button type="submit" className={style.btnFilter} style={btn} onClick={(e) => { this.filter(e) }} >Find Student</button>
                    </div>
                </div>
            )
            return filt
        } else if (this.state.value === 2) {
            const filt = (
                <div>
                    <div className="col-md-12" >
                        <Search
                            placeholder="input search text"
                            enterButton="Find"
                            size="large"
                            onSearch={value => this._handleSearchFind(value)}
                        />
                    </div>

                </div>
            )
            return filt
        } else {
            const filt = (
                <div></div>
            )
            return filt
        }
    }

    generateSearchData() {
        const border = {
            border: 'none'
        }

        if (this.state.show === true) {
            const tempt = (
                <div>
                    {
                        this.state.collect.map(function (array, i) {
                            console.log("i", i)
                            return <div class="col-md-4">
                                <div class="panel panel-default flex-col" style={border}>
                                    <div class="panel-body flex-grow" style={border}>
                                        <Flex>
                                            <div class="checkbox">
                                                <Button shape="no" icon="close" onClick={(e) => { this._remove(array.user_id, array.name, array.nis, array.grade_name, array.grade_level, array.programe_name) }} />
                                                <p style={{ marginTop: '-26%', marginLeft: '25%', marginBottom: '10%' }} > {array.name}</p>
                                                <p style={{ marginLeft: '25%' }}>{array.nis}</p>
                                                <p style={{ marginLeft: '25%', marginTop: '-10%' }}>{array.grade_name} | {array.grade_level} | {array.programe_name}</p>
                                            </div>
                                        </Flex>
                                    </div>
                                </div>
                            </div>
                        }, this)
                    }
                </div>
            )
            return tempt
        }

    }


    generateFilter() {
        const border = {
            border: 'none'
        }
        const model = []
        for (let i in this.state.student_name) {
            const b = this.state.student_name[i]
            model.push(b.user.full_name)
        }

        // console.log("here om", this.state.data.payment_tp)

        if (this.state.isShows === true) {
            const Content = (
                <div class="col-md-12"  >
                    {
                        this.state.halte.map(function (array, i) {
                            return <div class="col-md-4">
                                <div class="panel panel-default flex-col" style={border}>
                                    <div class="panel-body flex-grow" style={border}>
                                        <Flex>
                                            <div class="checkbox">
                                                {/* <Button shape="no" icon="close" onClick={(e) => { this._remove(array.user_id, array.name, array.nis, array.grade_name, array.grade_level, array.programe_name) }} /> */}
                                                <Button style={{ marginLeft: '-16%' }} shape="circle" icon="close" onClick={(e) => { this._remove(array.name, e) }} />
                                                <p style={{ marginTop: '-16%', marginLeft: '15%', marginBottom: '10%' }} >{array.name}</p>

                                            </div>
                                        </Flex>
                                    </div>
                                </div>
                            </div>

                        }, this)
                    }

                </div>
            )
            return Content
        }

    }

    generateDate(){
        if(this.state.data.payment_tp.value === 'edu_trip'){
            const tmp = (
                    <div>
                          <div className="col-md-12" >
                                    <label className={style.label} >Departure Date</label>
                                        <div>
                                            <DatePicker onChange={(e) => this.onChange(e, 'dueDate')} style={{width:400}} value={this.state.data.dueDate} />
                                        </div>
                                </div>
                                <div className="col-md-12" >
                                    <label className={style.label} >Return Date</label>
                                        <div>
                                            <DatePicker onChange={(e) => this.onChange(e, 'dueDate')}  style={{width:400}} value={this.state.data.dueDate} />
                                        </div>
                                </div>
                    </div>
            )
            return tmp
        }
    }

    generateSearch() {
        const border = {
            border: 'none'
        }
        console.log(this.state.show, "show status")
        if (this.state.show === true) {
            const tempt = (
                <div>
                    {
                        this.state.halte.map(function (array, i) {
                            console.log("i", i)
                            return <div class="col-md-4">
                                <div class="panel panel-default flex-col" style={border}>
                                    <div class="panel-body flex-grow" style={border}>
                                        <Flex>
                                            <div class="checkbox">
                                                <Button shape="no" icon="close" onClick={(e) => { this._remove(array.user_id, array.name, array.nis, array.grade_name, array.grade_level, array.programe_name) }} />
                                                <p style={{ marginTop: '-26%', marginLeft: '25%', marginBottom: '13%' }} > {array.name}</p>
                                                <p style={{ marginLeft: '25%' }}>{array.nis}</p>
                                                <p style={{ marginLeft: '25%', marginTop: '-10%' }}>{array.grade_name} | {array.grade_level} | {array.programe_name}</p>
                                            </div>
                                        </Flex>
                                    </div>
                                </div>
                            </div>
                        }, this)
                    }
                </div>
            )
            return tempt
        }

    }



    render() {
        const radio = {
            marginLeft: '-7%',
        }

        const model = []
        for (let i in this.state.student_name) {
            const b = this.state.student_name[i]
            model.push(b.user.full_name)
        }
        console.log('value', this.state.value)

        return (
            <div className={style.profilePage} >
                <NavbarMainPayment />
                <div className={style.mainContent}>

                    <div className={style.boxAddBook}>
                        <div>
                            <form >
                                <div className="col-sm-12 center">
                                    <label className={style.labelDate}>Title Type</label>
                                    <div className="input-group date col-sm-12">
                                        <Select
                                            className={style.selectCategory}
                                            value={this.state.data.payment_tp}
                                            onChange={(e) => this.handleDropdown(e, 'payment_tp')}
                                            options={this.state.payment_type}
                                        />
                                    </div>
                                </div>
                                <div className="col-md-11" >
                                    <label className={style.label} >Registration Title</label>
                                    <div className={style.formDisc}>
                                        <input type="text" className="form-control" value={this.state.data.register_title} onChange={(e) => this.onChange(e, 'register_title')} />
                                    </div>
                                </div>
                                <div className="col-md-11" >
                                    <label className={style.label} >Subtitle</label>
                                    <div className={style.formDisc}>
                                        <input type="text" className="form-control" value={this.state.data.subtitile} onChange={(e) => this.onChange(e, 'subtitile')} />
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label} >Description</label>
                                    <div className={style.form}>
                                        <input type="text" className="form-control" onChange={(e) => this.onChange(e, 'discription')} value={this.state.data.discription} />
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label} >Quota</label>
                                    <div className={style.form}>
                                        <input type="text" className="form-control" onChange={(e) => this.onChange(e, 'quota')} value={this.state.data.quota} />
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label} >Amount Fee</label>
                                    <div className={style.form}>
                                        <Cleave
                                            options={{
                                                numeral: true,
                                                numeralThousandsGroupStyle: 'thousand'
                                            }}
                                            className="form-control"
                                            onChange={(e) =>{this.onChange(e,'fee')}} />
                                    </div>
                                </div>
                                {this.typeFilter()}
                                {this.generateFilter()}
                                {
                                    this.state.student_from.map(function (arr, l) {
                                        return <div className="col-md-4" >
                                            <label className={style.label} > {l === 0 ? 'Registration Type' : ''}</label>
                                            <div className="radio" style={radio}>
                                                <RadioGroup onChange={(e) => { this.handleRadio(e, 'value') }} value={this.state.value}>
                                                    <Radio value={arr.value}>{arr.label}</Radio>
                                                </RadioGroup>
                                            </div>
                                        </div>
                                    }, this)
                                }
                                {this.typeFilterRender()}
                                {this.generateFilterRender()}

                                <div className="col-md-12" >
                                    <label className={style.label} >Registration End Date</label>
                                        <div>
                                            <DatePicker onChange={this.onChangeDate} style={{width:400}} />
                                        </div>
                                       

                                    {/* <input type="date" className="form-control" data-date-format="DD MMMM YYYY" onChange={(e) => this.onChange(e, 'dueDate')} value={this.state.data.dueDate} /> */}
                                </div>
                                {this.generateDate()}
                                

                            </form>
                            <div className="col-md-12" >
                                <button type="submit" className={style.btn} onClick={(e) => { this.handleBtnReady(e) }} >Save</button>
                            </div>

                            <div>
                                <Swal show={this.state.swal} >
                                    <div>
                                        <img src={Check} alt="check" className={style.Path} />
                                        <p className={style.swal}>New item has been added.</p>
                                    </div>
                                </Swal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Profile;