import React, { Component } from 'react';
import { NavbarMainPayment } from './../../components/Navbar/navbar';
import { Title } from './../../styles/pay';
import Sidebar from './../../components/Layout/layoutInventory';
import styled from 'styled-components';
import breakpoint from "styled-components-breakpoint";
import axios from 'axios';
import { apiPaymentHost } from './../../utils/constant'
import { Input, Select, DatePicker } from 'antd';
import Pagination from './../../components/pagination/index'

var dateFormat = require('dateformat');

const Search = Input.Search;
const Option = Select.Option;
const { RangePicker } = DatePicker;

const style = require('./../../styles/dashboard/dashboard.css');

export default class transaction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: '1',
      type: "payment",
      data: {
        registration_events: []
      },
      subject: [
        { value: 'asc', label: 'Ascending' },
        { value: 'desc', label: 'Descending' },
      ],
      import: [
        { value: '', label: '' }
      ],
    }
    this.toggle = this.toggle.bind(this);
    this.changePage = this.changePage.bind(this)
    this.changePageCreate = this.changePageCreate.bind(this)
    this.getData = this.getData.bind(this)
    this.getDataType = this.getDataType.bind(this)
    this.handlePagination = this.handlePagination.bind(this)
  }
  componentDidMount() {
    this.getData()
  }
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  
  handlePagination(page) {

    let self = this
    var url;
    url = `${apiPaymentHost}/registration_events/index?page=${page}`
    console.log(url, "thisurl")
    axios.request({
      url: url,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'GET'
    })
      .then(function (response) {
        self.setState({
          data: response.data.data,
          page: response.data.meta.pagination.page,
          total_page: response.data.meta.pagination.total_page
        })
      })
      .catch(function (error) {
        // refresh()
        console.log("here")
      });

  }


  formatPrice(price) {
    return new Intl.NumberFormat("ID", {
      style: "currency",
      currencyDisplay: "symbol",
      currency: "IDR"
    }).format(price);
  }
  changePage(id) {
    this.props.history.push({
      pathname: '/detail-registration',
      state: { id: id }
    })
  }
  changePageCreate() {
    this.props.history.push('/create-registration')
  }

  getDataType() {
    let self = this
    var url;
    url = `${apiPaymentHost}/payments/get_list?slug=registration`
    console.log(url, "thisurl")
    axios.request({
      url: url,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'GET'
    })
      .then(function (response) {
        self.setState({
          data: response.data.data,
          page: response.data.meta.pagination.page,
          total_page: response.data.meta.pagination.total_page
        })
      })
      .catch(function (error) {
        console.log("here")
      });
  }


  getData() {
    let self = this
    var url;
    url = `${apiPaymentHost}/registration_events/index`
    console.log(url, "thisurl")
    axios.request({
      url: url,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'GET'
    })
      .then(function (response) {
        self.setState({
          data: response.data.data,
          page: response.data.meta.pagination.page,
          total_page: response.data.meta.pagination.total_page
        })
      })
      .catch(function (error) {
        // refresh()
        console.log("here")
      });
  }

  render() {
    const color = {
      backgroundColor: '#edf5ff',
    }
    const margin = {
      marginTop: '2em',
      marginBottom: '2%'
    }
    const Selector = styled.div`
      margin: 0 0.1rem;
      border-radius: 4px;
      padding: 0.2rem 1.2rem;
      border: solid 1px;
      font-size: 1.2rem;
      margin-bottom: 0.3rem;
      
      ${breakpoint("sm")`
        font-size: 0.9rem;
        padding: 0.2rem 1rem;
      `}
    `;
    const AddFilter = styled(Selector)`
      width: 161px;
      height: 44px;
      margin-left : 83.5%;
      margin-top: -2.2%;
      border-color: #4274a8;
      background-color: #4274a8;
      cursor: pointer;
      color: #ffffff;
      font-family: Nunito Sans;
      
      span {
        margin-right: 1rem;
        margin-top : 1rem;
        
      }    
      :hover {
        border-radius: 4px;
        color: white;
      }
    `;

    console.log("wa", this.state.page,this.state.total_page)

    return (
      <div>
        <NavbarMainPayment status={this.state.type} />
        <Title>Registration</Title>
        <AddFilter onClick={this.changePageCreate} ><span className="glyphicon glyphicon-plus"></span> Create Registration</AddFilter>
        <Sidebar>
          <div className={style.boxTableInv}>
            <div style={margin}>
              <Search
                placeholder="Find your transaction"
                onSearch={value => this.findOne(value)}
                style={{ width: 320, marginRight: 20, height: 48, marginTop: '0.5em' }}
              />

              <Select
                showSearch
                style={{ width: 200, marginRight: 20 }}
                placeholder="Sort by title "
                optionFilterProp="children"
                onChange={this.handleChange}
                onFocus={this.handleFocus}
                onBlur={this.handleBlur}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              >
                {this.state.subject.map(function (arr, i) {
                  return <Option value={arr.value}>{arr.label}</Option>
                })
                }
              </Select>

              <Select
                showSearch
                style={{ width: 200, marginRight: 20 }}
                placeholder="Import from "
                optionFilterProp="children"
                onChange={this.handleChange}
                onFocus={this.handleFocus}
                onBlur={this.handleBlur}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              >
                {this.state.subject.map(function (arr, i) {
                  return <Option value={arr.value}>{arr.label}</Option>
                })
                }
              </Select>
              <RangePicker onChange={this.onChangeDate} style={{ width: '22em' }} />

            </div>

            <table className="table table-bordered" >
              <thead className="thead-dark" style={color}>
                <tr>
                  <th className={style.fontTable} scope="col">No</th>
                  <th className={style.fontTable} scope="col">Registration Code</th>
                  <th className={style.fontTable} scope="col">Title</th>
                  <th className={style.fontTable} scope="col">For</th>
                  <th className={style.fontTable} scope="col">Quta</th>
                  <th className={style.fontTable} scope="col">Fee</th>
                  <th className={style.fontTable} scope="col">End Date</th>
                  <th className={style.fontTable} >Create On</th>
                </tr>
              </thead>
              <tbody>
                {
                  this.state.data.registration_events.map(function (array, j) {
                    return <tr>
                      <td className={style.valueTable} >{j + 1}</td>
                      <td onClick={(e) => { this.changePage(array.id, e) }} className={style.popCol} >{array.code}</td>
                      <td className={style.valueTable} >{array.sub_title}</td>
                      <td className={style.valueTable} >{array.title}</td>
                      <td className={style.valueTable} >{array.quota}</td>
                      <td className={style.valueTable} >{this.formatPrice(array.amount_fee)}</td>
                      <td className={style.valueTable} >{dateFormat(array.closing_date, "isoDate")}</td>
                      <td className={style.valueTable} >{dateFormat(array.created_on, "isoDate")}</td>
                    </tr>
                  }, this)
                }

              </tbody>
            </table>
            <div className={style.page}>
              {/* <Pagination items={this.state.data.circulations}  pageSize="10"  onChangePage={this.onChangePage} /> */}
              <Pagination
                margin={2}
                page={this.state.page}
                count={Math.ceil(this.state.total_page)}
                onPageChange={this.handlePagination}
              />
            </div>

          </div>

        </Sidebar>
      </div>
    )
  }
}
