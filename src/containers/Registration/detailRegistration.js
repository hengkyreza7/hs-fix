import React, { Component } from 'react';
import { NavbarMainPayment } from './../../components/Navbar/navbar';
import { Title } from './../../styles/pay';
import Sidebar from './../../components/Layout/layoutInventory';
import styled from 'styled-components';
import breakpoint from "styled-components-breakpoint";
// import Select from 'react-select';
import axios from 'axios';
// import Modals from './../../components/Modal/stop';
import { apiPaymentHost } from './../../utils/constant'
// import Badge from './../../styles/assets/group-4@2x.png'
// import { Button } from 'antd';
// import Books from './../../styles/assets/user.svg';
// import User from './../../styles/assets/users.png';
// import Telp from './../../styles/assets/telp.svg';
// import Remove from './../../components/Modal/remove';


const style = require('./../../styles/dashboard/dashboard.css');
var dateFormat = require('dateformat');

export default class transaction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: '1',
            id: this.props.match.params.id,
            type: "payment",
            swal: false,
            data: {
                invoices: [],
                invoice_batch: {},
                registration_pre_orders: [],
                registration_event: {
                    bus_stops: []
                },

            },
            reason: '',
            isOpenPick: false,
            visible: false,
            code: ''
        }
        this.toggle = this.toggle.bind(this);
        this.changePage = this.changePage.bind(this);
        this.changePageDetail = this.changePageDetail.bind(this);
        this.getData = this.getData.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.remove = this.remove.bind(this)
        this.onChangeTextArea = this.onChangeTextArea.bind(this)
        this.renderButton = this.renderButton.bind(this)
        this.showModal = this.showModal.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
        this.handleOk = this.handleOk.bind(this)
        this.stop = this.stop.bind(this)
        this.group = this.group.bind(this)
        this.generateBusStop = this.generateBusStop.bind(this)
        this.generateTable = this.generateTable.bind(this)
    }
    componentDidMount() {
        this.getData()
    }
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    showModal(e, id) {
        this.setState({
            visible: !this.state.visible,
            id: id
        });
    }
    handleOk(e) {
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    handleCancel(e) {
        console.log(e);
        this.setState({
            visible: false,
        });
    }



    onChangeTextArea(event) {
        this.setState({
            reason: event.target.value
        })
    }

    stop(e) {
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.sendUrl()
            this.getData()
        }
        axios({
            url: `${apiPaymentHost}/payments/batch_stoped?id=${this.state.data.invoice_batch.id}`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            data: {
                "reason": this.state.reason
            }
        })
            .then(function (response) {
                refresh()


            })
            .catch(function (error) {

            });
    }

    remove(e) {
        console.log("awawa", this.state.id)
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ visible: false })
            }, 200)
            this.sendUrl()
            this.getData()
        }
        axios({
            url: `${apiPaymentHost}/payments/invoice_deleted?id=${this.state.id}`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            data: {
                "reason": this.state.reason
            }
        })
            .then(function (response) {
                refresh()


            })
            .catch(function (error) {

            });
    }

    group(e, id) {
        e.preventDefault()
        console.log(id, "the id")
        this.props.history.push({
            pathname: '/detail-group-payment-all/' + id,
            state: { id: id }
        })
    }



    toggleModal(user_id, id, number, title, status, author, e) {
        this.setState({
            isOpen: !this.state.isOpen,
            // receipt: number,
            // title: title,
            // status: status,
            // author: author,
            // id_circulation: id

        });
        // this.getBorrowedName(user_id)

    }

    changePage() {
        this.props.history.push('/registration')
    }

    changePageDetail() {
        this.props.history.push('/detail-payment')
    }

    generateBusStop() {
        console.log( this.state.data.registration_event.bus_stops,"array bus stop")
        const name = []
        for(let i in  this.state.data.registration_event.bus_stops){
            const data =  this.state.data.registration_event.bus_stops[i].name
            name.push(data)
        }

        if (this.state.data.registration_event.title_type === 'school_bus') {
            const tmp = (
                <div>
                    <h1>Bus Stop :  </h1>
                     <h5 style={bs} >{name.join(",")}</h5>
                </div>

            )
            return tmp
        }



    }

    generateTable() {
        if (this.state.data.registration_event.title_type === 'school_bus') {
            const tmp = (
                <table className="table table-bordered" style={margin} >
                  <thead className="thead-dark" style={color}>
                        <tr>
                            <th className={style.fontTable} scope="col">No</th>
                            <th className={style.fontTable} scope="col" style={width}>Student Name</th>
                            <th className={style.fontTable} scope="col">Program</th>
                            <th className={style.fontTable} scope="col">Bus Stop</th>
                            <th className={style.fontTable} scope="col">Registered On</th>
                            <th className={style.fontTable} scope="col">Payment</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.data.registration_pre_orders.map(function (arr, k) {
                                return <tr>
                                    <td className={style.valueTable} >{k + 1}</td>
                                    <td className={style.valueTable} >{arr.user.full_name}</td>
                                    <td className={style.valueTable} >{arr.user.student.program_name}</td>
                                    <td className={style.valueTable} >{arr.bus_stop.name}</td>
                                    <td className={style.valueTable} >{dateFormat(arr.created_at,"isoDate")}</td>
                                    <td onClick={this.changePageDetail} className={style.popCol} >Detail</td>
                                </tr>
                            }, this)
                        }

                    </tbody>
                </table>

            )
            return tmp
        } else {
            const tmp = (
                <table className="table table-bordered" style={margin} >
                    <thead className="thead-dark" style={color}>
                        <tr>
                            <th className={style.fontTable} scope="col">No</th>
                            <th className={style.fontTable} scope="col" style={width}>Student Name</th>
                            <th className={style.fontTable} scope="col">Program</th>
                            <th className={style.fontTable} scope="col">Start Date</th>
                            <th className={style.fontTable} scope="col">End Date</th>
                            <th className={style.fontTable} scope="col">status</th>
                            <th className={style.fontTable} scope="col">Payment</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.data.registration_pre_orders.map(function (arr, k) {
                                return <tr>
                                    <td className={style.valueTable} >{k + 1}</td>
                                    <td onClick={this.changePageDetail} className={style.popCol} >{arr.user.full_name}</td>
                                    <td className={style.valueTable} >{arr.user.student.program_name}</td>
                                    <td className={style.valueTable} >{arr.user.student.program_name}</td>
                                    <td className={style.valueTable} >20 Jan 2019</td>
                                    <td className={style.valueTable} >K-1</td>
                                    <td onClick={this.changePageDetail} className={style.popCol} >Detail</td>
                                </tr>
                            }, this)
                        }

                    </tbody>
                </table>

            )
            return tmp
        }

    }

    getData() {
        let self = this
        axios.request({
            url: `${apiPaymentHost}/registration_events/detail?id=${this.props.location.state.id}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                self.setState({
                    data: response.data.data,
                })

                // const model = {}
                // let index = 0;
                // for (let i in response.data.data) {
                //     const b = response.data.data[i]
                //     model[index] = b
                //     index = index+1
                // }
                // console.log(model,"here hash")
                // console.log(response.data.data,"here hash")

            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }

    formatPrice(price) {
        return new Intl.NumberFormat("ID", {
            style: "currency",
            currencyDisplay: "symbol",
            currency: "IDR"
        }).format(price);
    }

    renderButton() {
        const Selector = styled.div`
        margin: 0 0.1rem;
        border-radius: 4px;
        padding: 0.2rem 1.2rem;
        border: solid 1px;
        font-size: 1.2rem;
        margin-bottom: 0.3rem;
        
        ${breakpoint("sm")`
            font-size: 0.9rem;
            padding: 0.2rem 1rem;
        `}
        `;
        const Stop = styled.div`

        button {
            width: 170px;
            height: 30px;
            margin-top: 40%;
            margin-left : 90%;
            border: solid 1px #fe634f;
            background-color: #fe634f;
            cursor: pointer;
            color: #ffffff;
            font-family: Nunito Sans;
            font-size : 18px;
        }
        
        span {
            margin-right: 1rem;
            margin-top : 1rem;
        }    
        
    `;
        const Export = styled(Selector)`
        width: 170px;
        height: 30px;
        margin-top: 10%;
        margin-left : 90%;
        border: solid 1px #054ee1;
        background-color: #054ee1;
        cursor: pointer;
        color: #ffffff;
        font-family: Nunito Sans;
        font-size : 18px;
        
        span {
            margin-left: 1%;
            margin-top : 1rem;
        }
        
    `;

        let stop = this.state.data.invoice_batch.stoped
        if (stop === 0) {
            const tmpt = (
                <div class="col-md-2" >
                    <Stop>
                        <button onClick={(e) => { this.toggleModal(e) }}>Stop Registration</button>
                    </Stop>
                    <Export>Export Report</Export>
                </div>
            )
            return tmpt;

        }

    }
    render() {

        console.log(this.state.data)

        return (
            <div>
                <NavbarMainPayment status={this.state.type} />
                <Title>Transaction Management</Title>
                <Sidebar>
                    <div class="col-md-3" >
                        <Back onClick={this.changePage} ><span className="glyphicon glyphicon-arrow-left"></span> Back</Back>
                        <Right>
                            <div>
                                <h1>Registration Code:</h1>
                                <p>{this.state.data.registration_event.code}</p>
                            </div>
                            <div>
                                <h1>Create On:</h1>
                                <p>{dateFormat(this.state.data.registration_event.created_at, "isoDate")}</p>
                            </div>
                            <div>
                                <h1>Due Date:</h1>
                                <p>{dateFormat(this.state.data.registration_event.closing_date, "isoDate")}</p>
                            </div>
                            <div>
                                <h1>Registration For :</h1>
                                <p>{this.state.data.registration_event.registration_type}  </p>
                            </div>
                            <div>
                                <h1>Student From :</h1>
                                <p>{this.state.data.registration_event.sub_title}</p>
                            </div>
                            <div>
                                <h1>Amount Fee :</h1>
                                <p>{this.formatPrice(this.state.data.registration_event.amount_fee)}</p>
                            </div>

                        </Right>
                    </div>
                    <div class="col-md-9" >
                        <div class="col-md-7" >
                            <TitlePage>Detail - {this.state.data.registration_event.title} </TitlePage>
                            <Desc>
                                <div>
                                    <h1>Description</h1>
                                    <p>{this.state.data.registration_event.description}</p>

                                    {this.generateBusStop()}
                                </div>
                            </Desc>

                        </div>
                        {/* <div class="col-md-2" >
                            <Stop>Stop Registration</Stop>
                            <Export>Export Report</Export>
                        </div>
                    </div>
                    <div class="col-md-9" style={margin} >
                        <div class="col-md-3" style={marginSelect}>
                            <Select
                                options={this.state.subject}
                            />
                        </div>
                        <div class="col-md-3" style={marginSelect}>
                            <Select
                                options={this.state.subject}
                            />
                        </div>
                        <div class="col-md-3" style={marginSelect}>
                            <Select
                                options={this.state.subject}
                            />
                        </div> */}
                        {/* <div class="col-md-3"  >
                            <Find ><p>Filter</p></Find>
                        </div> */}


                        {this.generateTable()}
                    </div>
                </Sidebar>
            </div>
        )
    }
}
const width = {
    width: '20em'
}

const color = {
    backgroundColor: '#edf5ff',
}

const margin = {
    marginLeft: '0%',
    marginTop: '30%',
}


const Selector = styled.div`
    margin: 0 0.1rem;
    border-radius: 4px;
    padding: 0.2rem 1.2rem;
    border: solid 1px;
    font-size: 1.2rem;
    margin-bottom: 0.3rem;
    
    ${breakpoint("sm")`
        font-size: 0.9rem;
        padding: 0.2rem 1rem;
    `}
    `;
const Back = styled(Selector)`
    width: 120px;
    height: 44px;
    margin-top: 20%;
    margin-left : 10%;
    border: solid 1px #054ee1;
    background-color: #ffffff;
    cursor: pointer;
    color: #054ee1;
    font-family: Nunito Sans;
    font-size : 18px;
    
    span {
        margin-right: 1rem;
        margin-top : 1rem;
    }    
    
`;

// const Stop = styled(Selector)`
//     width: 170px;
//     height: 30px;
//     margin-top: 40%;
//     margin-left : 90%;
//     border: solid 1px #fe634f;
//     background-color: #fe634f;
//     cursor: pointer;
//     color: #ffffff;
//     font-family: Nunito Sans;
//     font-size : 18px;

//     span {
//         margin-right: 1rem;
//         margin-top : 1rem;
//     }    

// `;
// const Export = styled(Selector)`
//     width: 170px;
//     height: 30px;
//     margin-top: 10%;
//     margin-left : 90%;
//     border: solid 1px #054ee1;
//     background-color: #054ee1;
//     cursor: pointer;
//     color: #ffffff;
//     font-family: Nunito Sans;
//     font-size : 18px;

//     span {
//         margin-left: 1%;
//         margin-top : 1rem;
//     }    

// `;

const TitlePage = styled.div`
    margin-top : 10%;
    width: 277px;
    height: 34px;
    font-family: Nunito Sans;
    font-size: 25px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: -0.7px;
    color: #233539;
`
const Desc = styled.div`
   div{
        h1 {
            margin-top : 7%;
            width: 95px;
            height: 20px;
            font-family: Nunito Sans;
            font-size: 15px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #1f1f1f;
        }

        p {
            margin-top : 0%;
            height: 28px;
            font-family: Nunito Sans;
            font-size: 15px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.87;
            letter-spacing: normal;
            color: #323c47;

        }

        h5 {
            margin-top : -6.2%;
            margin-left : 20%;
            text-align : justify;
            height: 28px;
            font-family: Nunito Sans;
            font-size: 15px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.87;
            letter-spacing: normal;
            color: #323c47;
        }
        h4 {
            margin-left : 30%;
            margin-top : -13%;
            height: 18px;
            font-family: Nunito Sans;
            font-size: 13px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #4da1ff;
        }
        h3 {
            margin-top : -7.2%;
            margin-left: 30%;
            height: 28px;
            font-family: Nunito Sans;
            font-size: 15px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.87;
            letter-spacing: normal;
            color: #323c47;

        }
        
   } 
  
  
`

// const Find = styled(Selector)`
//     height: 44px;
//     margin-top : 90%;
//     border-color: #4f7cfe;
//     background-color: #4f7cfe;
//     cursor: pointer;
//     color: #ffffff;
//     font-family: Nunito Sans;

//     p {
//         margin-left : 60px;
//         margin-top: 10px;
//         width: 39px;
//         height: 16.5px;
//         font-family: Nunito Sans;
//         font-size: 16.1px;
//         font-weight: normal;
//         font-style: normal;
//         font-stretch: normal;
//         line-height: 1.36;
//         letter-spacing: normal;
//         text-align: center;
//         color: #ffffff;

//         }    
//     :hover {
//     border-radius: 4px;
//     color: white;
//     }
// `;

const Right = styled(Selector)`
    margin-top : 5%;
    margin-left : 8%;
    width: 220px;
    height: 60%;
    border-radius: 5px;
    box-shadow: 0 11px 14px -10px #e5eced;
    border: solid 1px #e5eced;
    background-color: #ffffff;
    div {
        margin-bottom : 2%;

        h1 {
            margin-left : 1%;
            height: 20px;
            font-family: Nunito Sans;
            font-size: 15px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #1f1f1f;

        }
        h2 {
            margin-left : 10%;
            height: 20px;
            font-family: Nunito Sans;
            font-size: 15px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #233539;
        }

        p {
            margin-left : 1%;
            height: 20px;
            font-family: Nunito Sans;
            font-size: 15px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #233539;
        }
    }
`
const bs = {
    marginTop:'-2.2em'
}