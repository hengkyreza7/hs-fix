import React, { Component } from 'react';
import { NavbarMainPayment } from '../../components/Navbar/navbar';
import { Profile } from '../../components/admission/profile';
import { Assessment } from '../../components/admission/assessment';
import classnames from 'classnames';
import { Select } from 'antd';

const style = require('./../../styles/profile/profile.css');

class Detail extends Component {
    render(){
        const values = [ 
            {label: 'Mathematics', value: '40% - 70%'}, 
            {label: 'English', value: '3.0'}, 
            {label: 'Psychological', value: 'Above Average'} 
        ]
        
        return (
            <div className={style.profilePage} >
                <NavbarMainPayment />
                <div className={style.mainContent}>
                    <div className="container">
                        <div class={style.admission_content}>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div className={style.margin_bottom_6}>
                                        <h1 className={style.label_header}>Create Payment(s)</h1>
                                    </div>
                                </div>
                            </div>
                            <div class={classnames(style.admission_border, style.admission_border_applicant)}>
                                <Profile />
                                <div className={style.margin_top_6}>
                                    <Assessment 
                                        title = "Assessment Test Result"
                                        color = "result" 
                                        values = {values}
                                    />
                                    <Assessment 
                                        title = "Student Interview"
                                        color = "interview" 
                                        values = {values}
                                    />
                                    <Assessment 
                                        title = "Parent Interview"
                                        color = "parent"
                                        values = {values}
                                    />
                                </div>
                                <div className={style.margin_4}>
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div className={style.margin_bottom_2}>
                                                <textarea rows='10' className="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label className={style.labelDate}>Payment Title</label>
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <Select
                                                        showSearch
                                                        style={{ width: 550, marginRight: 20 }}
                                                        placeholder="Payment title"
                                                    >
                                                    </Select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div className={style.text_align_center}>
                                                        <input type="submit" value="Add" className={style.admission_submit} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <label className={classnames(style.labelDate, style.margin_top_2)}>Amount Of Fee</label>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type="text" className="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={classnames('row', style.margin_top_2)}>
                                        <div className="col-sm-3">
                                            <label className={style.labelDate}>Discount</label>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type="text" className="form-control" />                                            
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <label className={style.labelDate}>Fee After Discount</label>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type="text" className="form-control" />                                            
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={classnames('row', style.margin_top_2)}>
                                        <div class="col-sm-9">
											<button type="submit" className={style.btn_addmission} >Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Detail;