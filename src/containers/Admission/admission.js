import React, { Component } from 'react';
import { NavbarMainPayment } from './../../components/Navbar/navbar';
import { Title } from './../../styles/pay';
import Sidebar from './../../components/Layout/layoutInventory';
import styled from 'styled-components';
import breakpoint from "styled-components-breakpoint";
import axios from 'axios';
import { apiPaymentHost } from './../../utils/constant'
import Pagination from './../../components/pagination/index';
import { Tabs, Input, Select, DatePicker } from 'antd';
var dateFormat = require('dateformat');

const TabPane = Tabs.TabPane;
const Search = Input.Search;
const Option = Select.Option;
const { RangePicker } = DatePicker;
const style = require('./../../styles/dashboard/dashboard.css');



export default class admission extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: '1',
      type: "admission",
      mode: 'top',
      data: {
        admission_fees: []
      },
      meta:{
        pagination:[]
      },
      renderedUsers: [],
      page:1,
      invoice: [],
      pageOfItems: [],
      pageOfInvoice: [],
      search: "",
      subject: [
        { value: '000', label: 'Computer science, information & general works' },
        { value: '100', label: 'Philosophy & psychology' },
        { value: '200', label: 'Religion' },
        { value: '300', label: 'Social sciences' },
        { value: '400', label: 'Language' },
        { value: '500', label: 'Science' },
        { value: '600', label: 'Technology' },
        { value: '700', label: 'Arts & recreation' },
        { value: '800', label: "Literature" },
        { value: '900', label: "History & geography" }
      ],
      sort_times: [
        { value: 'asc', label: 'ASC'},
        { value: 'desc', label: 'DESC'},
      ]
    }
    this.getDataInvoice = this.getDataInvoice.bind(this)
    this.toggle = this.toggle.bind(this);
    this.changePage = this.changePage.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.findOne = this.findOne.bind(this);
    this.getData = this.getData.bind(this);
    this.onChangeInvoince = this.onChangeInvoince.bind(this)
  }

  componentDidMount() {
    this.getData()
  }

  onChangeDate(date, dateString) {
    console.log(date, dateString);
  }
  getDataInvoice() {
    let self = this
    axios.request({
      url: `${apiPaymentHost}/payments/invoice_all`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'GET'
    })
      .then(function (response) {
        self.setState({
          invoice: response.data.data,
          meta: response.data.meta
        })

      })
      .catch(function (error) {
        console.log("here")
      });
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
      this.onChangeInvoince()
      this.getDataInvoice()
    }

  }

  changePage() {
    this.props.history.push('/create-admission')
  }

  formatPrice(price) {
    return new Intl.NumberFormat("ID", {
        style: "currency",
        currencyDisplay: "symbol",
        currency: "IDR"
    }).format(price);
  }

  handleModeChange(e) {
    const mode = e.target.value;
    this.setState({ mode });
  }
  handleChange(value) {
    console.log(`selected ${value}`);
  }
  handleBlur() {
    console.log('blur');
  }

  handleFocus() {
    console.log('focus');
  }

  toggleModal(id, e) {
    e.preventDefault()
    this.props.history.push({
      pathname: '/detail-payment/' + id,
      state: { id: id }
    })
  }
  handlePageChange(page) {
    
    console.log(this.state.page,page,"week")
    const renderedUsers = this.state.data.payment_batchs.slice((page - 1) * 2, (page - 1) * 2 + 2);
     this.setState({ page:page, renderedUsers });
    console.log(renderedUsers)
    this.getData()

  }

  onChangeInvoince(page) {
    console.log((page - 1) * 2, (page - 1) * 2 + 2,"PAGINATE")

  }


  findOne(value) {
    this.setState({
      search: value
    })
    this.getData(value)
  }


  getData(value) {
    let self = this
    var url;
    if (value === undefined) {
      url = `${apiPaymentHost}/admission_fees?pagination=true`
    } else {
      url = `${apiPaymentHost}/admission_fees?pagination=true&per_page=200&payment_title=${value}`
    }
    axios.request({
      url: url,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'GET'
    })
      .then(function (response) {   
        self.setState({
          data: response.data.data,
          page:response.data.meta.pagination.page,
          total_page:response.data.meta.pagination.total_page
        })
      })
      .catch(function (error) {
        // refresh()
        console.log("here")
      });
  }
  callback(key) {
    console.log(key);
  }

  render() {
    const  { page } = this.state
    const color = {
      backgroundColor: '#edf5ff',
    }
    const margin = {
      marginTop: '1em',
      marginBottom: '2%'
    }

    const Selector = styled.div`
      margin: 0 0.1rem;
      border-radius: 4px;
      padding: 0.2rem 1.2rem;
      border: solid 1px;
      font-size: 1.2rem;
      margin-bottom: 0.3rem;

      ${breakpoint("sm")`
        font-size: 0.9rem;
        padding: 0.2rem 1rem;
      `}
    `;

    const AddFilter = styled(Selector)`
      width: 161px;
      height: 44px;
      margin-left : 83.5%;
      margin-top: -2.2%;
      border-color: #4274a8;
      background-color: #4274a8;
      cursor: pointer;
      color: #ffffff;
      font-family: Nunito Sans;

      span {
        margin-right: 1rem;
        margin-top : 1rem;

      }
      :hover {
        border-radius: 4px;
        color: white;
      }
    `;

    return (
      <div>
        <NavbarMainPayment status={this.state.type} />
        <Title>Admission Management</Title>
        <AddFilter onClick={this.changePage} ><span className="glyphicon glyphicon-plus"></span> Create Admission</AddFilter>
        <Sidebar>
          <div className={style.boxTableInv}>
            <Tabs defaultActiveKey="1" onChange={this.callback}>
              <TabPane tab="Data Master" key="1">
                <div>
                  <Search
                    placeholder="Find your transaction"
                    onSearch={value => this.findOne(value)}
                    style={{ width: 320, marginRight: 20, height: 48, marginTop: '0.5em' }}
                  />

                  <Select
                    showSearch
                    style={{ width: 200, marginRight: 20 }}
                    placeholder="Sort by program "
                    optionFilterProp="children"
                    onChange={this.handleChange}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {this.state.sort_times.map(function (arr, i) {
                      return <Option value={arr.value}>{arr.label}</Option>
                    })
                    }
                  </Select>

                  <Select
                    showSearch
                    style={{ width: 200, marginRight: 20 }}
                    placeholder="Sort by create date"
                    optionFilterProp="children"
                    onChange={this.handleChange}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {this.state.sort_times.map(function (arr, i) {
                      return <Option value={arr.value}>{arr.label}</Option>
                    })
                    }
                  </Select>

                  <Select
                    showSearch
                    style={{ width: 200, marginRight: 20 }}
                    placeholder="Sort by last update"
                    optionFilterProp="children"
                    onChange={this.handleChange}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {this.state.sort_times.map(function (arr, i) {
                      return <Option value={arr.value}>{arr.label}</Option>
                    })
                    }
                  </Select>
                </div>

                <table className="table table-bordered" style={margin}>
                  <thead className="thead-dark" style={color}>
                    <tr>
                      <th className={style.fontTable} scope="col">No</th>
                      <th className={style.fontTable} scope="col">Payment Title</th>
                      <th className={style.fontTable} scope="col">For Program</th>
                      <th className={style.fontTable} scope="col">Grade/level</th>
                      <th className={style.fontTable} scope="col">Nominal</th>
                      <th className={style.fontTable} >Create On</th>
                      <th className={style.fontTable} >Last Update</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.state.data.admission_fees.map(function (array, i) {
                        return <tr key={i}>
                          <td className={style.valueTable} >{i + 1}</td>
                          <td className={style.popCol} onClick={(e) => { this.toggleModal(array.id, e) }}><a> {array.payment_title} </a></td>
                          <td className={style.valueTable} >{array.grade.program}</td>
                          <td className={style.valueTable} >{array.grade.level}</td>
                          <td className={style.valueTable} >{this.formatPrice(array.amount)}</td>
                          <td className={style.valueTable} >{array.created_at === null || array.created_at === '' ? 'N/A' : dateFormat(array.created_at, "isoDate")}</td>
                          <td className={style.valueTable} >{array.updated_at === null || array.updated_at === '' ? 'N/A' : dateFormat(array.updated_at, "isoDate")}</td>
                        </tr>
                      }, this)
                    }

                  </tbody>
                </table>
                <div className={style.page}>
                  <Pagination
                    margin={2}
                    page={page}
                    count={Math.ceil(this.state.total_page)}
                    onPageChange={this.handlePageChange}
                  />
                </div>
              </TabPane>
              <TabPane tab="Admission Payment" key="2">
                <div>
                  <Search
                    placeholder="Find your transaction"
                    onSearch={value => console.log(value)}
                    style={{ width: 320, marginRight: 20, height: 48, marginTop: '0.5em' }}
                  />

                  <Select
                    showSearch
                    style={{ width: 200, marginRight: 20 }}
                    placeholder="Sort by title "
                    optionFilterProp="children"
                    onChange={this.handleChange}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {this.state.subject.map(function (arr, i) {
                      return <Option value={arr.value}>{arr.label}</Option>
                    })
                    }
                  </Select>

                  <Select
                    showSearch
                    style={{ width: 200, marginRight: 20 }}
                    placeholder="Import from "
                    optionFilterProp="children"
                    onChange={this.handleChange}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {this.state.subject.map(function (arr, i) {
                      return <Option value={arr.value}>{arr.label}</Option>
                    })
                    }
                  </Select>
                  <RangePicker onChange={this.onChangeDate} style={{ width: '20em' }} />

                </div>
                <table className="table table-bordered" style={margin}>
                  <thead className="thead-dark" style={color}>
                    <tr>
                      <th className={style.fontTable} scope="col">No</th>
                      <th className={style.fontTable} scope="col">Invoice Code</th>
                      <th className={style.fontTable} scope="col">Payment Title</th>
                      <th className={style.fontTable} scope="col">Participant</th>
                      <th className={style.fontTable} scope="col">Import From</th>
                      <th className={style.fontTable} scope="col">Fee</th>
                      <th className={style.fontTable} scope="col">Due Date</th>
                      <th className={style.fontTable} >Create On</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                    }

                  </tbody>
                </table>
                <div className={style.page}>
                  {/* <Pagination items={this.state.data.invoice} pageSize="10" onChangePage={this.onChangeInvoince} /> */}
                </div>
              </TabPane>
            </Tabs>




          </div>

        </Sidebar>
      </div>
    )
  }
}
