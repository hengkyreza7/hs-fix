import React, { Component } from 'react';
import { NavbarMainPayment } from '../../components/Navbar/navbar';
import { Search } from '../../components/admission/search';
import { ItemList } from '../../components/admission/item_list';
import classnames from 'classnames';

const style = require('./../../styles/profile/profile.css');

class Applicant extends Component {
    render() {
        return (
            <div className={style.profilePage} >
                <NavbarMainPayment />
                <div className={style.mainContent}>
					<div className="container">
						<div class={style.admission_content}>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div className={style.margin_bottom_6}>
                                        <h1 className={style.label_header}>List Applicant(s)</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input className={style.admission_input_search} type="text" />
                                    <input type="submit" value="Search" className={style.admission_submit} />
                                </div>
                            </div>
                            <div class={classnames(style.admission_border, style.admission_border_applicant)}>
                                <Search />
                                <div class="row">
                                    <div className={classnames("col-sm-12", style.margin_top_4)} >
                                        <ItemList />
                                    </div>
                                    <div className={classnames("col-sm-12", style.margin_top_4)} >
                                        <ItemList />
                                    </div>
                                    <div className={classnames("col-sm-12", style.margin_top_4)} >
                                        <ItemList />
                                    </div>
                                    <div className={classnames("col-sm-12", style.margin_top_4)} >
                                        <ItemList />
                                    </div>
                                    <div className={classnames("col-sm-12", style.margin_top_4)} >
                                        <ItemList />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Applicant;