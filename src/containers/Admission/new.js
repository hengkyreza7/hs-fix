import React, { Component } from 'react';
import Swal from './../../components/Modal/swal';
import axios from 'axios';
import Check from './../../styles/assets/check.png';
import { NavbarMainPayment } from '../../components/Navbar/navbar';
import Cleave from 'cleave.js/react';
import { apiPaymentHost } from './../../utils/constant';
import Select from 'react-select';

const style = require('./../../styles/profile/profile.css');

class New extends Component {
	constructor(props) {
		super(props)
		this.state = {
			isOpen: false,
			swal: false,
			edu_list: [],
			grade_names: [],
			grade_levels: [],
			fee_types: [],
			data: {
				program_education: "",
				grade_name: "",
				grade_level: "",
				description: "",
				payment_title: "",
				amount: 0,
				fee_type: "",
				grade_id: "",
			},
			import_type: []
		}

		this.submitForm = this.submitForm.bind(this)
		this.getNew = this.getNew.bind(this)
		this.handleDropdown = this.handleDropdown.bind(this)
		this.getGradeName = this.getGradeName.bind(this)
		this.sendUrl = this.sendUrl.bind(this)
	}

	componentDidMount() {
        //did mount
        this.getNew()
        this.getNew = this.getNew.bind(this)
    }

    getNew() {
		let self = this
		axios.request({
            url: `${apiPaymentHost}/payments/transaction_new`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        }).then(function(response){
			let edu_list = [];			
			for (var i in response.data.data.import_type.education_list) {
				const datum = response.data.data.import_type.education_list[i]
				edu_list.push({ value: datum.key, label: datum.label })
			}
			self.setState({
				edu_list: edu_list,
			})
		}).catch(function (error) {
			// refresh()
			console.log("here")
		});

		axios.request({
            url: `${apiPaymentHost}/payments/get_list?slug=fee_type`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        }).then(function(response){
			let fee_types = [];			
			for (var i in response.data.data.list) {
				const datum = response.data.data.list[i]
				fee_types.push({ value: datum.key, label: datum.label })
			}
			self.setState({
				fee_types: fee_types,
			})
		}).catch(function (error) {
			// refresh()
			console.log("here")
		});
	}


	
	// submit FORM
	sendUrl() {
        setTimeout(() => {
            this.setState({
                swal: true
            })
        }, 500)

        setTimeout(() => {
            this.props.history.push({
                pathname: '/admission'
            })
        }, 2000)
    }
	submitForm(e){
		// let data = this.state.data
		// let fields = ['grade_level', 'grade_name', 'program_education']
		// data = this.filterDataSelectBox(data, fields)

		// redirect
		let self = this
        const refresh = () => {
            console.log("a")
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.sendUrl()
		}
		// 

		axios.request({
            url: `${apiPaymentHost}/admission_fees`,
            headers: { 'Content-Type': 'application/json' },
            method: 'POST',
            data: {
                admission_fee: {
                    payment_title: this.state.data.payment_title,
					grade_id: this.state.data.grade_level.value,
					grade_program: this.state.data.program_education.value,
					grade_name: this.state.data.grade_name.value,
                    fee_type: this.state.data.fee_type.value,
                    description: this.state.data.description,
                    amount: parseFloat(this.state.data.amount.replace(/,/g, '')),
                }
            }
        }).then(function (response) {
			refresh()
		})
		.catch(function (error) {
			// refresh()

			// this.setState({
			//     isOpen: !this.state.isOpen
			// });
		});
	}
	// 

	// extra

	onChange(e, prop) {
        e.preventDefault()
        if (this.state.id === undefined) {
            let dv = this.state.data
            dv[prop] = e.target.value
            this.setState({ data: dv })
        } else {
            let dv = this.state.data.inventory
            dv[prop] = e.target.value
            this.setState({ data: dv })
        }

	}

	handleDropdown(e, prop){
		let dv = this.state.data
		dv[prop] = e !== null ? e : 0

		if(prop === 'program_education'){
			dv['grade_name'] = ""	
			dv['grade_level'] = ""

			this.getGradeName();
		
		} else if(prop === 'grade_name'){
			dv['grade_level'] = ""

			this.getGradeLevel();
		}

		this.setState({ data: dv })
	}

	getGradeLevel(){
		let self = this
		axios.request({
            url: `${apiPaymentHost}/payments/get_list?slug=grade_level&program=${this.state.data.program_education.value}&name=${this.state.data.grade_name.value}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        }).then(function(response){
			let grade_levels = []
			for (var i in response.data.data.list) {
				const datum = response.data.data.list[i]
				grade_levels.push({ value: datum.key, label: datum.label })
			}
			self.setState({
				grade_level: grade_levels,
			})

		}).catch(function (error) {
			// refresh()
			console.log("here")
		});
	}

	getGradeName(){
		let self = this
		axios.request({
            url: `${apiPaymentHost}/payments/get_list?slug=grade_name&type=${this.state.data.program_education.value}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        }).then(function(response){
			let grade_names = []
			for (var i in response.data.data.list) {
				const datum = response.data.data.list[i]
				grade_names.push({ value: datum.key, label: datum.label })
			}
			self.setState({
				grade_name: grade_names,
			})

		}).catch(function (error) {
			// refresh()
			console.log("here")
		});
	}

	//

    render() {
		return (
    		<div className={style.profilePage} >
				<NavbarMainPayment />
				<div className={style.mainContent}>
					<div className="container">
						<div class={style.admission_content}>
							<div class="row">
								<div class="col-sm-12">
									<h1 class={style.label_header}>Create Data Master</h1>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class={style.admission_border}>
										<div class="row">
											<div class="col-sm-3">
												<label className={style.labelDate}>Education Program</label>
												<div className="input-group date col-sm-12">
													<Select
														className={style.selectCategory}
														value={this.state.data.program_education}
														onChange={(e) => this.handleDropdown(e, 'program_education')}
														options={this.state.edu_list}
													/>
												</div>
											</div>
											<div class="col-sm-3">
												<label className={style.labelDate}>Grade</label>
												<div className="input-group date col-sm-12">
													<Select
														className={style.selectCategory}
														value={this.state.data.grade_name}
														onChange={(e) => this.handleDropdown(e, 'grade_name')}
														options={this.state.grade_name}
													/>
												</div>
											</div>
											<div class="col-sm-3">
												<label className={style.labelDate}>Level</label>
												<div className="input-group date col-sm-12">
													<Select
														className={style.selectCategory}
														value={this.state.data.grade_level}
														onChange={(e) => this.handleDropdown(e, 'grade_level')}
														options={this.state.grade_level}
													/>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-9">
												<div class={style.margin_top_2}>
													<label className={style.label} >Payment Title</label>
													<div className={style.formDisc}>
														<input type="text" className="form-control" value={this.state.data.payment_title} onChange={(e) => this.onChange(e, 'payment_title')} />
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-9">
												<div class={style.margin_top_2}>
													<label className={style.label} >Fee Type</label>
													<div className="input-group date col-sm-12">
														<Select
															className={style.selectCategory}
															value={this.state.data.fee_type}
															onChange={(e) => this.handleDropdown(e, 'fee_type')}
															options={this.state.fee_types}
														/>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-9 ">
												<div class={style.margin_top_2}>
													<label className={style.label} >Description</label>
													<div className={style.formDisc}>
														<input type="text" className="form-control" value={this.state.data.discription} onChange={(e) => this.onChange(e, 'description')} />
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-9 ">
												<div class={style.margin_top_2}>
													<label className={style.label} >Amount Fee</label>
													<div>
														<Cleave
															options={{
																numeral: true,
																numeralThousandsGroupStyle: 'thousand'
															}}
															className="form-control"
															value={this.state.data.amount}
															onChange={(e) => this.onChange(e, 'amount')} />
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-9">
											<button type="submit" className={style.btn_addmission} onClick={(e) => { this.submitForm(e) }} >Submit</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div>
					<Swal show={this.state.swal} >
						<div>
							<img src={Check} alt="check" className={style.Path} />
							<p className={style.swal}>New item has been added.</p>
						</div>
					</Swal>
				</div>
			</div>
    	)
    }
}
export default New;