export { default as Dashboard } from './inventory/Dashboard/MainPage';
export { default as BookList } from './inventory/Dashboard/BookList';
export { default as BookDetail} from './inventory/Dashboard/BookDetail';
export { default as InventoryManagement } from './inventory/Dashboard/InventoryManagement';
export { default as AddBook } from './inventory/Dashboard/AddBook';
export { default as Navbar } from './'
export { default as App } from './App/App'; 
export { default as Register } from './inventory/Register/Register';
export { default as NotFound } from './NotFound/NotFound';
export { default as Login } from './inventory/Login/Login';
export { default as Validate } from './inventory/Register/Validate-reg';
export { default as Profile  } from './inventory/Profile/Profile';
export { default as Announcement } from './Announcement/Annoucement';       

// for payment

export { default as Transaction } from './Payment/transaction';
export { default as Refund } from './Refund/refund';
export { default as Registration } from './Registration/registration';
export { default as CreatePayment } from './Payment/create-payment';
export { default as DetailPayment } from './Payment/detail-payment';
export { default as DetailRegistration } from './Registration/detailRegistration';
export { default as CreateRegistration  } from './Registration/createRegistration';
export { default as DetailRefund } from './Refund/detailRefund';
export { default as DetailGroup  } from './Payment/detail-group';
export { default as PaymentAll  } from './Payment/payment-all';
export { default as Admission } from './Admission/admission';
export { default as AdmissionNew } from './Admission/new';
export { default as AdmissionApplicant } from './Admission/applicant';
export { default as AdmissionDetail } from './Admission/detail';