import React, { Component } from 'react';
import { NavbarMainPayment } from '../../components/Navbar/navbar';
import { Title } from '../../styles/pay';
import Sidebar from '../../components/Layout/layoutInventory';
import styled from 'styled-components';
import axios from 'axios';
import { apiPaymentHost } from './../../utils/constant'
import breakpoint from "styled-components-breakpoint";
import Modal from './../../components/Modal/refund'
import './../../styles/global/Layout.css'
import { formatPrice, formatDate } from './../../utils/helper'
import Swal from './../../components/Modal/swal';
import Books from './../../styles/assets/user.svg';
import Check from './../../styles/assets/check.png';
import {
    Form, Upload, Icon
} from 'antd';
import Bullet from './../../styles/assets/bullet.png'
const style = require('./../../styles/profile/profile.css');



class transaction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: '1',
            type: "payment",
            swal: false,
            reason: '',
            height: '11em',
            data: {
                invoices: [],
                user: {
                    full_name: "",
                    student: {
                        parents: []
                    }
                }
            }

        }
        this.toggle = this.toggle.bind(this);
        this.changePage = this.changePage.bind(this);
        this.changePageDetail = this.changePageDetail.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.generateParent = this.generateParent.bind(this);
        this.onSubmit = this.onSubmit.bind(this)
        this.normFile = this.normFile.bind(this)
        this.decline = this.decline.bind(this)
        this.handlepayment = this.handlepayment.bind(this)

    }
    componentDidMount() {
        let self = this
        var url;
        url = `${apiPaymentHost}/refunds/student_detail?refund_id=${this.props.match.params.id}`
        console.log(url, "thisurl")
        axios.request({
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                console.log(response.data.data)
                self.setState({
                    data: response.data.data
                })
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }
    normFile(e) {

        if (Array.isArray(e)) {
            return e;

        }
        this.setState({ height: '16em' })
        return e && e.fileList;


    }
    handlepayment(e,id){
        this.props.history.push({
          pathname: '/detail-group-payment-all/' + id,
          state: { id: id}
        })
    }
    sendUrl() {
        setTimeout(() => {
            this.setState({
                swal: true
            })
        }, 500)

        setTimeout(() => {
            window.location.reload();
            this.setState({
                swal: false
            })
        }, 2000)


    }
    onSubmit(e) {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            var bodyFormData = new FormData();
            bodyFormData.append('photo', values.upload[0].originFileObj)

            e.preventDefault()
            let self = this
            const refresh = () => {
                setTimeout(() => {
                    self.setState({ isOpen: false })
                }, 200)
                this.sendUrl()
                this.getData()
            }
            axios({
                url: `${apiPaymentHost}/refunds/done?refund_id=${this.props.match.params.id}`,
                headers: {
                    'Content-Type': 'application/json'
                },
                method: 'post',
                data: bodyFormData
            })
                .then(function (response) {
                    refresh()


                })
                .catch(function (error) {

                });
        })
    }
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    onChangeTextArea(e) {
        this.setState({
            reason: e.target.value
        })
    }

    changePage() {
        this.props.history.push('/refund-transaction')
    }

    changePageDetail() {
        this.props.history.push('/detail-payment')
    }
    formatPrice(price) {
        return new Intl.NumberFormat("ID", {
            style: "currency",
            currencyDisplay: "symbol",
            currency: "IDR"
        }).format(price);
    }
    toggleModal(e) {
        this.setState({
            isOpen: !this.state.isOpen,
            // receipt: number,
            // title: title,
            // status: status,
            // author: author,
            // id_circulation: id

        });
        // this.getBorrowedName(user_id)

    }

    generateParent(array) {
        console.log(array, "array")
        let parents = []
        for (let i in array) {
            const data = array[i].user.full_name
            parents.push(data)
        }
        let name = parents.join(",")
        const tmp = (
            <h5><strong>Parent Name :</strong> {name} </h5>
        )
        return tmp


    }


    decline(e) {
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.sendUrl()
            this.getData()
        }
        axios({
            url: `${apiPaymentHost}/refunds/decline?refund_id=${this.props.match.params.id}`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            data: {
                "reason": this.state.reason
            }
        })
            .then(function (response) {
                refresh()


            })
            .catch(function (error) {

            });
    }


    render() {
        const boxs = {
            // marginTop: '-7em',
            height: this.state.height
        }

        const Selector = styled.div`
            margin: 0 0.1rem;
            border-radius: 4px;
            padding: 0.2rem 1.2rem;
            border: solid 1px;
            font-size: 1.2rem;
            margin-bottom: 0.3rem;
            
            ${breakpoint("sm")`
                font-size: 0.9rem;
                padding: 0.2rem 1rem;
            `}
            `;
        const Back = styled(Selector)`
            width: 120px;
            height: 44px;
            margin-top: 10%;
            margin-left : 10%;
            border: solid 1px #054ee1;
            background-color: #ffffff;
            cursor: pointer;
            color: #054ee1;
            font-family: Nunito Sans;
            font-size : 18px;
            
            span {
                margin-right: 1rem;
                margin-top : 1rem;
            }    
            
        `;

        const Stop = styled(Selector)`
            width: 140px;
            height: 40px;
            margin-top: 5%;
            border: solid 1px #054ee1;
            background-color: #054ee1;
            
            cursor: pointer;
            color: #ffffff;
            font-family: Nunito Sans;
            font-size : 18px;
            
            p {
                margin-left: 3rem;
                margin-top : 0.5rem;
            }     
            
        `;
        const Export = styled.button`
            width: 140px;
            height: 40px;
            border-radius: 4px;
            margin-top: -10%
            margin-left : 40%;
            border: solid 1px #fe634f;
            background-color: #fe634f;
            cursor: pointer;
            position:absolute;
            color: #ffffff;
            font-family: Nunito Sans;
            font-size : 18px;
            
            p {
                margin-left: 3rem;
                margin-top : 0.5rem;
            }    
            
        `;

        const TitlePage = styled.div`
        
           div {
                margin-left: 10%;
                h1 {
                    height: 20px;
                    font-family: Nunito Sans;
                    font-size: 25px;
                    font-weight: normal;
                    font-style: normal;
                    font-stretch: normal;
                    line-height: normal;
                    letter-spacing: normal;
                    color: #233539;
                
               }

               h2 {
                width: 91px;
                height: 20px;
                font-family: Nunito Sans;
                font-size: 15px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: normal;
                letter-spacing: normal;
                color: #233539;
                text-decoration: underline;
               }
               div {
                   margin-left : 5%;
                   h5 {
                        padding-top : 5%;
                        padding-top : 1%;
                        margin: 0.5em;
                        height: 20px;
                        font-family: Nunito Sans;
                        font-size: 15px;
                        font-weight: normal;
                        font-style: normal;
                        font-stretch: normal;
                        line-height: normal;
                        letter-spacing: normal;
                        color: #1f1f1f;
                        text-align : justify
                    
                   }
                   h4 {

                        height: 20px;
                        opacity: 0.9;
                        font-family: Nunito Sans;
                        font-size: 15px;
                        font-weight: bold;
                        font-style: normal;
                        font-stretch: normal;
                        line-height: normal;
                        letter-spacing: normal;
                        color: #273d52;
                   }
               }

           }
        `

        const LeftPage = styled.div`
             margin-top : 43%;
             
            div {
                margin-left: 0%;
                h1 {
                    height: 20px;
                    font-family: Nunito Sans;
                    font-size: 25px;
                    font-weight: normal;
                    font-style: normal;
                    font-stretch: normal;
                    line-height: normal;
                    letter-spacing: normal;
                    color: #233539;
                
               }

               h2 {
                width: 91px;
                height: 20px;
                font-family: Nunito Sans;
                font-size: 15px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: normal;
                letter-spacing: normal;
                color: #233539;
                text-decoration: underline;
               }
               div {
                margin:1px;
                    h5 {
                        height: 20px;
                        font-family: Nunito Sans;
                        font-size: 15px;
                        font-weight: normal;
                        font-style: normal;
                        font-stretch: normal;
                        line-height: 1;
                        letter-spacing: normal;
                        color: #1f1f1f;
                        text-align : justify
                    
                   }
                   h4 {
                    
                      
                        height: 20px;
                        opacity: 0.9;
                        font-family: Nunito Sans;
                        font-size: 15px;
                        font-weight: bold;
                        font-style: normal;
                        font-stretch: normal;
                        line-height: normal;
                        letter-spacing: normal;
                        color: #273d52;
                   }
               }

           }
        `

        const RightPage = styled.div`
            div {
                margin-top:50%;
              
                h4 {
                    padding-right:2em;
                        height: 20px;
                        opacity: 0.9;
                        font-family: Nunito Sans;
                        font-size: 15px;
                        font-weight: normal;
                        font-style: normal;
                        font-stretch: normal;
                        line-height: normal;
                        letter-spacing: normal;
                        color: #273d52;
                    }

           }
        `

        const { getFieldDecorator } = this.props.form;
        console.log(this.state.data.user.full_name, "student")
        return (
            <div>
                <NavbarMainPayment status={this.state.type} />
                <Title>Transaction Management</Title>
                <Sidebar>
                    <div class="col-md-4" >
                        <Back onClick={this.changePage} ><span className="glyphicon glyphicon-arrow-left"></span> Back</Back>
                        <TitlePage>
                            <div>
                                <h1>Detail - School Bus</h1>
                                <div className="boxleft" style={margin}>
                                    <div >
                                        <h5><strong>Invoice(s) List</strong> </h5>
                                    </div>
                                    <ul class="list-inline" style={list}>
                                        {
                                            this.state.data.invoices.map(function (j, k) {
                                                return <li class="list-inline-item" style={li}><img src={Bullet} alt="bullet" />{j.invoice_batch.title}</li>
                                            })
                                        }

                                    </ul>
                                    <div >
                                        <h5><strong style={invoiceTotal}>Invoice Total</strong> </h5>
                                    </div>
                                    <div >
                                        <h5 style={invoiceTotal}>{formatPrice(this.state.data.amount_fee)}</h5>
                                    </div>
                                    <hr style={Line} />
                                    <div >
                                        <h5><strong style={invoiceTotal}>Paid Total</strong> </h5>
                                    </div>
                                    <div >
                                        <h5 style={invoiceTotal}>{formatPrice(this.state.data.amount_fee)}</h5>
                                    </div>

                                    {
                                        this.state.data.invoices.map(function (j, k) {
                                            return <div >
                                                <h5 style={view} onClick={(e) => {this.handlepayment(e,j.id)}} >View detail transaction</h5>
                                            </div>
                                        },this)
                                    }

                                </div>

                            </div>
                        </TitlePage>

                    </div>
                    <div class="col-md-4" >
                        <LeftPage>
                            <div className="boxleft" style={marginCenter} >
                                <div  >
                                    <h5><strong>NIS :</strong> {this.state.data.user.student.nis} </h5>
                                </div>
                                <div>
                                    <h5><strong>Student Name :</strong> {this.state.data.user.full_name} </h5>
                                </div>
                                <div>
                                    <h5><strong>Degree :</strong> {this.state.data.user.student.grade_name} </h5>
                                </div>
                                <div>
                                    <h5><strong>Grade :</strong> {this.state.data.user.student.grade_level} </h5>
                                </div>
                                <div>
                                    <h5><strong>Refund Date :</strong> {formatDate(this.props.location.state.date)} </h5>
                                </div>
                                <div>
                                    {this.generateParent(this.state.data.user.student.parents)}
                                </div>
                                <div>
                                    <h5><strong>Reason :</strong> {this.state.data.user_reason}.</h5>
                                </div>
                            </div>
                        </LeftPage>
                        {
                            this.state.data.payment_status !== 'done' ?
                                <div>
                                    <Stop onClick={(e) => { this.onSubmit(e) }} ><p>Done</p></Stop>
                                    <Export onClick={(e) => { this.toggleModal(e) }}>Reject</Export></div> : ""
                        }

                    </div>
                    <div className="col-md-4" >
                        <RightPage>
                            <div>
                                <h4><strong>Account Name : </strong> {this.state.data.name_account}</h4>
                                <h4> <strong>Tranfer via : </strong> {this.state.data.bank_name} </h4>
                                <h4><strong>Acc. bank number :</strong> {this.state.data.bank_account} </h4>
                                <h4><strong>Request Refund : </strong>{this.formatPrice(this.state.data.amount_fee)} </h4>
                                <h4><strong>Status Refund : </strong>{this.state.data.payment_status} </h4>
                                <hr style={Line} />
                                {
                                    this.state.data.payment_status !== 'done' ? <h4><strong>Upload Evidance : </strong></h4> : <h4><strong> Evidance  </strong></h4>
                                }

                            </div>
                        </RightPage>

                        {
                            this.state.data.payment_status !== 'done' ?
                                <div className="border-photo" style={boxs}>
                                    <div className="dropbox" style={photo} >
                                        {getFieldDecorator('upload', {
                                            valuePropName: 'fileList',
                                            getValueFromEvent: this.normFile,
                                        })(
                                            <Upload.Dragger name="files" listType="picture">
                                                <p className="ant-upload-drag-icon">
                                                    <Icon type="camera" />

                                                </p>
                                                <p className="ant-upload-text">Upload Picture</p>
                                            </Upload.Dragger>
                                        )}
                                    </div>
                                </div> :
                                <div>
                                    <img src={this.state.data.photo} height="148px" width="250px" alt="resi" />
                                </div>
                        }

                    </div>



                </Sidebar>
                <Modal shouldCloseOnOverlayClick={true} show={this.state.isOpen} data={this.state.reason} onClosePress={() => this.toggleModal()} onClose={this.toggleModal} onReady={this.decline} >
                    <div>
                        {/* <Mdl>
                            <div style={textarea}>
                                <h5>Reason</h5>
                                <textarea className="form-control" value={this.state.reason} onChange={(e) => { this.onChangeTextArea(e) }} style={box} rows="4" cols="50" />
                            </div>
                        </Mdl> */}
                        <Rmv>
                            <div>
                                <img src={Books} alt="logo" width="34px" height="34px" />
                                <h1>{this.state.data.user.full_name}</h1>
                                <h2>{this.state.data.user.student.nis}</h2>
                                <h3>{this.state.data.user.student.program_name + '|' + this.state.data.user.student.grade_name + '|' + this.state.data.user.student.grade_level}  </h3>
                                <br />
                                <h5>Reason</h5>
                                <textarea className="form-control" value={this.state.reason} onChange={(e) => { this.onChangeTextArea(e) }} style={box} rows="4" cols="50" />

                            </div>
                        </Rmv>

                    </div>
                </Modal>
                <Swal show={this.state.swal} >
                    <div>
                        <img src={Check} alt="check" className={style.Path} />
                        <p className={style.swal}>New item has been added.</p>
                    </div>
                </Swal>
            </div>
        )
    }
}
export default Form.create()(transaction);


const view = {
    marginTop: '7em',
    marginLeft: '3em',
    cursor: 'pointer',
    color: '#3680ce',
    position: 'relative'
}

const li = {
    marginRight: '14.4%'
}
const margin = {
    marginTop: '4em',
    marginLeft: '-0.1em'
}

const invoiceTotal = {
    marginLeft: '12em'
}


const marginCenter = {
    marginTop: '12.5em',
    marginLeft: '-0.1em',
    padding: '3em'

}

const photo = {
    width: '17.2em',
    height: '10em',
    paddingTop: '1em',
    marginLeft: '1em'
}

const Line = {
    marginLeft: '-0.4em',
    width: '19.375em',
    height: '1px',
    color: 'solid 1px #e5e5e5'
}

const list = {
    marginLeft: "5%",
    marginRight: "15%"
}



const box = {
    height: '80%'
}




const Rmv = styled.div`
    div{
        width:'30em';
        height: '250em';
        div{
            margin-bottom:10px;
            width: 335px;
            height: 121px;
            box-shadow: 0 11px 14px -10px #e5eced;
            border: solid 1px #e5eced;
            background-color: #ffffff;
            
            img {
                margin-left : 1em;
                margin-bottom : 2em;
                display: block;
                border-radius: 4%;
            }

            h5{
                margin-top : -37%;
                margin-left: 25%;
                height: 18px;
                opacity: 0.6;
                font-family: Nunito Sans;
                font-size: 13px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: normal;
                letter-spacing: normal;
                color: #273d52;
            }
            
            h4{
                margin-top : -0%;
                margin-left: 25%;
                width: 500px;
                height: 18px;
                opacity: 0.6;
                font-family: Nunito Sans;
                font-size: 13px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: normal;
                letter-spacing: normal;
                color: #273d52;
            }

            h3{
                margin-top : 3%;
                margin-left: 25%;
                width: 77px;
                height: 18px;
                opacity: 0.6;
                font-family: Nunito Sans;
                font-size: 13px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: normal;
                letter-spacing: normal;
                color: #273d52;
            }

        }
        img {
            margin-left: 2%;
            margin-top: 2%;
            width: 34px;
            height: 34px;
            border: solid 0.55px #d6d6d6;
            border-radius: 50%;
        }
        h1{
            margin-top: -11%;
            margin-left : 15%;
            width: 360px;
            height: 27px;
            font-family: Nunito Sans;
            font-size: 17px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #759bfa;
        }

        h2 {
            margin-top: -6%;
            margin-left : 15%;
            width: 198px;
            height: 25px;
            opacity: 0.8;
            font-family: Nunito Sans;
            font-size: 14px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #759bfa;
        }

        h3 {
            margin-top: -2%;
            margin-left : 15%;
            font-family: Nunito Sans;
            width: 198px;
            height: 18px;
            font-size: 13px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #759bfa;
        }

        p {
            height: 28px;
            font-family: NunitoSans;
            font-size: 13px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 2.15;
            letter-spacing: normal;
            color: #323c47;
        }
        h4 {
            height: 20px;
            font-family: Nunito Sans;
            font-size: 15px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #1f1f1f;
        }

        h5 {
            height: 15px;
            font-family: Nunito Sans;
            font-size: 11px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: 0.1px;
            color: #323c47;
        }

            html, body, .container {
            height: 100%;
            }
            textarea.form-control {
                height: 300%;
                width: 331px;
                height: 198px;
                border-radius: 4px;
                border: solid 1px #e9eff4;
                background-color: #ffffff;
                font-family: NunitoSans;
                font-size: 15px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: 1.8;
                letter-spacing: -0.2px;
                color: #323c47;
            }
            }

`