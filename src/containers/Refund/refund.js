import React, { Component } from 'react';
import { NavbarMainPayment } from './../../components/Navbar/navbar';
import { Title } from './../../styles/pay';
import Sidebar from './../../components/Layout/layoutInventory';
import axios from 'axios';
import { apiPaymentHost } from './../../utils/constant';
import { Input, Select, DatePicker } from 'antd';
import Pagination from './../../components/pagination/index'
import { formatDate } from './../../utils/helper'

const Search = Input.Search;
const Option = Select.Option;
const { RangePicker } = DatePicker;


const style = require('./../../styles/dashboard/dashboard.css');

export default class transaction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: '1',
      type: "payment",
      data: {
        refunds: []
      },
      parents: [],
      subject: [
        { value: 'asc', label: 'Ascending' },
        { value: 'desc', label: 'Descending' },
      ],
      import: [
        { value: 'done', label: 'Done' },
        { value: 'pending', label: 'Pending' },
        { value: 'on_progress', label: 'On Progress' },
      ],
    }
    this.toggle = this.toggle.bind(this);
    this.onChangePage = this.onChangePage.bind(this);
    this.getData = this.getData.bind(this);
    this.generateParent = this.generateParent.bind(this)
    this.handlePageChange = this.handlePageChange.bind(this)
    this.findOne = this.findOne.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.onChangeDate = this.onChangeDate.bind(this)
  }
  componentDidMount() {
    this.getData()
  }
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  handleChange(value) {
    let type = 'status'
    let start_date = undefined
    let end_date = undefined
    this.getData(type, value, start_date, end_date)
  }

  findOne(value) {
    let type = 'keyword'
    let start_date = undefined
    let end_date = undefined
    this.getData(type, value, start_date, end_date)
  }

  onChangePage(id, e) {
    this.props.history.push({
      pathname: '/detail-refund/' + id,
      state: { id: id }
    })
  }

  formatPrice(price) {
    return new Intl.NumberFormat("ID", {
      style: "currency",
      currencyDisplay: "symbol",
      currency: "IDR"
    }).format(price);
  }

  onChangeDate(date, dateString){
    let type = 'date'
    let start_date = dateString[0]
    let end_date = dateString[1]
    let value = undefined

    this.getData(type,value, start_date,end_date)
  }

  getData(type,value, start_date, end_date) {
    console.log(type,value, start_date, end_date, "checking value")
    let self = this
    var url;
    if (value === undefined && type === undefined && start_date === undefined && end_date === undefined ) {
      url = `${apiPaymentHost}/refunds/index`
    }else if(value === undefined && type === 'date' && start_date !== undefined && end_date !== undefined ){
      url = `${apiPaymentHost}/refunds/index?start_date=${start_date}&end_date=${end_date}`
    } else if(value !== undefined && type === 'status' && start_date === undefined && end_date === undefined ) {
      url = `${apiPaymentHost}/refunds/index?payment_status=${value}`
    }else if (value !== undefined && type === 'keyword' && start_date === undefined && end_date === undefined ){
      url = `${apiPaymentHost}/refunds/index?key_name=${value}`
    }
    console.log(url)
    axios.request({
      url: url,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'GET'
    })
      .then(function (response) {
        response.data.data.refunds.map(function (k, l) {
          return response.data.data.refunds[l].no = (l + 1)
        })
        self.setState({
          data: response.data.data,
          page: response.data.meta.pagination.page,
          total_page: response.data.meta.pagination.total_page,
          total_data: response.data.meta.pagination.total_data
        })
      })
      .catch(function (error) {
        // refresh()
        console.log("here")
      });
  }

  generateParent(array) {
    let parents = []
    for (let i in array) {
      const data = array[i].user.full_name
      parents.push(data)
      
    }
    let name = parents.join(",")
    const tmp = (
      <td className={style.valueTable} >{name}</td>
    )
    return tmp


  }
  handlePageChange(page) {

    let self = this
    var url = `${apiPaymentHost}/refunds/index?page=${page}`
    axios.request({
      url: url,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'GET'
    })
      .then(function (response) {
        response.data.data.refunds.map(function (k, l) {
          return response.data.data.refunds[l].no = (l + 1)
        })
        self.setState({
          data: response.data.data,
          page: response.data.meta.pagination.page,
          total_page: response.data.meta.pagination.total_page,
          total_data: response.data.meta.pagination.total_data
        })
      })
      .catch(function (error) {
        // refresh()
        console.log("here")
      });
  }

  render() {
    const color = {
      backgroundColor: '#edf5ff',
    }
    const margin = {
      marginTop: '2%',
      marginBottom: '2%'
    }
    const { page } = this.state


    return (
      <div>
        <NavbarMainPayment status={this.state.type} />
        <Title>Refund Management</Title>
        <Sidebar>
          <div className={style.boxTableInv}>
            <div style={margin}>
              <Search
                placeholder="Find student, program name and grade name"
                onSearch={value => this.findOne(value)}
                style={{ width: 350, marginRight: 20, height: 48, marginTop: '0.5em' }}
              />

              <Select
                showSearch
                style={{ width: 300, marginRight: 20 }}
                placeholder="Sory by status "
                optionFilterProp="children"
                onChange={this.handleChange}
                onFocus={this.handleFocus}
                onBlur={this.handleBlur}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              >
                {this.state.import.map(function (arr, i) {
                  return <Option value={arr.value}>{arr.label}</Option>
                })
                }
              </Select>

              <RangePicker onChange={this.onChangeDate} style={{ width: '25em' }} />


            </div>

            <table className="table table-bordered table-striped" style={margin}>
              <thead className="thead-dark" style={color}>
                <tr>
                  <th className={style.fontTable} scope="col">No</th>
                  <th className={style.fontTable} >Student</th>
                  <th className={style.fontTable} scope="col">Program</th>
                  <th className={style.fontTable} scope="col">Grade</th>
                  <th className={style.fontTable} scope="col">Refund Request Date</th>
                  <th className={style.fontTable} scope="col">Nominal</th>
                  <th className={style.fontTable} >Parent</th>
                  <th className={style.fontTable} >Status</th>
                </tr>
              </thead>
              <tbody>
                {
                  this.state.data.refunds.map(function (array, i) {
                    console.log(array.user.student)
                    return <tr>
                      <td className={style.valueTable} >{array.no}</td>
                      <td onClick={(e) => { this.onChangePage(array.id, e) }} className={style.popCol} style={culumn} >{array.user.full_name}</td>
                      <td className={style.valueTable} >{array.user.student === undefined ? 'N/A' : array.user.student.program_name}</td>
                      <td className={style.valueTable} >{array.user.student === undefined ? 'N/A' : array.user.student.grade_level}</td>
                      <td className={style.valueTable} >{array.created_at === null ? 'N/A' : formatDate(array.created_at, "isoDate")}</td>
                      <td className={style.valueTable} >{this.formatPrice(array.amount_fee)}</td>
                      {this.generateParent(array.user.student === undefined ? [] : array.user.student.parents)}
                      <td className={style.valueTable} >{array.payment_status}</td>
                    </tr>
                  }, this)
                }

              </tbody>
            </table>
            <div className={style.page}>
              <Pagination
                margin={2}
                page={page}
                count={Math.ceil(this.state.total_page)}
                onPageChange={this.handlePageChange}
              />


            </div>

          </div>

        </Sidebar>
      </div>
    )
  }
}

const culumn = {
  width: '15em'
}
