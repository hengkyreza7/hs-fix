import React, { Component } from 'react';
import { NavbarMainPayment } from './../../components/Navbar/navbar';
import { Title } from './../../styles/pay';
import Sidebar from './../../components/Layout/layoutInventory';
import styled from 'styled-components';
import breakpoint from "styled-components-breakpoint";
import axios from 'axios';
import { apiPaymentHost, apiHost } from './../../utils/constant'
import Pagination from './../../components/pagination/index';
import Badge from './../../styles/assets/group-4.png'
import { Tabs, Input, Select, DatePicker, Button } from 'antd';
import Seo from './../../components/Seo/Seo';
import { changeValue, formatDate } from './../../utils/helper'


const TabPane = Tabs.TabPane;
const Search = Input.Search;
const Option = Select.Option;
const { RangePicker } = DatePicker;
const style = require('./../../styles/dashboard/dashboard.css');



export default class transaction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: this.props.match.params.id,
      type: "payment",
      mode: 'top',
      data: {
        payment_batchs: []
      },
      meta: {
        pagination: []
      },
      renderedUsers: [],
      page: 1,
      pageInvoice: 1,
      total_page_invoice: 10,
      invoice: {
        invoices: [],
        invoice_batch: [],
        payment_groups: [],

      },
      pageOfItems: [],
      pageOfInvoice: [],
      search: "",
      title: 'payment',
      subject: [
        { value: 'asc', label: 'Ascending' },
        { value: 'desc', label: 'Descending' },
      ],
      import: [
        { value: 'grade', label: 'Grade' },
        { value: 'specific', label: 'Specific' },
        { value: 'registration', label: 'Registration' },
      ],
      status: [
        { value: 'paid', label: 'Paid' },
        { value: 'pending', label: 'Pending' },
        { value: 'less', label: 'Less' },
        { value: 'over', label: 'Over' },
        { value: 'waiting', label: 'Waiting' },
      ]
    }
    this.getDataInvoice = this.getDataInvoice.bind(this)
    this.toggle = this.toggle.bind(this);
    this.group = this.group.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleChange = this.handleChange.bind(this)
    this.findOne = this.findOne.bind(this);
    this.getData = this.getData.bind(this);
    this.refund = this.refund.bind(this);
    this.handlePageChangePaymentGroup = this.handlePageChangePaymentGroup.bind(this)
    this.onChangeDate = this.onChangeDate.bind(this)
    this.onChangePageDueDate = this.onChangePageDueDate.bind(this)
    this.changePage = this.changePage.bind(this)
    this.findPayment = this.findPayment.bind(this)
    this.findByDueDate = this.findByDueDate.bind(this)
    this.handleStatus = this.handleStatus.bind(this)
    this.generatePaymentTitle = this.generatePaymentTitle.bind(this)
    this.callback = this.callback.bind(this)

  }

  componentDidMount() {
    this.getData()
    this.getDataInvoice()

    let self = this
    axios.request({
      url: `${apiHost}/finances/${localStorage.getItem("_userID")}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'GET'
    })
      .then(function (response) {
        self.setState({
          user: response.data.data,
        })
        localStorage.setItem("_username", response.data.data.full_name)

      })
      .catch(function (error) {
        // refresh()
        console.log("here")
      });



  }

  findPayment(value) {
    let type = 'participant'
    this.getDataInvoice(type, value)
  }

  handleStatus(value) {
    let type = 'status'
    let start_paid = undefined
    let end_paid = undefined

    this.getDataInvoice(type, value, start_paid, end_paid)
  }


  onChangeDate(date, dateString) {
    console.log("startede", dateString[0], dateString[1])
    let startdate = dateString[0]
    let enddate = dateString[1]
    let value = undefined
    let type = undefined
    this.getData(value, type, startdate, enddate)


  }
  onChangePageDueDate(date, dateString) {
    const value = dateString
    const type = 'duedate'
    this.getData(type, value)
  }

  changePage() {
    this.props.history.push({
      pathname: '/create-payment'
    })
  }

  findByDueDate(date, dateString) {
    let type = 'paid'
    let start_paid = dateString[0]
    let end_paid = dateString[1]
    let value = undefined

    this.getDataInvoice(type, value, start_paid, end_paid)
  }

  getDataInvoice(type, value, start_paid, end_paid) {
    let self = this
    let url;
    if (type === undefined && value === undefined) {
      url = `${apiPaymentHost}/payments/invoice_all`
    } else if (type === 'participant' && value !== undefined) {
      url = `${apiPaymentHost}/payments/invoice_all?full_name=${value}`
    } else if (type === 'paid' && value === undefined && start_paid !== undefined && end_paid !== undefined) {
      url = `${apiPaymentHost}/payments/invoice_all?start_paid_date=${start_paid}&end_paid_date=${end_paid}`
    } else if (type === 'status' && value !== undefined && start_paid === undefined && end_paid === undefined) {
      url = `${apiPaymentHost}/payments/invoice_all?payment_status=${value}`
    }
    axios.request({
      url: url,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'GET'
    })
      .then(function (response) {
        self.setState({
          invoice: response.data.data,
          pageInvoice: response.data.meta.pagination.page,
          total_page_invoice: response.data.meta.pagination.total_page
        })
      })
      .catch(function (error) {
        // refresh()
        console.log("here invoice")
      });
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
      this.handlePageChangePaymentGroup()
    }

  }

  group(e, id) {
    e.preventDefault()
    this.props.history.push({
      pathname: '/detail-group-payment-all/' + id,
      state: { id: id, type: 'paid' }
    })
  }
  formatPrice(price) {
    return new Intl.NumberFormat("ID", {
      style: "currency",
      currencyDisplay: "symbol",
      currency: "IDR"
    }).format(price);
  }
  refund(e, id) {
    console.log("awawa", id)
    this.props.history.push({
      pathname: '/detail-group-payment/' + id,
      state: { id: id }
    })
  }

  handleModeChange(e) {
    const mode = e.target.value;
    this.setState({ mode });
  }
  handleChange(value) {
    var type = ""
    if (value === 'asc' || value === 'desc') {
      type = "sort"
    } else {
      type = "import"
    }
    this.getData(value, type)
  }
  handleBlur() {
    console.log('blur');
  }

  handleFocus() {
    console.log('focus');
  }

  toggleModal(id, e) {
    e.preventDefault()
    this.props.history.push({
      pathname: '/detail-payment/' + id,
      state: { id: id, tab: 2 }
    })
  }

  handlePageChange(page) {

    let self = this
    axios.request({
      url: `${apiPaymentHost}/payments/transaction_index?page=${page}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'GET'
    })
      .then(function (response) {
        self.setState({
          data: response.data.data,
          page: response.data.meta.pagination.page,
          total_page: response.data.meta.pagination.total_page
        })
      })
      .catch(function (error) {
        // refresh()
        console.log("here invoice")
      });


  }

  handlePageChangePaymentGroup(page) {

    const renderedUsers = this.state.data.payment_batchs.slice((page - 1) * 2, (page - 1) * 2 + 2);
    this.setState({ page: page, renderedUsers });
    var self = this
    axios.request({
      url: `${apiPaymentHost}/payments/invoice_all?page=${page}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'GET'
    })
      .then(function (response) {
        self.setState({
          invoice: response.data.data,
        })
      })
      .catch(function (error) {
        // refresh()
        console.log("here invoice")
      });


    // this.getData()

  }



  findOne(value) {
    this.setState({
      search: value
    })
    let type = 'find'
    this.getData(value, type)
  }


  getData(value, type, startdate, enddate) {
    let self = this
    var url;
    if (value === undefined && startdate === undefined && enddate === undefined) {
      url = `${apiPaymentHost}/payments/transaction_index?page=1`
    } else if (startdate !== undefined && enddate !== undefined) {
      url = `${apiPaymentHost}/payments/transaction_index?start_date=${startdate}&end_date=${enddate}`
    } else if (type === 'duedate' && startdate === undefined && enddate === undefined) {
      url = `${apiPaymentHost}/payments/transaction_index?due_date=${value}`
    } else if (type !== 'sort' && type !== 'find' && value !== undefined && startdate === undefined && enddate === undefined) {
      url = `${apiPaymentHost}/payments/transaction_index?import_type=${value}`
    } else if (type === 'find' && value !== undefined && startdate === undefined && enddate === undefined) {
      url = `${apiPaymentHost}/payments/transaction_index?title=${value}`
    } else {
      url = `${apiPaymentHost}/payments/transaction_index?title=${value}`
    }
    console.log(url, "thisurl")
    axios.request({
      url: url,
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'GET'
    })
      .then(function (response) {
        self.setState({
          data: response.data.data,
          page: response.data.meta.pagination.page,
          total_page: response.data.meta.pagination.total_page
        })
      })
      .catch(function (error) {
        // refresh()
        console.log("here")
      });
  }
  callback(key) {
    window.history.pushState('payment-transaction/', "")
    // this.props.history.push('payment-detail')
  }

  generatePaymentTitle(invoices) {
    let payment_title = []
    let lengthArray = invoices.invoices.length
    if (lengthArray === 2) {
      payment_title = invoices.invoices[0].invoice_batch.payment_title
    }
    for (var i in invoices.invoices) {
      let data = invoices.invoices[i].invoice_batch.payment_title
      payment_title.push(data)
    }

    if (invoices.invoices.length === 2) {
      const tmp = (
        <td className={style.popCol} onClick={(e) => { this.group(e, invoices.id) }} >{changeValue(payment_title).join(" & ")}</td>
      )
      return tmp
    } else if (invoices.invoices.length > 2) {
      console.log(payment_title)
      const tmp = (

        <td className={style.popCol} onClick={(e) => { this.group(e, invoices.id) }} >{changeValue(payment_title[0])} & {(lengthArray - 1)}  others  </td>
      )
      return tmp
    } else {
      const tmp = (
        <td className={style.popCol} onClick={(e) => { this.group(e, invoices.id) }} >{changeValue(payment_title).join(" & ")}</td>
      )
      return tmp
    }

  }


  render() {
    const { page } = this.state
    const color = {
      backgroundColor: '#edf5ff',
    }
    const margin = {
      marginTop: '1em',
      marginBottom: '2%'
    }

    const Selector = styled.div`
      margin: 0 0.1rem;
      border-radius: 4px;
      padding: 0.2rem 1.2rem;
      border: solid 1px;
      font-size: 1.2rem;
      margin-bottom: 0.3rem;

      ${breakpoint("sm")`
        font-size: 0.9rem;
        padding: 0.2rem 1rem;
      `}
    `;

    const AddFilter = styled(Selector)`
      width: 161px;
      height: 44px;
      margin-left : 83.5%;
      margin-top: -2.2%;
      border-color: #4274a8;
      background-color: #4274a8;
      cursor: pointer;
      color: #ffffff;
      font-family: Nunito Sans;

      span {
        margin-right: 1rem;
        margin-top : 1rem;

      }
      :hover {
        border-radius: 4px;
        color: white;
      }
    `;
    const bg = {
      backgroundColor: "yellow",
      color: 'black',
      fontFamily: 'Nunito Sans',
      border: "yellow"
    }
    console.log(this.state.invoice)
    return (
      <div>
        <Seo title={this.state.title} />
        <NavbarMainPayment status={this.state.type} />
        <Title>Payment Management</Title>
        <AddFilter onClick={this.changePage} ><span className="glyphicon glyphicon-plus"></span> Create Payment</AddFilter>
        <Sidebar>
          <div className={style.boxTableInv}>
            <Tabs defaultActiveKey={this.state.activeTab === '1' ? '1' : '2'} onChange={this.callback} >
              <TabPane tab="School Billing" key="1">
                <div>
                  <Search
                    placeholder="Find your Refund"
                    onSearch={value => this.findOne(value)}
                    style={{ width: 320, marginRight: 20, height: 48, marginTop: '0.5em' }}
                  />


                  <DatePicker onChange={this.onChangePageDueDate} placeholder="Select Due Date" style={{ width: 200, marginRight: 20 }} />

                  <Select
                    showSearch
                    style={{ width: 200, marginRight: 20 }}
                    placeholder="Import from "
                    optionFilterProp="children"
                    onChange={this.handleChange}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {this.state.import.map(function (arr, i) {
                      return <Option value={arr.value}>{arr.label}</Option>
                    })
                    }
                  </Select>
                  <RangePicker onChange={this.onChangeDate} style={{ width: '20em' }} />

                </div>

                <table className="table table-bordered table-striped" style={margin}>
                  <thead className="thead-dark" style={color}>
                    <tr>
                      <th className={style.fontTable} scope="col">No</th>
                      <th className={style.fontTable} scope="col">Invoice Code</th>
                      <th className={style.fontTable} scope="col">Payment Title</th>
                      <th className={style.fontTable} scope="col">Participant</th>
                      <th className={style.fontTable} scope="col">Import From</th>
                      <th className={style.fontTable} scope="col">Fee</th>
                      <th className={style.fontTable} scope="col">Due Date</th>
                      <th className={style.fontTable} >Date Created</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.state.data.payment_batchs.map(function (array, i) {
                        return <tr key={i}>
                          <td className={style.valueTable} >{i + 1}</td>
                          <td className={style.popCol} onClick={(e) => { this.toggleModal(array.id, e) }}><a> {array.batch_code} </a></td>

                          {
                            array.stoped !== 1 ? <td className={style.valueTable} >{changeValue(array.payment_title)}</td> :
                              <td className={style.valueTable} >{changeValue(array.payment_title)} <img src={Badge} alt="Badge" /></td>
                          }

                          <td className={style.valueTable} >{array.participant_count}</td>
                          <td className={style.valueTable} >{array.import_type}</td>
                          <td className={style.valueTable} >{this.formatPrice(array.gross_fee)}</td>
                          <td className={style.valueTable} >{formatDate(array.due_date, "isoDate")}</td>
                          <td className={style.valueTable} >{array.created_at === null || array.created_at === '' ? 'N/A' : formatDate(array.created_at, "isoDate")}</td>
                        </tr>
                      }, this)
                    }

                  </tbody>
                </table>
                <div className={style.page}>
                  <Pagination
                    margin={2}
                    page={page}
                    count={Math.ceil(this.state.total_page)}
                    onPageChange={this.handlePageChange}
                  />

                </div>
              </TabPane>
              <TabPane tab="Transaction List" key="2">
                <div>
                  <Search
                    placeholder="Find participant"
                    onSearch={value => this.findPayment(value)}
                    style={{ width: 320, marginRight: 20, height: 48, marginTop: '0.5em' }}
                  />
                  <RangePicker onChange={this.findByDueDate} style={{ width: '20em', marginRight: 20 }} />

                  <Select
                    showSearch
                    style={{ width: 200, marginRight: 20 }}
                    placeholder="Sort by status "
                    optionFilterProp="children"
                    onChange={this.handleStatus}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {this.state.status.map(function (arr, i) {
                      return <Option value={arr.value}>{arr.label}</Option>
                    })
                    }
                  </Select>


                </div>
                <table className="table table-bordered table-striped" style={margin}>
                  <thead className="thead-dark" style={color}>
                    <tr>
                      <th className={style.fontTable} scope="col">No</th>
                      <th className={style.fontTable} scope="col">Payment Title</th>
                      <th className={style.fontTable} scope="col">Participant</th>
                      <th className={style.fontTable} scope="col">Status</th>
                      <th className={style.fontTable} scope="col">Paid Date</th>
                      <th className={style.fontTable} scope="col">Paid Nominal</th>
                      <th className={style.fontTable} >Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.state.invoice.payment_groups.map(function (arr, k) {
                        return <tr>
                          <td className={style.valueTable} >{k + 1}</td>
                          {
                            this.generatePaymentTitle(arr)
                          }
                          <td className={style.valueTable} >{arr.user.full_name}</td>
                          <td className={style.valueTable} >{arr.payment_status}</td>
                          <td className={style.valueTable} >{formatDate(arr.paid_date, "isoDate")}</td>
                          <td className={style.valueTable} >{this.formatPrice(arr.total_amount_fee)}</td>
                          <td className={style.valueTable} >
                            {
                              arr.refund === undefined ? '' : <Button type="primary" onClick={(e) => { this.refund(e, arr.id) }} style={bg} size="small">Refund</Button>
                            }

                          </td>
                        </tr>
                      }, this)
                    }
                  </tbody>
                </table>
                <div className={style.page}>
                  <Pagination
                    margin={2}
                    page={this.state.pageInvoice}
                    count={Math.ceil(this.state.total_page_invoice)}
                    onPageChange={this.handlePageChangePaymentGroup}
                  />


                </div>
              </TabPane>
            </Tabs>




          </div>

        </Sidebar>
      </div>
    )
  }
}
