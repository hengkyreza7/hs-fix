import React, { Component } from 'react';
import Swal from './../../components/Modal/swal';
import axios from 'axios';
import { apiPaymentHost } from './../../utils/constant';
import Check from './../../styles/assets/check.png';
import { NavbarMainPayment } from '../../components/Navbar/navbar';
import Select from 'react-select';
import { Card, CardHeader, CardBody } from "react-simple-card";
import { Flex } from './../../styles/pay';
import Cleave from 'cleave.js/react';
import Checkbox from './../../components/Checkbox/checkbox'
import { Radio, Input, Button } from 'antd';
const RadioGroup = Radio.Group;
const Search = Input.Search;

const style = require('./../../styles/profile/profile.css');
const defaultCheckedList = ['Apple', 'Orange'];
const plainOptions = ['Apple', 'Pear', 'Orange'];

class SelectBox extends Component {
    constructor(props) {
        super(props)
        this.state = {
            checkedItems: new Map(),
            isOpen: false,
            swal: false,
            files: [],
            id: this.props.match.params.id,
            payment_tp: "",
            isShow: false,
            payment_title: [],
            edu_list: [],
            level: [],
            student_name: [],
            isCheck: true,
            show: false,
            amount_fee: 0,
            readonly: true,
            discount: true,
            value: 1,
            user: [],
            disbaled: '',
            nis: '',
            collect: [],
            collectType: [],
            type: [],
            name: '',
            checkedList: defaultCheckedList,
            indeterminate: true,
            checkAll: false,
            payment_type: [
                { value: 'mandatory', label: 'Mandatory' },
                { value: 'optional', label: 'Optional' },
                { value: 'registration', label: 'Registration' },
            ],
            student_from: [
                { value: 1, label: 'Import From Grade' },
                { value: 2, label: 'Specific student name/ID' },
                // { value: 3, label: 'Import From Registration' },
            ],
            allChecked: false,
            checkedCount: 0,
            options: [
                { value: 'selectAll', text: 'Select All' },
                { value: 'orange', text: 'Orange' },
                { value: 'apple', text: 'Apple' },
                { value: 'grape', text: 'Grape' }
            ],
            checked: true,
            data: {
                title: "",
                author: "",
                publisher: "",
                isbn: "",
                type_id: "",
                series_title: "",
                summary: "",
                dewey: "",
                payment_tp: '',
                payment_tl: '',
                edu: '',
                grade_lvl: '',
                discription: '',
                fee: 0,
                discount: '',
                afterDisc: '',
                dueDate: '',
                inventory: {
                    title: '',
                    summary: '',
                    author: "",
                    publisher: "",
                    isbn: "",
                    series_title: "",
                    dewey: "",
                    dewey_decimal_class: ""
                },

            },
            selected: 1

        }
        this.handleClick = this.handleClick.bind(this);
        this.onChange = this.onChange.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
        this.handleBtnReady = this.handleBtnReady.bind(this)
        this.onDrop = this.onDrop.bind(this)
        this.handleDropdown = this.handleDropdown.bind(this)
        this.getDataStudent = this.getDataStudent.bind(this)
        this.generateFilter = this.generateFilter.bind(this)
        this.filter = this.filter.bind(this)
        this.getPaymentTitle = this.getPaymentTitle.bind(this)
        this.onChangeDiscount = this.onChangeDiscount.bind(this)
        this.handleRadio = this.handleRadio.bind(this)
        this.generateSearch = this.generateSearch.bind(this)
        this.getDataEduProgram = this.getDataEduProgram.bind(this)
        this.handleDropdownEducation = this.handleDropdownEducation.bind(this)
        this.getDataEduLevel = this.getDataEduLevel.bind(this)
        this.toggleCheckboxChange = this.toggleCheckboxChange.bind(this)
        this.typeFilter = this.typeFilter.bind(this)
        this.onCheckAllChange = this.onCheckAllChange.bind(this)
        this._handleSearch = this._handleSearch.bind(this)
        this._add = this._add.bind(this);
        this._remove = this._remove.bind(this);
        this.getDataType = this.getDataType.bind(this);
        this.handleDropdownSearch = this.handleDropdownSearch.bind(this)
        this.generateRadio = this.generateRadio.bind(this)
        this.handleChange = this.handleChange.bind(this);


        const token = localStorage.getItem("_token")
        if (token === null) {
            this.props.history.push('/login')
        }
    }
    componentWillMount() {
        this.selectedCheckboxes = new Set();
    }
    handleChange(e,id) {
        const item = e.target.name;
        const isChecked = e.target.checked;
        const dataArr = []
        this.setState(prevState => ({ checkedItems: prevState.checkedItems.set(item, !isChecked) }));
        if(isChecked  === false){
            var newlines = this.state.student_name.filter(function (line) {
                return line.user_id !== id;
            });
            // this.setState({stu})
            for(let i in newlines){
                let data =newlines[i].user_id   
                dataArr.push(data)       
          }
          this.setState({
            user: dataArr
          })
        }else{
            for(let i in this.state.student_name){
                let data =this.state.student_name[i].user_id   
                dataArr.push(data)       
          }
        //   let arr = dataArr.push(id)
          
        //   this.setState(this.state)
          console.log(dataArr,"user")
          this.setState({
              user:dataArr
          })
        }
        console.log(this.state.user,"user end")
       
        

    

        
       
        this.setState({ selected: !this.state.selected });
    }
    handleClick(e) {
        let clickedValue = e.target.value;

        if (clickedValue === 'selectAll' && this.refs.selectAll.getDOMNode().checked) {
            for (let i = 1; i < this.state.options.length; i++) {
                let value = this.state.options[i].value;
                this.refs[value].getDOMNode().checked = true;
            }
            this.setState({
                checkedCount: this.state.options.length - 1
            });

        } else if (clickedValue === 'selectAll' && !this.refs.selectAll.getDOMNode().checked) {
            for (let i = 1; i < this.state.options.length; i++) {
                let value = this.state.options[i].value;
                this.refs[value].getDOMNode().checked = false;
            }
            this.setState({
                checkedCount: 0
            });
        }

        if (clickedValue !== 'selectAll' && this.refs[clickedValue].getDOMNode().checked) {
            this.setState({
                checkedCount: this.state.checkedCount + 1
            });
        } else if (clickedValue !== 'selectAll' && !this.refs[clickedValue].getDOMNode().checked) {
            this.setState({
                checkedCount: this.state.checkedCount - 1
            });
        }
    }
    onCheckAllChange(e) {
        this.setState({
            checkedList: e.target.checked ? plainOptions : [],
            indeterminate: false,
            checkAll: e.target.checked,
        });
    }
    componentDidMount() {
        //did mount
        this.getDataEduProgram()
        this.getDataType()

    }

    handleDropdownSearch(e, prop) {
        let dv = this.state.data
        dv[prop] = e !== null ? e : 0
        this.state.collectType.push({ name: e.label })
        this.setState(this.state)
        this.setState({ data: dv, isShow: true })

        var self = this
        var url = `${apiPaymentHost}/payments/student_search?type=registration&registration_event_id=${e.value}`
        console.log(url, "url")
        axios.request({
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                let type = [];
                for (var i in response.data.data.students) {
                    const datum = response.data.data.students[i]
                    type.push({ value: datum.user_id })
                }
                console.log("here", Object.assign({}, type))
                self.setState({
                    user: type
                })

            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });



    }
    _add(user_id, name, nis, grade_name, grade_level, programe_name) {
        this.state.collect.push({
            user_id,
            name,
            nis,
            programe_name,
            grade_name,
            grade_level
        })
        console.log(this.state.student_name, "student name")
        var newlines = this.state.student_name.filter(function (line) {
            return line.user_id !== user_id;
        });
        this.setState(this.state)
        this.setState({
            show: true,
            student_name: newlines
        })
    }

    getDataType() {
        let self = this
        var url;
        url = `${apiPaymentHost}/payments/get_list?slug=registration`
        axios.request({
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                let type = [];
                for (var i in response.data.data.list) {
                    const datum = response.data.data.list[i]
                    type.push({ value: datum.key, label: datum.label })
                }
                self.setState({
                    type: type
                })

                console.log("type", this.state.type)
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }


    _remove(user_id, name, nis, grade_name, grade_level, programe_name) {
        this._processLineRemove(user_id)

    }
    _handleSearch(value) {
        console.log("value", value)
        this.setState({
            name: value,
            isShow: true

        })
        this.getDataStudent(value)
    }

    _processLineRemove(id) {
        var newlines = this.state.collect.filter(function (line) {
            return line.user_id !== id;
        });
        console.log("new line", newlines)
        this.setState({ collect: newlines });
    }

    formatPrice(price) {
        return new Intl.NumberFormat("ID", {
            style: "currency",
            currencyDisplay: "symbol",
            currency: "IDR"
        }).format(price);
    }
    onChange(e, prop) {
        e.preventDefault()
        let dv = this.state.data
        dv[prop] = e.target.value

        this.setState({ data: dv, afterDisc: dv.undefined })


    }
    onChangeFee(e, prop) {
        e.preventDefault()
        let dv = this.state.data
        dv[prop] = e.target.value
        dv['discount'] = 0

        this.setState({ data: dv, discount: false, amount_fee: e.target.value, afterDisc: dv.undefined })


    }

    onChangeDiscount(e, prop) {
        let dv = this.state.data;
        dv[prop] = e.target.value
        const pembilang = parseFloat(dv.undefined.replace(/,/g, ''))
        const discAmt = pembilang - e.target.value / 100 * pembilang


        this.setState({ data: dv, amount_fee: discAmt, discount: 0, afterDisc: this.formatPrice(discAmt) })
    }

    handleDropdown(e, prop) {
        let dv = this.state.data
        dv[prop] = e !== null ? e : 0
        this.setState({ data: dv })
        this.getPaymentTitle()

    }
    handleDropdownEducation(e, prop) {
        let dv = this.state.data
        dv[prop] = e !== null ? e : 0
        this.setState({ data: dv })
        console.log(e !== null ? e : 0)
        this.getDataEduLevel()
    }

    handleDropdownPay(e, prop) {
        let dv = this.state.data
        dv[prop] = e !== null ? e : 0
        this.setState({ data: dv })

        console.log(this.state.data.payment_tp.value, "value")
        if (this.state.data.payment_tp.value === 'registration') {
            this.getDataStudent()
        }
    }

    handleRadio(e, prop) {
        console.log('radio checked', e.target.value);
        this.setState({
            value: e.target.value,
        });

    }
    toggleModal() {
        this.setState({
            isOpen: !this.state.isOpen
        });

    }

    feeHandle(event) {
        this.setState({
            fee: this.formatPrice(event.target.value)
        })
    }

    getDataStudent(value) {
        let self = this
        console.log(this.state.value, "value", self.state.name)
        var url = ''
        if (this.state.value === 1 && this.state.data.payment_tp.value !== 'registration') {
            url = `${apiPaymentHost}/payments/student_search?program_education=${self.state.data.edu.value}&grade_level=${self.state.data.grade_lvl.value}`;
        } else if (this.state.value === 2 && this.state.data.payment_tp.value !== 'registration') {
            url = `${apiPaymentHost}/payments/student_search?type=specific&name=${value}`;
        } else if (this.state.data.payment_tp.value === 'registration') {
            url = `${apiPaymentHost}/payments/student_search?type=registration&registration_event_id=${self.state.data.payment_tl.value}`;
        } else {
            url = `${apiPaymentHost}/payments/student_search?type=specific&name=${value}`;
        }

        console.log(url)
        axios.request({
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                // let studentArr = [];
                // let user = [];
                // let student_name = [];
                // console.log("here", response.data.data.students)
                self.setState({
                    student_name: response.data.data.students
                })
                console.log(response.data.data, "hahahaha")
                // for (var i in response.data.data.students) {
                //     const datum = response.data.data.students[i]
                //     studentArr.push({ user: datum.user  })
                // }

                // for (var j in studentArr) {
                //     const datums = studentArr[j]
                //     user.push({ userArray: datums  })

                // }
                // // console.log("here", user)
                // for (var k in user) {
                //     const dat = user[k]
                //     console.log(dat.user,"fullname")
                //     student_name.push(dat.userArray)
                // }
                // console.log("herehrherhehr",student_name)


                // self.setState({
                //     student_name: student_name
                // })
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }
    getDataEduProgram() {
        let self = this
        axios.request({
            url: `${apiPaymentHost}/payments/transaction_new`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                console.log(response.data.data, "data")
                let edu_list = [];
                for (var i in response.data.data.import_type.education_list) {
                    const datum = response.data.data.import_type.education_list[i]
                    edu_list.push({ value: datum.key, label: datum.label })
                }
                self.setState({
                    edu_list: edu_list,
                })
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }

    getDataEduLevel() {
        let self = this
        axios.request({
            url: `${apiPaymentHost}/payments/get_list?type=${self.state.data.edu.value}&slug=program_education`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                console.log(response.data.data, "data")
                let level = [];
                for (var i in response.data.data.list) {
                    const datum = response.data.data.list[i]
                    level.push({ value: datum.key, label: datum.label })
                }
                self.setState({
                    level: level,
                })
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }



    getPaymentTitle() {
        let self = this
        var url = '';
        if (this.state.data.payment_tp.value === 'registration') {
            url = `${apiPaymentHost}/payments/get_list?type=${this.state.data.payment_tp.value}&slug=registration`
        } else {
            url = `${apiPaymentHost}/payments/get_list?type=${this.state.data.payment_tp.value}&slug=payment_type`
        }
        axios.request({
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                let payment_title = [];
                for (var i in response.data.data.list) {
                    const datum = response.data.data.list[i]
                    payment_title.push({ value: datum.key, label: datum.label })
                }
                self.setState({
                    payment_title: payment_title,
                })
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }


    onDrop(files) {
        this.setState({
            files
        });
    }

    sendUrl() {
        setTimeout(() => {
            this.setState({
                swal: true
            })
        }, 500)

        setTimeout(() => {
            this.props.history.push({
                pathname: '/payment-transaction/1'
            })
        }, 2000)


    }
    handleBtnReady(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
        this.setState({ disbaled: true })
        var model = {}
        var index = 0;
        var import_type;
        var title;
        var payment_tp;
        let student = []
        if (this.state.value === 1) {
            for(let i in this.state.student_name){
                let data = this.state.student_name[i].user_id
                student.push(data)
            }
            model = Object.assign({}, this.state.user.length === 0?student:this.state.user)

        } else if (this.state.value === 2) {
            for (let i in this.state.collect) {
                const b = this.state.collect[i]
                model[index] = b.user_id
                index = index + 1
            }
        } else {
            model = Object.assign({}, this.state.user)

        }

        if (this.state.value === 1 && this.state.data.payment_tp.value !== 'registration') {
            import_type = 'grade'
        } else if (this.state.value === 2 && this.state.data.payment_tp.value !== 'registration') {
            import_type = 'specific'
        } else {
            import_type = 'registration'
        }

        if (this.state.data.payment_tl.value === 'other' && this.state.data.payment_tp.value !== 'registration') {
            title = this.state.data.title
        } else {
            title = this.state.data.payment_tl.label
        }
        console.log(this.state.data.payment_tp.value, "value tp")

        if (this.state.data.payment_tl.value === 'other' && this.state.data.payment_tp.value !== 'registration') {
            payment_tp = this.state.data.title
        } else if (this.state.data.payment_tp.value === 'registration') {
            payment_tp = this.state.data.payment_tl.label
        } else {
            payment_tp = this.state.data.payment_tl.value
        }
        let self = this
        const refresh = () => {
            console.log("a")
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.sendUrl()
        }


        axios.request({
            url: `${apiPaymentHost}/payments/transaction_new`,
            headers: { 'Content-Type': 'application/json' },
            method: 'POST',
            data: {
                invoice_batch: {
                    payment_type: this.state.data.payment_tp.value,
                    payment_title: payment_tp,
                    title: title,
                    description: this.state.data.discription,
                    gross_fee: parseFloat(this.state.data.undefined.replace(/,/g, '')),
                    discount: this.state.data.discount,
                    amount_fee: this.state.amount_fee,
                    import_type: import_type,
                    due_date: this.state.data.dueDate,
                    user_id: model

                }

            }

        })
            .then(function (response) {
                refresh()

            })
            .catch(function (error) {
                // refresh()

                // this.setState({
                //     isOpen: !this.state.isOpen
                //   });

            });


    }
    filter(e) {
        e.preventDefault()
        this.setState({
            isShow: true
        })
        this.getDataStudent()
    }

    toggleCheckboxChange(id, e) {
        e.preventDefault()
        this.state.user.push(id)
        this.setState(this.state)
        console.log(this.state.user, "model")
        this.setState({ selected: !this.state.selected });
        // var newlines = this.state.student_name.filter(function (line) {
        //     return line.user_id !== id;
        // });
        // this.setState({selected: !this.state.selected,student_name:newlines}); 



    }
    // onChange(e) {
    //     console.log('checked = ', e.target.checked);
    //     this.setState({
    //         checked: e.target.checked,
    //     });
    // }

    typeFilter() {
        const wr = {
            height: '150px',
            overflowY: 'scroll'
        }
        const border = {
            border: 'none'
        }

        // const { isCheck } = this.state
        var isSelected = this.state.selected;
        const btn = {
            marginLeft: '-0.3%'
        }
        console.log("lo lo lo", this.state.type)
        const model = []
        for (let i in this.state.student_name) {
            const b = this.state.student_name[i]
            model.push(b.user.full_name)
        }
        // const { isCheck } = this.state
        if (this.state.value === 1 && this.state.data.payment_tp.value !== 'registration') {
            const filt = (
                <div>
                    <div className="col-md-6" >
                        <label className={style.label} >Education Program</label>
                        <Select
                            className={style.selectCategory}
                            value={this.state.edu}
                            onChange={(e) => this.handleDropdownEducation(e, 'edu')}
                            options={this.state.edu_list}
                        />
                    </div>
                    <div className="col-md-4" >
                        <label className={style.label} >Level</label>
                        <Select
                            className={style.selectCategory}
                            value={this.state.grade_lvl}
                            onChange={(e) => this.handleDropdown(e, 'grade_lvl')}
                            options={this.state.level}
                        />
                    </div>
                    <div className="col-md-12" >
                        <button type="submit" className={style.btnFilter} style={btn} onClick={(e) => { this.filter(e) }} >Find Student</button>
                    </div>
                </div>
            )
            return filt
        } else if (this.state.value === 2 & this.state.data.payment_tp.value !== 'registration') {
            const filt = (
                <div>
                    <div className="col-md-12" >
                        <Search
                            placeholder="input search text"
                            enterButton="Find"
                            size="large"
                            onSearch={value => this._handleSearch(value)}
                        />
                    </div>

                </div>
            )
            return filt
        } else {
            // console.log(this.state.data.payment_tl.value,"value of")
            if (this.state.data.payment_tl.value !== undefined) {
                const filt = (
                    <div>
                        <div className="col-md-12" >
                            <Card>
                                <CardHeader>Student of middle Schools</CardHeader>
                                <CardBody>
                                    <div class="row row-flex row-flex-wrap" style={wr}>
                                        {
                                            this.state.student_name.map(function (array, i) {
                                                return <div class="col-md-4">
                                                    <div class="panel panel-default flex-col" style={border}>
                                                        <div class="panel-body flex-grow" style={border}>
                                                            <Flex>
                                                                <div class="checkbox">
                                                                    <Checkbox onChange={(e) => { this.toggleCheckboxChange(array.user_id, e) }} value={array.user_id} checked={isSelected}>{array.user.full_name}</Checkbox>
                                                                    <p>{array.student.nis}</p>
                                                                    <p>{array.student.grade_name} | {array.student.grade_level} | {array.student.programe_name}</p>

                                                                </div>
                                                            </Flex>
                                                        </div>
                                                    </div>
                                                </div>
                                            }, this)
                                        }


                                    </div>

                                </CardBody>
                            </Card>
                        </div>

                    </div>
                )
                return filt
            } else {

            }

        }
    }
    generateRadio() {
        const radio = {
            marginLeft: '-7%',
        }

        if (this.state.data.payment_tp.value !== 'registration') {
            const tmp = (
                this.state.student_from.map(function (arr, l) {
                    return <div className="col-md-4" >
                        <label className={style.label} > {l === 0 ? 'Select Student' : ''}</label>
                        <div className="radio" style={radio}>
                            <RadioGroup onChange={(e) => { this.handleRadio(e, 'value') }} value={this.state.value}>
                                <Radio value={arr.value}>{arr.label}</Radio>
                            </RadioGroup>
                        </div>
                    </div>
                }, this)
            )
            return tmp
        }
    }

    generateFilter() {
        const wr = {
            height: '150px',
            overflowY: 'scroll'
        }
        const border = {
            border: 'none'
        }
        // const btn = {
        //     marginLeft: '-0.3%'
        // }

        console.log("heawa", this.state.student_name)
        const model = []
        for (let i in this.state.student_name) {
            const b = this.state.student_name[i]
            model.push(b.user.full_name)
        }
        // const { isCheck } = this.state

        if (this.state.value === 1) {
            if (this.state.isShow === true) {
                const Content = (
                    <div class="col-md-12"  >
                        <Card>
                            <CardHeader>Student of middle Schools</CardHeader>
                            <CardBody>
                                <div class="row row-flex row-flex-wrap" style={wr}>
                                    {
                                        this.state.student_name.map(array => (
                                             <div class="col-md-4">
                                                <div class="panel panel-default flex-col" style={border}>
                                                    <div class="panel-body flex-grow" style={border}>
                                                        <Flex>
                                                            <div class="checkbox">
                                                                {/* <Checkbox onChange={(e) => { this.toggleCheckboxChange(array.user_id, e) }} value={array.user_id}  checked={isSelected}>{array.user.full_name}</Checkbox> */}
                                                                <Checkbox name={array.user.full_name} checked={!this.state.checkedItems.get(array.user.full_name)} value={array.user_id}  onChange={(e) => {this.handleChange(e,array.user_id)}} /><p>{array.user.full_name}</p>
                                                                <p>{array.student.nis}</p>
                                                                <p>{array.student.grade_name} | {array.student.grade_level} | {array.student.programe_name}</p>


                                                            </div>
                                                        </Flex>
                                                    </div>
                                                </div>
                                            </div>
                                        ))
                                    }


                                </div>

                            </CardBody>
                        </Card>
                    </div>
                )
                return Content
            }
        } else if (this.state.value === 2) {
            if (this.state.isShow === true) {
                const Content = (
                    <div class="col-md-12"  >
                        <Card>
                            <CardHeader>Student of middle Schools</CardHeader>
                            <CardBody>
                                <div class="row row-flex row-flex-wrap" style={wr}>
                                    {
                                        this.state.student_name.map(function (array, i) {
                                            return <div class="col-md-4">
                                                <div class="panel panel-default flex-col" style={border}>
                                                    <div class="panel-body flex-grow" style={border}>
                                                        <Flex>
                                                            <div class="checkbox">
                                                                <p>{array.user.full_name}</p>
                                                                <p>{array.student.nis}</p>
                                                                <p>{array.student.grade_name} | {array.student.grade_level} | {array.student.programe_name}</p>
                                                                <Button style={{ marginLeft: '2em' }} onClick={(e) => { this._add(array.user_id, array.user.full_name, array.student.nis, array.student.grade_name, array.student.grade_level, array.student.programe_name) }} type="primary">Add</Button>
                                                            </div>
                                                        </Flex>
                                                    </div>
                                                </div>
                                            </div>
                                        }, this)
                                    }
                                </div>
                            </CardBody>
                        </Card>
                        {this.generateSearch()}
                    </div>
                )
                return Content
            }
        } else {
            if (this.state.isShow === true) {
                const tempt = (

                    <div class="col-md-12"  >
                        {
                            this.state.collectType.map(function (arr, i) {
                                return <div class="col-md-4">
                                    <div class="panel panel-default flex-col" style={border}>
                                        <div class="panel-body flex-grow" style={border}>
                                            <Flex>
                                                <div class="checkbox">
                                                    {/* <a>{arr.name}</a> */}
                                                    {/* <Button shape="no" icon="close" onClick={(e) => { this._remove(arr.name) }} /> */}
                                                    <p style={{ marginTop: '0%', marginLeft: '0%', marginBottom: '0%' }} > {arr.name}</p>
                                                    {/* <p style={{ marginLeft: '25%' }}>{array.nis}</p>
                                                    <p style={{ marginLeft: '25%', marginTop: '-10%' }}>{array.grade_name} | {array.grade_level} | {array.programe_name}</p> */}
                                                </div>
                                            </Flex>
                                        </div>
                                    </div>
                                </div>
                            }, this)
                        }


                        {/* {this.generateSearch()} */}
                    </div>
                )

                return tempt
            }

        }
    }

    generateSearch() {
        const border = {
            border: 'none'
        }
        console.log(this.state.show, "show status")
        if (this.state.show === true) {
            const tempt = (
                <div>
                    {
                        this.state.collect.map(function (array, i) {
                            console.log("i", i)
                            return <div class="col-md-4">
                                <div class="panel panel-default flex-col" style={border}>
                                    <div class="panel-body flex-grow" style={border}>
                                        <Flex>
                                            <div class="checkbox">
                                                <Button shape="no" icon="close" onClick={(e) => { this._remove(array.user_id, array.name, array.nis, array.grade_name, array.grade_level, array.programe_name) }} />
                                                <p style={{ marginTop: '-26%', marginLeft: '25%', marginBottom: '10%' }} > {array.name}</p>
                                                <p style={{ marginLeft: '25%' }}>{array.nis}</p>
                                                <p style={{ marginLeft: '25%', marginTop: '-10%' }}>{array.grade_name} | {array.grade_level} | {array.programe_name}</p>
                                            </div>
                                        </Flex>
                                    </div>
                                </div>
                            </div>
                        }, this)
                    }
                </div>
            )
            return tempt
        }

    }



    render() {
        console.log(this.state.data.payment_tl.value, "value")
        const model = []
        for (let i in this.state.student_name) {
            const b = this.state.student_name[i]
            model.push(b.user.full_name)
        }
        return (
            <div className={style.profilePage} >
                <NavbarMainPayment />
                <div className={style.mainContent}>

                    <div className={style.boxAddBook}>
                        <div>
                            <form >
                                <div className="col-sm-12 center">
                                    <label className={style.labelDate}>Payment Type</label>
                                    <div className="input-group date col-sm-12">
                                        <Select
                                            className={style.selectCategory}
                                            value={this.state.data.payment_tp}
                                            onChange={(e) => this.handleDropdown(e, 'payment_tp')}
                                            options={this.state.payment_type}
                                        />
                                    </div>
                                </div>
                                <div className="col-sm-12 center">
                                    <label className={style.labelDate}>Payment Title</label>
                                    <div className="input-group date col-sm-12">
                                        <Select
                                            className={style.selectCategory}
                                            value={this.state.data.payment_tl}
                                            onChange={(e) => this.handleDropdownPay(e, 'payment_tl')}
                                            options={this.state.payment_title}
                                        />
                                    </div>
                                </div>
                                {
                                    this.state.data.payment_tl.value === 'other' ? <div className={style.formGroup}>
                                        <label className={style.label} >title</label>
                                        <div className={style.form}>
                                            <input type="text" className="form-control" onChange={(e) => this.onChange(e, 'title')} value={this.state.data.title} />
                                        </div>
                                    </div> : ''
                                }
                                <div className={style.formGroup}>
                                    <label className={style.label} >Description</label>
                                    <div className={style.form}>
                                        <input type="text" className="form-control" onChange={(e) => this.onChange(e, 'discription')} value={this.state.data.discription} />
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label} >Amount Fee</label>
                                    <div className={style.form}>
                                        <Cleave
                                            options={{
                                                numeral: true,
                                                numeralThousandsGroupStyle: 'thousand'
                                            }}
                                            className="form-control"
                                            onChange={this.onChangeFee.bind(this)} />
                                    </div>
                                </div>

                                <div className="col-md-4" >
                                    <label className={style.label} >Discount</label>
                                    <div className={style.formDisc}>
                                        <input type="text" className="form-control" readOnly={this.state.discount} value={this.state.data.discount} onChange={(e) => this.onChangeDiscount(e, 'discount')} />
                                    </div>
                                </div>
                                <div className="col-md-8" >
                                    <label className={style.label} >Fee After Discount</label>
                                    <div className={style.formDiscAfter}>
                                        <input type="text" className="form-control" value={this.state.afterDisc} onChange={(e) => this.onChange(e, 'afterDisc')} readOnly={this.state.readonly} />
                                    </div>
                                </div>

                                {this.generateRadio()}


                                {this.typeFilter()}
                                {this.generateFilter()}

                                <div className="col-md-6" >
                                    <label className={style.label} >Due Date</label>
                                    <input type="date" className="form-control" data-date-format="DD MMMM YYYY" onChange={(e) => this.onChange(e, 'dueDate')} value={this.state.data.dueDate} />
                                </div>

                            </form>
                            <div className="col-md-12" >
                                <button className={style.btn} onClick={(e) => { this.handleBtnReady(e) }} disabled={this.state.disbaled} >Save</button>
                            </div>

                            <div>
                                <Swal show={this.state.swal} >
                                    <div>
                                        <img src={Check} alt="check" className={style.Path} />
                                        <p className={style.swal}>New item has been added.</p>
                                    </div>
                                </Swal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default SelectBox;