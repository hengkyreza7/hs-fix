import React, { Component } from 'react';
import { NavbarMainPayment } from './../../components/Navbar/navbar';
import { Title } from './../../styles/pay';
import Sidebar from './../../components/Layout/layoutInventory';
import styled from 'styled-components';
import breakpoint from "styled-components-breakpoint";
// import Select from 'react-select';
import axios from 'axios';
import Modals from './../../components/Modal/stop';
import { apiPaymentHost } from './../../utils/constant'
import Badge from './../../styles/assets/group-4@2x.png'
import { Button } from 'antd';
import Books from './../../styles/assets/user.svg';
import User from './../../styles/assets/users.png';
import Telp from './../../styles/assets/telp.svg';
import Remove from './../../components/Modal/remove';
import Pagination from './../../components/pagination/index';
import { changeValue } from './../../utils/helper'
import {  Input, Select, DatePicker } from 'antd';
const Option = Select.Option;
const Search = Input.Search;




const style = require('./../../styles/dashboard/dashboard.css');
var dateFormat = require('dateformat');



export default class transaction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: '1',
            id: this.props.match.params.id,
            type: "payment",
            swal: false,
            data: {
                invoices: [],
                invoice_batch: {}
            },
            status: [
                { value: 'paid', label: 'Paid' },
                { value: 'pending', label: 'Pending' },
                { value: 'less', label: 'Less' },
                { value: 'over', label: 'Over' },
                { value: 'waiting', label: 'Waiting' },
            ],
            reason: '',
            name: "",
            class: '',
            nis: '',
            isOpenPick: false,
            visible: false,
            code: '',
            page: 1,
            total_page: 0,
            parents: {
                parents: []
            }
        }
        this.toggle = this.toggle.bind(this);
        this.changePage = this.changePage.bind(this);
        this.changePageDetail = this.changePageDetail.bind(this);
        this.getData = this.getData.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.remove = this.remove.bind(this)
        this.onChangeTextArea = this.onChangeTextArea.bind(this)
        this.renderButton = this.renderButton.bind(this)
        this.showModal = this.showModal.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
        this.handleOk = this.handleOk.bind(this)
        this.stop = this.stop.bind(this)
        this.handlePaginate = this.handlePaginate.bind(this)
        this.group = this.group.bind(this)
        this.onChange = this.onChange.bind(this)
        this.find = this.find.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }
    componentDidMount() {
        this.getData()
    }
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    handlePaginate(page) {
        let self = this
        axios.request({
            url: `${apiPaymentHost}/payments/batch_get?id=${this.props.location.state.id}&page=${page}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                response.data.data.invoices.map(function (i, j) {
                    return response.data.data.invoices[j].no = (j + 1)
                })
                console.log("logged", response.data.meta)
                self.setState({
                    data: response.data.data,
                    page: response.data.meta.pagination.page,
                    total_page: response.data.meta.pagination.total_page

                })


            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });

    }
    showModal(e, id, array, name) {
        if (!this.state.visible === false) {
            this.setState({
                parents: {
                    parents: []
                }
            })
        } else {
            this.setState({
                parents: array,
                name: name,
                nis: array.nis,
                class: array.program_name + '|' + array.grade_name + '|' + array.grade_level
            });
        }
        this.setState({
            visible: !this.state.visible,
            id: id
        });


    }
    handleOk(e) {
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    handleCancel(e) {
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    onChangeTextArea(event) {
        this.setState({
            reason: event.target.value
        })
    }

    stop(e) {
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.sendUrl()
            this.getData()
        }
        axios({
            url: `${apiPaymentHost}/payments/batch_stoped?id=${this.state.data.invoice_batch.id}`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            data: {
                "reason": this.state.reason
            }
        })
            .then(function (response) {
                refresh()


            })
            .catch(function (error) {

            });
    }

    remove(e) {
        console.log("awawa", this.state.id)
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ visible: false })
            }, 200)
            this.sendUrl()
            this.getData()
        }
        axios({
            url: `${apiPaymentHost}/payments/invoice_deleted?id=${this.state.id}`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            data: {
                "reason": this.state.reason
            }
        })
            .then(function (response) {
                refresh()


            })
            .catch(function (error) {

            });
    }

    group(e, id) {
        e.preventDefault()
        this.props.history.push({
            pathname: '/detail-group-payment-all/' + id,
            state: { id: this.props.location.state.id }
        })
    }



    toggleModal(user_id, id, number, title, status, author, e) {
        this.setState({
            isOpen: !this.state.isOpen,
            // receipt: number,
            // title: title,
            // status: status,
            // author: author,
            // id_circulation: id

        });
        // this.getBorrowedName(user_id)

    }

    changePage() {
        this.props.history.push('/payment-transaction/1')
    }

    handleChange(status) {
        this.getData(status)
    }
    changePageDetail() {
        this.props.history.push('/detail-payment')
    }
    handleBlur() {
        console.log('blur');
    }

    handleFocus() {
        console.log('focus');
    }

    onChange(date, dateString) {
        this.getData(dateString)
    }
    find(value){
        this.getData(value)
    }

    getData(value = '',date = '', status = '') {
        let self = this
        var url = ''
        console.log(value,date)
        if(value === '' && date === '' && status === '' ){
            url = `${apiPaymentHost}/payments/batch_get?id=${this.props.location.state.id}`
            console.log(url,"my url")
        }else if(value === '' && date !== '' && status === '' ){
            url = `${apiPaymentHost}/payments/batch_get?id=${this.props.location.state.id}&paid_date=${date}`
            console.log(url,"my url")
        }else if( value !== '' && date === '' && status === '' ){
            url = `${apiPaymentHost}/payments/batch_get?id=${this.props.location.state.id}&full_name=${value}`   
        }else if(value === '' && date === '' && status !== ''){
            url = `${apiPaymentHost}/payments/batch_get?id=${this.props.location.state.id}&payment_status=${status}`
        }else{
            url = `${apiPaymentHost}/payments/batch_get?id=${this.props.location.state.id}&paid_date=${date}`
            console.log(url,"my url -")
        }

        axios.request({
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                response.data.data.invoices.map(function (i, j) {
                    return response.data.data.invoices[j].no = (j + 1)
                })
                console.log("logged", response.data.meta)
                self.setState({
                    data: response.data.data,
                    page: response.data.meta.pagination.page,
                    total_page: response.data.meta.pagination.total_page

                })


            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }

    formatPrice(price) {
        return new Intl.NumberFormat("ID", {
            style: "currency",
            currencyDisplay: "symbol",
            currency: "IDR"
        }).format(price);
    }

    renderButton() {
        const Selector = styled.div`
        margin: 0 0.1rem;
        border-radius: 4px;
        padding: 0.2rem 1.2rem;
        border: solid 1px;
        font-size: 1.2rem;
        margin-bottom: 0.3rem;
        
        ${breakpoint("sm")`
            font-size: 0.9rem;
            padding: 0.2rem 1rem;
        `}
        `;
        const Stop = styled.div`

        button {
            width: 170px;
            height: 30px;
            margin-top: 40%;
            margin-left : 90%;
            border: solid 1px #fe634f;
            background-color: #fe634f;
            cursor: pointer;
            color: #ffffff;
            font-family: Nunito Sans;
            font-size : 18px;
        }
        
        span {
            margin-right: 1rem;
            margin-top : 1rem;
        }    
        
    `;
        const Export = styled(Selector)`
        width: 170px;
        height: 30px;
        margin-top: 10%;
        margin-left : 90%;
        border: solid 1px #054ee1;
        background-color: #054ee1;
        cursor: pointer;
        color: #ffffff;
        font-family: Nunito Sans;
        font-size : 18px;
        
        span {
            margin-left: 1%;
            margin-top : 1rem;
        }
        
    `;

        let stop = this.state.data.invoice_batch.stoped
        if (stop === 0) {
            const tmpt = (
                <div class="col-md-2" >
                    <Stop>
                        <button onClick={(e) => { this.toggleModal(e) }}>Stop Registration</button>
                    </Stop>
                    <Export>Export Report</Export>
                </div>
            )
            return tmpt;

        }

    }

    render() {
        console.log(this.state.data, "parents")
        return (
            <div>
                <NavbarMainPayment status={this.state.type} />
                <Title>Transaction Management</Title>
                <Sidebar>
                    <div class="col-md-3" >
                        <Back onClick={this.changePage} ><span className="glyphicon glyphicon-arrow-left"></span> Back</Back>
                        <Right>
                            <div>
                                <h1>Payment ID:</h1>
                                <p>{this.state.data.invoice_batch.batch_code}</p>
                            </div>
                            <div>
                                <h1>Create On:</h1>
                                <p>{dateFormat(this.state.data.invoice_batch.created_at, "isoDate")}</p>
                            </div>
                            <div>
                                <h1>Due Date:</h1>
                                <p>{dateFormat(this.state.data.invoice_batch.due_date, "isoDate")}</p>
                            </div>
                            <div>
                                <h1>Participants :</h1>
                                <p>{this.state.data.invoice_batch.participant_count} Student </p>
                            </div>
                            <div>
                                <h1>Student From :</h1>
                                <p>{this.state.data.invoice_batch.import_type}</p>
                            </div>
                            <div>
                                <h1>Type :</h1>
                                <p>{this.state.data.invoice_batch.payment_type}</p>
                            </div>
                            <div>
                                <h1>Amount :</h1>
                                {
                                    this.state.data.invoice_batch.discount > 0 ?

                                        <div className="col-sm-12">
                                            <div className="col-md-6">
                                                <h3>{this.formatPrice(this.state.data.invoice_batch.gross_fee)} </h3>
                                            </div>
                                            <div className="col-md-6">
                                                <h2>{'(' + this.state.data.invoice_batch.discount + ' Disc)'}</h2>


                                            </div>
                                            <h4>{this.formatPrice(this.state.data.invoice_batch.amount_fee)} </h4>
                                        </div> :
                                        <p>{this.formatPrice(this.state.data.invoice_batch.gross_fee)} </p>
                                }

                            </div>
                        </Right>
                    </div>
                    <div class="col-md-9" >
                        <div class="col-md-7" >
                            {
                                this.state.data.invoice_batch.stoped === 1 ? <TitlePage>Detail - {changeValue(this.state.data.invoice_batch.payment_title)} <img src={Badge} alt="badge" /></TitlePage> : <TitlePage>Detail - {changeValue(this.state.data.invoice_batch.payment_title)} </TitlePage>
                            }
                            <Desc>
                                <div>
                                    <h1>Description</h1>
                                    <p>{this.state.data.invoice_batch.description}</p>
                                    <div className="col-sm-4">
                                        <h5>Total Payment</h5>
                                        <h3>{this.formatPrice(this.state.data.invoice_batch.payment_total)}</h3>
                                    </div>
                                    <div className="col-sm-6">
                                        <h5>Payment Receive</h5>
                                        <h3>{this.state.data.invoice_batch.payment_received === null ? this.formatPrice('0') : this.formatPrice(this.state.data.invoice_batch.payment_received)}</h3>
                                    </div>
                                </div>
                            </Desc>

                        </div>
                        {this.renderButton()}
                    </div>
                    <div class="col-md-9"  >
                        <div className="col-md-12 filter">
                            <div className="col-md-4">
                                <Select
                                    allowClear={true}
                                    showSearch
                                    style={{ width: 240, margin: 10 }}
                                    // className={}
                                    placeholder="Sort by Status"
                                    optionFilterProp="children"
                                    onChange={this.handleChange}
                                    onFocus={this.handleFocus}
                                    onBlur={this.handleBlur}
                                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                >
                                    {this.state.status.map(function (arr, i) {
                                        return <Option value={arr.value}>{arr.label}</Option>
                                    })
                                    }
                                </Select>
                            </div>
                            <div className="col-md-4">
                                <DatePicker
                                    allowClear={true}	 
                                    onChange={this.onChange} 
                                    style={{ width: 240, margin: 10 }}
                                />
                            </div>
                            <div className="col-md-4">
                                <Search
                                    style={{ width: 260, margin: 10 }}
                                    placeholder="Find student name"
                                    onSearch={value => this.find(value)}
                                    enterButton
                                />
                            </div>
                        </div>


                        <table className="table table-bordered" style={margin}>
                            <thead className="thead-dark" style={color}>
                                <tr>
                                    <th className={style.fontTable} scope="col">No</th>
                                    <th className={style.fontTable} style={width} >Student Name</th>
                                    <th className={style.fontTable} scope="col">Paid Nominal</th>
                                    <th className={style.fontTable} scope="col">Status</th>
                                    <th className={style.fontTable} scope="col">Paid Date</th>
                                    <th className={style.fontTable} scope="col">Program</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.data.invoices.map(function (arr, i) {
                                        return <tr>
                                            <td className={style.valueTable} >{i + 1}</td>
                                            <td onClick={(e) => { this.showModal(e, arr.id, arr.user.student, arr.user.full_name) }} className={style.popCol} >{arr.user.full_name}</td>
                                            {arr.payment_group !== undefined ?
                                                <td className={style.valueTable} >

                                                    {this.formatPrice(arr.amount_fee)} <Button type="primary" onClick={(e) => { this.group(e, arr.payment_group.id) }} style={btn} shape="circle" icon="search" />
                                                </td> :
                                                <td className={style.valueTable} >

                                                    {this.formatPrice(arr.amount_fee)}
                                                </td>
                                            }
                                            <td className={style.valueTable} >{arr.payment_status}</td>
                                            <td className={style.valueTable} >{arr.paid_date === null ? "N/A" : dateFormat(arr.paid_date, "isoDate")}</td>
                                            <td className={style.valueTable} >{arr.user.student === undefined ? "N/A" : arr.user.student.program_name}</td>

                                        </tr>
                                    }, this)
                                }
                            </tbody>
                        </table>
                        <Pagination
                            margin={2}
                            style={marginPage}
                            page={this.state.page}
                            count={Math.ceil(this.state.total_page)}
                            onPageChange={this.handlePaginate}
                        />
                    </div>
                </Sidebar>
                <Modals shouldCloseOnOverlayClick={true} show={this.state.isOpen} data={this.state.reason} onClosePress={() => this.toggleModal()} onClose={this.toggleModal} onReady={this.stop} >
                    <div>
                        <Mdl>
                            <div>
                                <h1>Detail - {this.state.data.invoice_batch.payment_title}</h1>
                                <h2>Description</h2>
                                <p>{this.state.data.invoice_batch.description}</p>
                                <h4><strong>Create On : </strong> {dateFormat(this.state.data.invoice_batch.created_at, "isoDate")} </h4>
                                <h5>Reason</h5>
                                <textarea className="form-control" value={this.state.reason} onChange={(e) => { this.onChangeTextArea(e) }} style={box} rows="4" cols="50" />
                            </div>
                        </Mdl>
                    </div>
                </Modals>
                <Remove shouldCloseOnOverlayClick={true} show={this.state.visible} data={this.state.reason} onClosePress={() => this.showModal()} onClose={this.showModal} onReady={this.remove} >
                    <div>
                        <Rmv>
                            <div>
                                <img src={Books} alt="logo" width="34px" height="34px" />
                                <h1>{this.state.name}</h1>
                                <h2>{this.state.nis}</h2>
                                <h3>{this.state.class}</h3>
                                {
                                    this.state.parents.parents.map(function (arr, l) {
                                        return <div>
                                            <img src={User} alt="logo" width="34px" height="34px" />
                                            <img src={Telp} alt="logo" width="34px" height="34px" />
                                            <h5>{arr.user.gender === 1 ? 'Father Name' : 'Mother Name'}</h5>
                                            <h4>{arr.user.full_name}</h4>
                                            <h3>Phone</h3>
                                            <h4>{arr.user.phone_number === "" ? "N/A" : arr.user.phone_number}</h4>
                                        </div>
                                    }, this)
                                }



                                <br />
                                <h5>Reason</h5>
                                <textarea className="form-control" value={this.state.reason} onChange={(e) => { this.onChangeTextArea(e) }} style={box} rows="4" cols="50" />

                            </div>
                        </Rmv>
                    </div>
                </Remove>

            </div>
        )


    }
}

const marginPage = {
    marginLeft: '10%'
}


const Rmv = styled.div`
    div{
        width:'30em';
        height: '250em';
        div{
            margin-bottom:10px;
            width: 335px;
            height: 121px;
            box-shadow: 0 11px 14px -10px #e5eced;
            border: solid 1px #e5eced;
            background-color: #ffffff;
            
            img {
                margin-left : 1em;
                margin-bottom : 2em;
                display: block;
                border-radius: 4%;
            }

            h5{
                margin-top : -37%;
                margin-left: 25%;
                height: 18px;
                opacity: 0.6;
                font-family: Nunito Sans;
                font-size: 13px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: normal;
                letter-spacing: normal;
                color: #273d52;
            }
            
            h4{
                margin-top : -0%;
                margin-left: 25%;
                width: 500px;
                height: 18px;
                opacity: 0.6;
                font-family: Nunito Sans;
                font-size: 13px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: normal;
                letter-spacing: normal;
                color: #273d52;
            }

            h3{
                margin-top : 3%;
                margin-left: 25%;
                width: 77px;
                height: 18px;
                opacity: 0.6;
                font-family: Nunito Sans;
                font-size: 13px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: normal;
                letter-spacing: normal;
                color: #273d52;
            }

        }
        img {
            margin-left: 2%;
            margin-top: 2%;
            width: 34px;
            height: 34px;
            border: solid 0.55px #d6d6d6;
            border-radius: 50%;
        }
        h1{
            margin-top: -11%;
            margin-left : 15%;
            width: 360px;
            height: 27px;
            font-family: Nunito Sans;
            font-size: 17px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #759bfa;
        }

        h2 {
            margin-top: -6%;
            margin-left : 15%;
            width: 198px;
            height: 25px;
            opacity: 0.8;
            font-family: Nunito Sans;
            font-size: 14px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #759bfa;
        }

        h3 {
            margin-top: -2%;
            margin-left : 15%;
            font-family: Nunito Sans;
            width: 198px;
            height: 18px;
            font-size: 13px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #759bfa;
        }

        p {
            height: 28px;
            font-family: NunitoSans;
            font-size: 13px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 2.15;
            letter-spacing: normal;
            color: #323c47;
        }
        h4 {
            height: 20px;
            font-family: Nunito Sans;
            font-size: 15px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #1f1f1f;
        }

        h5 {
            height: 15px;
            font-family: Nunito Sans;
            font-size: 11px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: 0.1px;
            color: #323c47;
        }

            html, body, .container {
            height: 100%;
            }
            textarea.form-control {
                height: 300%;
                width: 331px;
                height: 198px;
                border-radius: 4px;
                border: solid 1px #e9eff4;
                background-color: #ffffff;
                font-family: NunitoSans;
                font-size: 15px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: 1.8;
                letter-spacing: -0.2px;
                color: #323c47;
            }
            }

`

const color = {
    backgroundColor: '#edf5ff',
}

const margin = {
    marginLeft: '0%',
    marginTop: '10%'
}

const Selector = styled.div`
margin: 0 0.1rem;
border-radius: 4px;
padding: 0.2rem 1.2rem;
border: solid 1px;
font-size: 1.2rem;
margin-bottom: 0.3rem;

${breakpoint("sm")`
    font-size: 0.9rem;
    padding: 0.2rem 1rem;
`}
`;

const Back = styled(Selector)`
    width: 120px;
    height: 44px;
    margin-top: 20%;
    margin-left : 10%;
    border: solid 1px #054ee1;
    background-color: #ffffff;
    cursor: pointer;
    color: #054ee1;
    font-family: Nunito Sans;
    font-size : 18px;
    
    span {
        margin-right: 1rem;
        margin-top : 1rem;
    }    
    
`;



const TitlePage = styled.div`
    margin-top : 10%;
    width: 577px;
    height: 34px;
    font-family: Nunito Sans;
    font-size: 25px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: -0.7px;
    color: #233539;
`
const Desc = styled.div`
   div{
        h1 {
            margin-top : 7%;
            width: 95px;
            height: 20px;
            font-family: Nunito Sans;
            font-size: 15px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #1f1f1f;
        }

        p {
            margin-top : 0%;
            height: 28px;
            font-family: Nunito Sans;
            font-size: 15px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.87;
            letter-spacing: normal;
            color: #323c47;

        }

        h5 {
            margin-left: -8.5%;
            height: 18px;
            font-family: Nunito Sans;
            font-size: 13px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #4da1ff;
        }
        h4 {
            margin-left : 30%;
            margin-top : -9.3%;
            height: 18px;
            font-family: Nunito Sans;
            font-size: 13px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #4da1ff;
        }
        h3 {
            ${'' /* margin-top : -5.2%; */}
            margin-left: -5%;
            height: 28px;
            font-family: 'Nunito Sans';
            font-size: 15px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.87;
            letter-spacing: normal;
            color: #323c47;

        }
        
   } 
  
  
`

// const Find = styled(Selector)`
//     height: 44px;
//     margin-top : 90%;
//     border-color: #4f7cfe;
//     background-color: #4f7cfe;
//     cursor: pointer;
//     color: #ffffff;
//     font-family: Nunito Sans;

//     p {
//         margin-left : 60px;
//         margin-top: 10px;
//         width: 39px;
//         height: 16.5px;
//         font-family: Nunito Sans;
//         font-size: 16.1px;
//         font-weight: normal;
//         font-style: normal;
//         font-stretch: normal;
//         line-height: 1.36;
//         letter-spacing: normal;
//         text-align: center;
//         color: #ffffff;

//         }    
//     :hover {
//     border-radius: 4px;
//     color: white;
//     }
// `;

const Right = styled(Selector)`
    margin-top : 5%;
    margin-left : 8%;
    width: 220px;
    height: 550px;
    border-radius: 5px;
    box-shadow: 0 11px 14px -10px #e5eced;
    border: solid 1px #e5eced;
    background-color: #ffffff;
    div {
        margin-bottom : 2%;
            h1 {
            margin-left : 1%;
            height: 20px;
            font-family: NunitoSans;
            font-size: 15px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #1f1f1f;

        }

        p {
            margin-left : 1%;
            height: 20px;
            font-family: Nunito Sans;
            font-size: 15px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #233539;
        }
        h3 {
                margin-top:-0.5em;
                margin-left:-1.5em;
                height: 20px;
                font-family: Nunito Sans;
                font-size: 15px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                text-decoration:line-through;
                line-height: normal;
                letter-spacing: normal;
                color: #233539;
            
        }
        h4 {
                margin-top:-0.5em;
                margin-left:-0.5em;
                height: 20px;
                font-family: Nunito Sans;
                font-size: 15px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: normal;
                letter-spacing: normal;
                color: #233539;
            
        }
        h2 {
            margin-top:-0.5em;
            margin-left:-0.5em;
            height: 20px;
            width:100px;
            font-family: Nunito Sans;
            font-size: 15px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #66c003;
            text-decoration-color:green
        }
        
      
    }
`

const Mdl = styled.div`
    h1{
        width: 160px;
        height: 27px;
        font-family: Nunito Sans;
        font-size: 20px;
        font-weight: 600;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: -0.6px;
        color: #233539;
    }

    h2 {
        width: 95px;
        height: 20px;
        font-family: Nunito Sans;
        font-size: 15px;
        font-weight: bold;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #1f1f1f;

    }

    h3 {
        width: 342px;
        height: 28px;
        font-family: Nunito Sans;
        font-size: 13px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 2.15;
        letter-spacing: normal;
        color: #323c47;
    }
    
    p {
        height: 28px;
        font-family: NunitoSans;
        font-size: 13px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 2.15;
        letter-spacing: normal;
        color: #323c47;
    }
    h4 {
        height: 20px;
        font-family: Nunito Sans;
        font-size: 15px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #1f1f1f;
    }

    h5 {
        height: 15px;
        font-family: Nunito Sans;
        font-size: 11px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: 0.1px;
        color: #323c47;
    }

        html, body, .container {
          height: 100%;
        }
        textarea.form-control {
            height: 300%;
            width: 331px;
            height: 198px;
            border-radius: 4px;
            border: solid 1px #e9eff4;
            background-color: #ffffff;
            font-family: NunitoSans;
            font-size: 15px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.8;
            letter-spacing: -0.2px;
            color: #323c47;
        }
`
const box = {
    height: '80%'
}

const width = {
    width: '20em'
}

const btn = {
    width: '20px',
    height: '20px'
}