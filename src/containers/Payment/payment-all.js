import React, { Component } from 'react';
import { NavbarMainPayment } from './../../components/Navbar/navbar';
import { Title } from './../../styles/pay';
import Sidebar from './../../components/Layout/layoutInventory';
import styled from 'styled-components';
import breakpoint from "styled-components-breakpoint";
// import Select from 'react-select';
import { Modal } from 'antd';
import axios from 'axios';
import { apiPaymentHost } from './../../utils/constant'
const style = require('./../../styles/dashboard/dashboard.css');
var dateFormat = require('dateformat');



export default class transaction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: '1',
            // id: this.props.match.params.id,
            type: "",
            date:"",
            amount:"",
            swal: false,
            data: {
                invoices: [],
                invoice_batch: {},
                user: {
                    user: {
                        full_name: ''
                    },
                    student: {
                        program_name: '',
                        nis: ''
                    }
                },
                payment_group: {
                    payment_group_details: [],
                    payments: []
                },

            },
            reason: '',
            isOpenPick: false,
            loading: false,
            visible: false,

        }
        this.toggle = this.toggle.bind(this);
        this.changePage = this.changePage.bind(this);
        this.changePageDetail = this.changePageDetail.bind(this);
        this.getData = this.getData.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.remove = this.remove.bind(this)
        this.onChangeTextArea = this.onChangeTextArea.bind(this)
        this.renderButton = this.renderButton.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
        this.showModal = this.showModal.bind(this)

    }
    componentDidMount() {
        console.log("wawa", this.props.location.state.type)
        let self = this
        axios.request({
            url: `${apiPaymentHost}/payments/group_detail?payment_group_id=${this.props.match.params.id}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                self.setState({
                    data: response.data.data,
                })

            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });

    }
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    showModal(e,date,type,amount) {
        console.log(date,type,amount)
        this.setState({
            visible: true,
            date:date,
            type:type,
            amount:amount
        });
    }
    handleCancel() {
        this.setState({ visible: false });
    }

    onChangeTextArea(event) {
        this.setState({
            reason: event.target.value
        })
    }

    renderButton() {
        const Selector = styled.div`
        margin: 0 0.1rem;
        border-radius: 4px;
        padding: 0.2rem 1.2rem;
        border: solid 1px;
        font-size: 1.2rem;
        margin-bottom: 0.3rem;
        
        ${breakpoint("sm")`
            font-size: 0.9rem;
            padding: 0.2rem 1rem;
        `}
        `;
        const Stop = styled.div`

        button {
            width: 170px;
            height: 30px;
            margin-top: 40%;
            margin-left : 90%;
            border: solid 1px #ffec00;
            background-color: #ffec00;
            cursor: pointer;
            color: #333333;
            font-family: Nunito Sans;
            font-size : 18px;
        }
        
        span {
            margin-right: 1rem;
            margin-top : 1rem;
        }    
        
    `;
        const Export = styled(Selector)`
        width: 170px;
        height: 30px;
        margin-top: 10%;
        margin-left : 90%;
        border: solid 1px #054ee1;
        background-color: #054ee1;
        cursor: pointer;
        color: #ffffff;
        font-family: Nunito Sans;
        font-size : 18px;
        
        span {
            margin-left: 1%;
            margin-top : 1rem;
        }
        
    `;

        // if (stop === 0) {
        const tmpt = (
            <div class="col-md-2" >
                <Stop>
                    <button >Refund</button>
                </Stop>
                <Export>Export Report</Export>
            </div>
        )
        return tmpt;

        // }

    }


    remove(e) {
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.sendUrl()
            this.getData()
        }
        axios({
            url: `${apiPaymentHost}/payments/batch_stoped?id=${this.state.data.invoice_batch.id}`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            data: {
                "reason": this.state.reason
            }
        })
            .then(function (response) {
                refresh()


            })
            .catch(function (error) {

            });
    }



    toggleModal(user_id, id, number, title, status, author, e) {
        this.setState({
            isOpen: !this.state.isOpen,
            // receipt: number,
            // title: title,
            // status: status,
            // author: author,
            // id_circulation: id

        });
        // this.getBorrowedName(user_id)

    }

    changePage() {
        if(this.props.location.state.type  === undefined){
            this.props.history.push({
                pathname: '/detail-payment/'+ this.props.location.state.id,
                state: { id:  this.props.location.state.id}
            })
        }else{
            this.props.history.push({
                pathname: '/payment-transaction/2',
                state: {activeTab:2}
            })

        }
      
    }

    

    changePageDetail() {
        this.props.history.push('/detail-payment')
    }

    getData() {
        let self = this
        axios.request({
            url: `${apiPaymentHost}/payments/batch_get?id=${this.props.location.state.id}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                self.setState({
                    data: response.data.data,
                })
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }

    formatPrice(price) {
        return new Intl.NumberFormat("ID", {
            style: "currency",
            currencyDisplay: "symbol",
            currency: "IDR"
        }).format(price);
    }


    render() {
        const color = {
            backgroundColor: '#edf5ff',
        }

        const margin = {
            marginLeft: '0%',
            marginTop: '3%',
        }

        const Selector = styled.div`
        margin: 0 0.1rem;
        border-radius: 4px;
        padding: 0.2rem 1.2rem;
        border: solid 1px;
        font-size: 1.2rem;
        margin-bottom: 0.3rem;
        
        ${breakpoint("sm")`
            font-size: 0.9rem;
            padding: 0.2rem 1rem;
        `}
        `;

        const Back = styled(Selector)`
            width: 120px;
            height: 44px;
            margin-top: 20%;
            margin-left : 10%;
            border: solid 1px #054ee1;
            background-color: #ffffff;
            cursor: pointer;
            color: #054ee1;
            font-family: Nunito Sans;
            font-size : 18px;
            
            span {
                margin-right: 1rem;
                margin-top : 1rem;
            }    
            
        `;



        const TitlePage = styled.div`
            margin-left:-0.8em;
            margin-top : 10%;
            width: 577px;
            height: 34px;
            font-family: Nunito Sans;
            font-size: 25px;
            font-weight: 600;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: -0.7px;
            color: #233539;
        `


        const Right = styled(Selector)`
            margin-top : 5%;
            margin-left : 8%;
            width: 220px;
            height: 60%;
            border-radius: 5px;
            box-shadow: 0 11px 14px -10px #e5eced;
            border: solid 1px #e5eced;
            background-color: #ffffff;
            div {
                margin-bottom : 2%;
                    h1 {
                    margin-left : 1%;
                    height: 20px;
                    font-family: NunitoSans;
                    font-size: 15px;
                    font-weight: bold;
                    font-style: normal;
                    font-stretch: normal;
                    line-height: normal;
                    letter-spacing: normal;
                    color: #1f1f1f;

                }

                p {
                    margin-left : 1%;
                    height: 20px;
                    font-family: NunitoSans;
                    font-size: 15px;
                    font-weight: normal;
                    font-style: normal;
                    font-stretch: normal;
                    line-height: normal;
                    letter-spacing: normal;
                    color: #233539;
                }
            }
        `
        var amount = 0
        for (let i = 0; i < this.state.data.payment_group.payments.length; i++) {
            amount += this.state.data.payment_group.payments[i].gross_amount;
        }

        var amount_invoice = 0
        for (let i = 0; i < this.state.data.payment_group.payment_group_details.length; i++) {
            amount_invoice += this.state.data.payment_group.payment_group_details[i].invoice.amount_fee;
        }


        // console.log("mount",amount_invoice)
        console.log(this.state.data)

        return (
            <div>
                <NavbarMainPayment status={this.state.type} />
                <Title>Transaction Management</Title>
                <Sidebar>
                    <div class="col-md-3" >
                        <Back onClick={this.changePage} ><span className="glyphicon glyphicon-arrow-left"></span> Back</Back>

                        <Right>
                            <div>
                                <h1>Student ID:</h1>
                                <p>{this.state.data.user.student.nis}</p>
                            </div>
                            <div>
                                <h1>Name:</h1>
                                <p>{this.state.data.user.user.full_name}</p>
                            </div>
                            <div>
                                <h1>Program</h1>
                                <p>{this.state.data.user.student.program_name}</p>
                            </div>
                        </Right>
                    </div>
                    <div class="col-md-9" >
                        <div class="col-md-7" >
                            <TitlePage>Detail Payment- Pay All </TitlePage>
                        </div>
                        {this.renderButton()}
                    </div>
                    <div class="col-md-9" >
                        <P>Invoice List</P>
                        <table className="table table-bordered" style={margin} >
                            <thead className="thead-dark" style={color}>
                                <tr>
                                    <th className={style.fontTable} scope="col">No</th>
                                    <th className={style.fontTable} scope="col">Title</th>
                                    <th className={style.fontTable} scope="col">Payment ID</th>
                                    <th className={style.fontTable} scope="col">Invoice Code</th>
                                    <th className={style.fontTable} scope="col">Status</th>
                                    <th className={style.fontTable} scope="col">Create Date</th>
                                    <th className={style.fontTable} scope="col">Payment Fee</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.data.payment_group.payment_group_details.map(function (arr, i) {
                                        return <tr>
                                            <td className={style.valueTable} >{i + 1}</td>
                                            <td onClick={this.changePageDetail} className={style.popCol} >{arr.invoice.invoice_batch.payment_title}</td>
                                            <td className={style.valueTable} >{arr.invoice.invoice_batch.batch_code}</td>
                                            <td className={style.valueTable} >{arr.invoice.invoice_code === null ? 'N/A' : arr.invoice.invoice_code}</td>
                                            <td className={style.valueTable} >{arr.invoice.payment_status}</td>
                                            <td className={style.valueTable} >{dateFormat(arr.invoice.invoice_batch.create_at, "isoDate") === null ? "N/A" : dateFormat(arr.invoice.invoice_batch.create_at, "isoDate")}</td>
                                            <td className={style.valueTable} >{this.formatPrice(arr.invoice.amount_fee)}</td>

                                            {/* {
                                                arr.user.user_ables.map(function (array, k) {
                                                    return <td className={style.valueTable} >{array.student.program_name}</td>
                                                })
                                            } */}

                                        </tr>
                                    }, this)
                                }
                            </tbody>
                        </table>
                        <Total>Payment Total : </Total> <Value>{this.formatPrice(amount_invoice)}</Value>

                        <P>Tranfer List</P>
                        <table className="table table-bordered" style={margin} >
                            <thead className="thead-dark" style={color}>
                                <tr>
                                    <th className={style.fontTable} scope="col">No</th>
                                    <th className={style.fontTable} scope="col">Paid Nominal</th>
                                    <th className={style.fontTable} scope="col">Status</th>
                                    <th className={style.fontTable} scope="col">Paid Date</th>
                                    <th className={style.fontTable} scope="col">Paid Time</th>
                                    <th className={style.fontTable} scope="col">Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.data.payment_group.payments.map(function (arr, i) {
                                        console.log(arr,"arra")
                                        return <tr>
                                            <td className={style.valueTable} >{i + 1}</td>
                                            <td onClick={this.changePageDetail} className={style.popCol} >{arr.paid_amount === null ? this.formatPrice(arr.gross_amount) : this.formatPrice(arr.paid_amount)}</td>
                                            <td className={style.valueTable} >{arr.payment_status === null ? "N/A" : arr.payment_status}</td>
                                            <td className={style.valueTable} >{arr.transaction_time === null ? "N/A" : dateFormat(arr.transaction_time, "isoDate")}</td>
                                            <td className={style.valueTable} >{arr.transaction_time === null ? "N/A" : dateFormat(arr.transaction_time, "h:MM TT")}</td>
                                            <td className={style.valueTable} onClick={(e) => { this.showModal(e,arr.transaction_time,arr.payment_type,arr.gross_amount) }} ><a>Detail</a></td>
                                        </tr>
                                    }, this)
                                }
                            </tbody>
                        </table>
                        <Total>Paid Total : </Total> <Value>{this.formatPrice(amount)}</Value>
                    </div>
                </Sidebar>
                {/* <Modal shouldCloseOnOverlayClick={true} show={this.state.isOpen} data={this.state.reason} onClosePress={() => this.toggleModal()} onClose={this.toggleModal} onReady={this.remove} >
                    <div>
                        <Mdl>
                            <div>
                                <h1>Detail - {this.state.data.invoice_batch.payment_title}</h1>
                                <h2>Description</h2>
                                <p>{this.state.data.invoice_batch.description}</p>
                                <h4><strong>Create On : </strong> {dateFormat(this.state.data.invoice_batch.created_at, "isoDate")} </h4>
                                <h5>Reason</h5>
                                <textarea className="form-control" value={this.state.reason} onChange={(e) => { this.onChangeTextArea(e) }} style={box} rows="4" cols="50" />
                            </div>
                        </Mdl>
                    </div>
                </Modal> */}
                <Modal
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    width={394}
                    height={1200}
                    footer={null}
                >
                    {/* <div className="col-md-12"> */}
                        <table>
                            <tr>
                                <th style={th}>Payment Date</th>
                                <th style={th}>:</th>
                                <th style={thValue}> {this.state.date === null?'N/A':dateFormat(this.state.date,"isoDate")}</th>
                            </tr>
                            <tr>
                                <th style={th}>Payment Time</th>
                                <th style={th}>:</th>
                                <th style={thValue}> {this.state.date === null?'N/A':dateFormat(this.state.date,"h:MM TT")}</th>
                            </tr>
                            <tr>
                                <th style={th}>Transfered Via</th>
                                <th style={th}>:</th>
                                <th style={thValue}>{this.state.type === null?'N/A':this.state.type}</th>
                            </tr>
                            <tr>
                                <th style={th}>Paid Total</th>
                                <th style={th}>:</th>
                                <th style={thValue}>{this.state.amount === null?'N/A':this.formatPrice(this.state.amou) }</th>
                            </tr>


                        </table>
                    {/* </div> */}

                </Modal>
            </div>
        )
    }
}



const P = styled.p`
    margin-top:1em;
    width: 95px;
    height: 27px;
    font-family: Nunito Sans;
    font-size: 20px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: -0.6px;
    color: #273d52;

`
const Total = styled.p`
    margin-left: 68%;
    width: 106px;
    height: 20px;
    font-family: Nunito Sans;
    font-size: 15px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #273d52;
`

const Value = styled.p`
    margin-left: 85%;
    margin-top : -3.2%;
    width: 99px;
    height: 20px;
    font-family: Nunito Sans;
    font-size: 15px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #898989;
`
const th = {
    marginRight:'20px',
    height: '20px',
    fontFamily: 'Nunito Sans',
    fontSize: '15px',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: '#273d52'
}
const thValue = {
    height: '20px',
    fontFamily: 'Nunito Sans',
    fontSize: '15px',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: '#898989'
}

