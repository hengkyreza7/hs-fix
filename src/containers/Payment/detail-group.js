import React, { Component } from 'react';
import { NavbarMainPayment } from './../../components/Navbar/navbar';
import { Title } from './../../styles/pay';
import Sidebar from './../../components/Layout/layoutInventory';
import styled from 'styled-components';
import Swal from './../../components/Modal/swal';
import breakpoint from "styled-components-breakpoint";
import Check from './../../styles/assets/check.png';
import axios, { post } from 'axios';
// import Modal from './../../components/Modal/stop';
import { apiPaymentHost } from './../../utils/constant'
import {
    Form, Button, Upload, Icon
} from 'antd';
// import './../../styles/global/colorGuide.css';
// import Dropzone from 'react-dropzone';
const style = require('./../../styles/profile/profile.css');



class transaction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: '1',
            // id: this.props.match.params.id,
            type: "payment",
            swal: false,
            isOpen: false,
            data: {
                payment_group: {
                    user: [],
                    invoices: []
                },

            },
            payment_group: [],
            form: {
                reason: '',
                ac_name: '',
                amt_refund: '',
                bank_name: '',
                ac_bank: '',
                file: null,
            },
            file: null,
            isOpenPick: false,
            subject: [
                { value: '000', label: 'Computer science, information & general works' },
                { value: '100', label: 'Philosophy & psychology' },
                { value: '200', label: 'Religion' },
                { value: '300', label: 'Social sciences' },
                { value: '400', label: 'Language' },
                { value: '500', label: 'Science' },
                { value: '600', label: 'Technology' },
                { value: '700', label: 'Arts & recreation' },
                { value: '800', label: "Literature" },
                { value: '900', label: "History & geography" }
            ],
        }
        this.toggle = this.toggle.bind(this);
        this.changePage = this.changePage.bind(this);
        this.changePageDetail = this.changePageDetail.bind(this);
        this.getData = this.getData.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.remove = this.remove.bind(this)
        this.onChangeTextArea = this.onChangeTextArea.bind(this)
        this.renderButton = this.renderButton.bind(this)
        this.onChangeForm = this.onChangeForm.bind(this)
        this.onSubmitData = this.onSubmitData.bind(this)
        this.fileUpload = this.fileUpload.bind(this)
        this.onChange = this.onChange.bind(this)
        this.onDrop = this.onDrop.bind(this)
    }
    componentDidMount() {
        this.getData()
    }
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    onChange(e) {
        let files = e.target.files
        console.log("form", files)
        this.setState({ file: files })
    }

    onChangeForm(e, prop) {
        if (e.target.type === 'file') {
            let dv = this.state.form;
            dv[prop] = e.target.files[0]
            this.setState({ form: dv })
            console.log(this.state.form)
        } else {
            let dv = this.state.form;
            dv[prop] = e.target.value
            this.setState({ form: dv })
        }

    }
    sendUrl() {
        setTimeout(() => {
            this.setState({
                swal: true
            })
        }, 500)

        setTimeout(() => {
            window.location.reload(); 
            this.setState({
                swal: false
            })
        }, 2000)


    }
    onSubmitData(e) {
        this.props.form.validateFields((err, values) => {
            e.preventDefault()
            if (!err) {
                console.log(values.upload[0].originFileObj)
                console.log(this.state.form.file)
                var bodyFormData = new FormData();
                bodyFormData.set('reason', this.state.form.reason);
                bodyFormData.set('amount_fee', this.state.form.amt_refund);
                bodyFormData.set('name_account', this.state.form.ac_name);
                bodyFormData.set('bank_name', this.state.form.bank_name);
                bodyFormData.set('bank_account', this.state.form.ac_bank);
                bodyFormData.append('photo', values.upload[0].originFileObj)

                let self = this
                const refresh = () => {
                    setTimeout(() => {
                        self.setState({ isOpen: false })
                    }, 200)
                    this.sendUrl()
                    this.getData()
                }



                axios({
                    url: `${apiPaymentHost}/refunds/new?payment_group_id=${this.props.match.params.id}`,
                    headers: { 'Content-Type': 'multipart/form-data' },
                    method: 'post',
                    data: bodyFormData

                })
                    .then(function (response) {
                        refresh()


                    })
                    .catch(function (error) {

                    });
            }
        });

    }

    fileUpload(file) {
        const url = `${apiPaymentHost}/refunds/new?payment_group_id=801fe8ed-9845-4426-b7f4-19e030adc28d`;
        const formData = new FormData();
        const refund = [
            formData.append('photo', file)
        ]
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        console.log(post(url, refund, config))
        return post(url, refund, config)
    }


    onChangeTextArea(event) {
        this.setState({
            reason: event.target.value
        })
    }

    onDrop(files) {
        this.setState({
            files
        });
    }

    remove(e) {
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.sendUrl()
            this.getData()
        }
        axios({
            url: `${apiPaymentHost}/payments/batch_stoped?id=${this.state.data.invoice_batch.id}`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            data: {
                "reason": this.state.reason
            }
        })
            .then(function (response) {
                refresh()


            })
            .catch(function (error) {

            });
    }



    toggleModal(user_id, id, number, title, status, author, e) {
        this.setState({
            isOpen: !this.state.isOpen
        });
        // this.getBorrowedName(user_id)

    }
    normFile(e) {

        if (Array.isArray(e)) {
            return e;

        }

        return e && e.fileList;


    }

    changePage() {
        this.props.history.push('/payment-transaction')
    }

    changePageDetail() {
        this.props.history.push('/detail-payment')
    }

    getData() {
        let self = this
        axios.request({
            url: `${apiPaymentHost}/refunds/new?payment_group_id=${this.props.match.params.id}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                self.setState({
                    data: response.data.data,
                })
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }

    formatPrice(price) {
        return new Intl.NumberFormat("ID", {
            style: "currency",
            currencyDisplay: "symbol",
            currency: "IDR"
        }).format(price);
    }

    renderButton() {
        const Selector = styled.div`
        margin: 0 0.1rem;
        border-radius: 4px;
        padding: 0.2rem 1.2rem;
        border: solid 1px;
        font-size: 1.2rem;
        margin-bottom: 0.3rem;
        
        ${breakpoint("sm")`
            font-size: 0.9rem;
            padding: 0.2rem 1rem;
        `}
        `;
        const Stop = styled.div`

        button {
            width: 170px;
            height: 30px;
            margin-top: 40%;
            margin-left : 90%;
            border: solid 1px #ffec00;
            background-color: #ffec00;
            cursor: pointer;
            color: #333333;
            font-family: Nunito Sans;
            font-size : 18px;
        }
        
        span {
            margin-right: 1rem;
            margin-top : 1rem;
        }    
        
    `;
        const Export = styled(Selector)`
        width: 170px;
        height: 30px;
        margin-top: 10%;
        margin-left : 90%;
        border: solid 1px #054ee1;
        background-color: #054ee1;
        cursor: pointer;
        color: #ffffff;
        font-family: Nunito Sans;
        font-size : 18px;
        
        span {
            margin-left: 1%;
            margin-top : 1rem;
        }
        
    `;


        const tmpt = (
            <div class="col-md-2" >
                <Stop>
                </Stop>
                <Export>Export Report</Export>
            </div>
        )
        return tmpt;

    }

    render() {
        console.log(this.state.data.payment_group.invoices, "here")

        const { getFieldDecorator } = this.props.form;
        // const formItemLayout = {
        //     labelCol: { span: 6 },
        //     wrapperCol: { span: 14 },
        // };

        return (
            <div>
                <NavbarMainPayment status={this.state.type} />
                <Title>Transaction Management</Title>
                <Sidebar>
                    <div class="col-md-3" >
                        <Back onClick={this.changePage} ><span className="glyphicon glyphicon-arrow-left"></span> Back</Back>
                        <Right>
                            <div>
                                <h1>Student ID:</h1>
                                <p>{this.state.data.payment_group.user.student === undefined ? 'N/A' : this.state.data.payment_group.user.student.nis}</p>
                            </div>
                            <div>
                                <h1>Name:</h1>
                                <p>{this.state.data.payment_group.user.full_name}</p>
                            </div>
                            <div>
                                <h1>Program</h1>
                                <p>{this.state.data.payment_group.user.student === undefined ? 'N/A' : this.state.data.payment_group.user.student.program_name + '|' + this.state.data.payment_group.user.student.grade_level}</p>
                            </div>
                        </Right>
                        <Right>
                            <div>
                                <h1>Payment List</h1>
                                {
                                    this.state.data.payment_group.invoices.map(function (array, k) {
                                        return <div><p>{array.invoice_batch.payment_title}</p></div>
                                    })
                                }

                            </div>

                        </Right>
                    </div>
                    <div class="col-md-9" >
                        <div class="col-md-7" >
                            <TitlePage>Detail Payment- Pay All </TitlePage>
                        </div>
                        {this.renderButton()}

                    </div>
                    <div class="col-md-6" >
                        <form encType="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Reason</label>
                                <textarea className="form-control" id="exampleFormControlTextarea1" onChange={(e) => { this.onChangeForm(e, 'reason') }} value={this.state.form.reason} rows="3"></textarea>
                            </div>
                            <div className="form-group">
                                <label for="exampleInputEmail1">Amount of Refund</label>
                                <input type="text" className="form-control" onChange={(e) => { this.onChangeForm(e, 'amt_refund') }} value={this.state.form.amt_refund} />
                            </div>
                            <div className="form-group">
                                <label for="exampleInputEmail1">Account Name</label>
                                <input type="text" className="form-control" onChange={(e) => { this.onChangeForm(e, 'ac_name') }} value={this.state.form.ac_name} />
                            </div>
                            <div className="form-group">
                                <label for="exampleInputEmail1">Bank Name</label>
                                <input type="text" className="form-control" onChange={(e) => { this.onChangeForm(e, 'bank_name') }} value={this.state.form.bank_name} />
                            </div>
                            <div className="form-group">
                                <label>Bank Account Number</label>
                                <input type="text" className="form-control" onChange={(e) => { this.onChangeForm(e, 'ac_bank') }} value={this.state.form.ac_bank} />
                            </div>
                            <div className="dropbox">
                                {getFieldDecorator('upload', {
                                    valuePropName: 'fileList',
                                    getValueFromEvent: this.normFile,
                                })(
                                    <Upload.Dragger name="files">
                                        <p className="ant-upload-drag-icon">
                                            <Icon type="camera" />
                                        </p>
                                        <p className="ant-upload-text">Upload Picture</p>
                                    </Upload.Dragger>
                                )}
                            </div>
                            <Button type="primary" onClick={(e) => { this.onSubmitData(e) }} size="small">Primary</Button>
                        </form>

                    </div>
                </Sidebar>
                <Swal show={this.state.swal} >
                    <div>
                        <img src={Check} alt="check" className={style.Path} />
                        <p className={style.swal}>New item has been added.</p>
                    </div>
                </Swal>
            </div>
        )
    }
}
export default Form.create()(transaction);
// const P = styled.p`
//     margin-top:1em;
//     width: 95px;
//     height: 27px;
//     font-family: Nunito Sans;
//     font-size: 20px;
//     font-weight: 600;
//     font-style: normal;
//     font-stretch: normal;
//     line-height: normal;
//     letter-spacing: -0.6px;
//     color: #273d52;

// `
// const Total = styled.p`
//     margin-left: 68%;
//     width: 106px;
//     height: 20px;
//     font-family: Nunito Sans;
//     font-size: 15px;
//     font-weight: bold;
//     font-style: normal;
//     font-stretch: normal;
//     line-height: normal;
//     letter-spacing: normal;
//     color: #273d52;
// `

// const Value = styled.p`
//     margin-left: 85%;
//     margin-top : -3.2%;
//     width: 99px;
//     height: 20px;
//     font-family: Nunito Sans;
//     font-size: 15px;
//     font-weight: normal;
//     font-style: normal;
//     font-stretch: normal;
//     line-height: normal;
//     letter-spacing: normal;
//     color: #898989;
// `

// const color = {
//     backgroundColor: '#edf5ff',
// }

// const margin = {
//     marginLeft: '0%',
//     marginTop: '3%',
// }

const Selector = styled.div`
margin: 0 0.1rem;
border-radius: 4px;
padding: 0.2rem 1.2rem;
border: solid 1px;
font-size: 1.2rem;
margin-bottom: 0.3rem;

${breakpoint("sm")`
    font-size: 0.9rem;
    padding: 0.2rem 1rem;
`}
`;

const Back = styled(Selector)`
    width: 120px;
    height: 44px;
    margin-top: 20%;
    margin-left : 10%;
    border: solid 1px #054ee1;
    background-color: #ffffff;
    cursor: pointer;
    color: #054ee1;
    font-family: Nunito Sans;
    font-size : 18px;
    
    span {
        margin-right: 1rem;
        margin-top : 1rem;
    }    
    
`;



const TitlePage = styled.div`
    margin-top : 10%;
    width: 577px;
    height: 34px;
    font-family: Nunito Sans;
    font-size: 25px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: -0.7px;
    color: #233539;
`

const Right = styled(Selector)`
    margin-top : 5%;
    margin-left : 8%;
    width: 220px;
    height: 60%;
    border-radius: 5px;
    box-shadow: 0 11px 14px -10px #e5eced;
    border: solid 1px #e5eced;
    background-color: #ffffff;
    div {
        margin-bottom : 2%;
            h1 {
            margin-left : 1%;
            height: 20px;
            font-family: NunitoSans;
            font-size: 15px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #1f1f1f;

        }

        p {
            margin-left : 1%;
            height: 20px;
            font-family: NunitoSans;
            font-size: 15px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #233539;
        }
    }
`

// const Mdl = styled.div`
//     h1{
//         width: 160px;
//         height: 27px;
//         font-family: Nunito Sans;
//         font-size: 20px;
//         font-weight: 600;
//         font-style: normal;
//         font-stretch: normal;
//         line-height: normal;
//         letter-spacing: -0.6px;
//         color: #233539;
//     }

//     h2 {
//         width: 95px;
//         height: 20px;
//         font-family: Nunito Sans;
//         font-size: 15px;
//         font-weight: bold;
//         font-style: normal;
//         font-stretch: normal;
//         line-height: normal;
//         letter-spacing: normal;
//         color: #1f1f1f;

//     }

//     h3 {
//         width: 342px;
//         height: 28px;
//         font-family: Nunito Sans;
//         font-size: 13px;
//         font-weight: normal;
//         font-style: normal;
//         font-stretch: normal;
//         line-height: 2.15;
//         letter-spacing: normal;
//         color: #323c47;
//     }

//     p {
//         height: 28px;
//         font-family: NunitoSans;
//         font-size: 13px;
//         font-weight: normal;
//         font-style: normal;
//         font-stretch: normal;
//         line-height: 2.15;
//         letter-spacing: normal;
//         color: #323c47;
//     }
//     h4 {
//         height: 20px;
//         font-family: Nunito Sans;
//         font-size: 15px;
//         font-weight: normal;
//         font-style: normal;
//         font-stretch: normal;
//         line-height: normal;
//         letter-spacing: normal;
//         color: #1f1f1f;
//     }

//     h5 {
//         height: 15px;
//         font-family: Nunito Sans;
//         font-size: 11px;
//         font-weight: normal;
//         font-style: normal;
//         font-stretch: normal;
//         line-height: normal;
//         letter-spacing: 0.1px;
//         color: #323c47;
//     }

//         html, body, .container {
//           height: 100%;
//         }
//         textarea.form-control {
//             height: 300%;
//             width: 331px;
//             height: 198px;
//             border-radius: 4px;
//             border: solid 1px #e9eff4;
//             background-color: #ffffff;
//             font-family: NunitoSans;
//             font-size: 15px;
//             font-weight: normal;
//             font-style: normal;
//             font-stretch: normal;
//             line-height: 1.8;
//             letter-spacing: -0.2px;
//             color: #323c47;
//         }
// `
// const box = {
//     height: '80%'
// }

// import React, { Component } from 'react';
// import axios, { post } from 'axios';
// import { apiPaymentHost } from './../../utils/constant'


// export default class SimpleReactFileUpload extends Component {

//   constructor(props) {
//     super(props);
//     this.state ={
//       file:null
//     }
//     this.onFormSubmit = this.onFormSubmit.bind(this)
//     this.onChange = this.onChange.bind(this)
//     this.fileUpload = this.fileUpload.bind(this)
//   }
//   onFormSubmit(e){
//     e.preventDefault() // Stop form submit
//     this.fileUpload(this.state.file).then((response)=>{
//       console.log(response.data);
//     })
//   }
//   onChange(e) {
//     this.setState({file:e.target.files[0]})
//   }
//   fileUpload(file){
//     const url =  `${apiPaymentHost}/refunds/new?payment_group_id=801fe8ed-9845-4426-b7f4-19e030adc28d`
//     const formData = new FormData();
//     formData.append('files',file)
//     console.log(formData)
//     const config = {
//         headers: {
//             'content-type': 'multipart/form-data'
//         }
//     }
//     return  post(url, formData,config)
//   }

//   render() {
//     return (
//       <form onSubmit={this.onFormSubmit}>
//         <h1>File Upload</h1>
//         <input type="file" onChange={this.onChange} />
//         <button type="submit">Upload</button>
//       </form>
//    )
//   }
// }