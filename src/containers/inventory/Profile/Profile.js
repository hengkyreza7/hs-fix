import React, { Component } from 'react';
import Modal from './../../../components/Modal/modal';
import Swal from './../../../components/Modal/swal';
import axios from 'axios';
import { apiHost } from './../../../utils/constant';
import Check from './../../../styles/assets/check.png';
import { NavbarRegister } from './../../../components/Navbar/navbar';
const style = require('./../../../styles/profile/profile.css');

class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            password:"",
            isOpen: false,
            swal: false,
            data: this.props.location.state.data,
            email:this.props.location.state.data.email,
            phone :this.props.location.state.data.phone_number,
            subject: this.props.location.state.data.subject,
            name: this.props.location.state.data.full_name,
           

        }
        this.onChange = this.onChange.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
        this.handleBtnReady = this.handleBtnReady.bind(this)
        this.generate = this.generate.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handlePhone = this.handlePhone.bind(this)
        this.handlePassword = this.handlePassword.bind(this)
    }
    componentDidMount() {
        //did mount
        console.log(this.props.location.state.data)
    }

    onChange(event) {
        this.setState({
            name: event.target.value
        })
    }
    toggleModal() {
        this.setState({
            isOpen: !this.state.isOpen
        });

    }
    
    
    sendUrl() {
        setTimeout(() => {
            this.setState({
                swal: true
            })
        }, 500)
        if(this.props.location.state.data.model_type === 'librarians'){
            setTimeout(() => {
                this.props.history.push({
                    pathname: '/inventory-management',
                    state: { detail: this.state.name }
                })
            }, 2000)
        }else{
            setTimeout(() => {
                this.props.history.push({
                    pathname: '/payment-transaction/1',
                    state: { detail: this.state.name }
                })
            }, 2000)
        }
       


    }
    handleInputChange(e) {
        const { id, value } = e.target;
        let { data } = this.state;
        const targetIndex = data.findIndex(datum => {
            return datum.invoice_number === id;
        });

        if (targetIndex !== -1) {
            data[targetIndex].some_value = value;
            this.setState({ data });
        }
    }

    handlePassword(event){
        this.setState({
            password: event.target.value
        })
    }
    handlePhone(event){
        this.setState({
            phone: event.target.value
        })
    }

    handleBtnReady(e) {
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.sendUrl()
        }
        const url =  `${apiHost}/users/${self.state.data.user_id}`
        console.log(self.state.data,"0")
        axios({
            url: url,
            headers: {
                'Content-Type': 'application/json',
            },
            method: 'PUT',
            data: {
                "email": self.state.data.email,
                "full_name": self.state.name,
                "dob": self.state.data.dob,
                "phone_number": self.state.phone,
            }
        })
            .then(function (response) {
                axios({
                    url: `${apiHost}/users/${self.state.data.user_id}/passwords`,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    method: 'POST',
                    data: {
                       "password" : self.state.password,
                       "verify_password" : self.state.password
                    }
                })
                    .then(function (response) {
                        
                        axios({
                            url: `${apiHost}/login`,
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            method: 'POST',
                            data: {
                               "email" : self.state.email,
                               "password" : self.state.password
                            }
                        })
                            .then(function (response) {
                                localStorage.setItem("_token", response.data.data.token)
                                localStorage.setItem("_userID", self.state.data.model_id)
                                refresh()
                
                            })
                            .catch(function (error) {
                                // refresh()
                                console.log("here erro")
                                // this.setState({
                                //     isOpen: !this.state.isOpen
                                //   });
                            });
        
                    })
                    .catch(function (error) {
                        // refresh()
        
                        // this.setState({
                        //     isOpen: !this.state.isOpen
                        //   });
                    });
                

            })
            .catch(function (error) {
                // refresh()

                // this.setState({
                //     isOpen: !this.state.isOpen
                //   });
            });
    }
    generate() {
        const temp = (
            <div>
                {
                    this.state.subject.map(function (sbj, i) {
                        return <input key={i} type="text" value={sbj.name} className="form-control" readOnly={true} />
                    })
                }
            </div>
        )
        return temp;
    }



    render() {

        const arr = []
        for(let i in this.state.data.subjects){
            const a = this.state.data.subjects[i]
            arr.push(a.name)
        }

        const model = []
        for(let i in this.state.data.roles){
            const b = this.state.data.roles[i]
            model.push(b.model_id)
        }
        console.log(model)
        return (
            <div>
                <NavbarRegister />
                <div className={style.mainContent}>
                    <p className={style.title}>Edit Profile  </p>
                    <div className={style.box}>
                       <div>
                            <form>
                                <div className={style.formGroup}>
                                    <label className={style.label}>NAME</label>
                                    <div className={style.form}>
                                      <input type="text" className="form-control" onChange={this.onChange}  value={this.state.name} />
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label} >EMAIL</label>
                                    <div className={style.form}>
                                      <input type="email" className="form-control" value={this.state.email} onChange={this.onChange}  />
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label} >PHONE NUMBER</label>
                                    <div className={style.form}>
                                      <input type="text" className="form-control" onChange={this.handlePhone}  value={this.state.phone} />
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label}>NEW PASSWORD</label>
                                    <div className={style.form}>
                                      <input type="password" className="form-control" value={this.state.password} onChange={this.handlePassword} />
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label}>VERIFY PASSWORD</label>
                                    <div className={style.form}>
                                      <input type="password" className="form-control" />
                                    </div>
                                </div>
                            </form>    
                            <div className={style.footer}>
                                    <button type="submit" className={style.btn} onClick={this.toggleModal} >Register</button>
                            </div>
                            <Modal show={this.state.isOpen} onReady={(e) => {this.handleBtnReady(e)}}  onClose={this.toggleModal}>
                                <div>
                                <h3 className={style.MessageBox}> IMPORTANT</h3>
                                <p className={style.MessageBox2}>You can only do this once. 
                                    Changes beyond this point only able to be done by contacting school’s admin.</p>
                                </div>
                            </Modal>
                            <div>
                                <Swal show={this.state.swal} >
                                    <div>
                                    <img  src={Check} alt="check" className={style.Path} />
                                    <p className={style.swal}>Your Account  has been activated.</p>
                                    </div>
                                </Swal>
                            </div>
                       </div>   
                    </div>
                </div>              

            </div>
        )
    }
}
export default Profile;