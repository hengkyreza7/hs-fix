import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import logo from './../../../styles/assets/HighScope.svg';
import axios from 'axios';
import { apiHost } from './../../../utils/constant';
import { Grid, GridItem } from 'styled-grid-component';
import { LeftCol, RightCol } from './../../../styles/register';
import Seo from './../../../components/Seo/Seo';
import NotificationSystem from 'react-notification-system';
const style = require('./../../../styles/scss/register.css');
const notification = require('./../../../styles/global/notification');
const title = 'Welcome to HighScope Indonesia'

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: "",
            password: "",
            _notificationSystem: null,
            data: "",
            title: 'login'
        }
        this.onChangeForm = this.onChangeForm.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
    }

    onChangeForm(event) {
        this.setState({
            username: event.target.value,
        })
    }
    onChangePassword(event) {
        this.setState({
            password: event.target.value
        })
    }

    changepage(type) {
        console.log(type, "reactjs")
        var self = this
        if (type === 'librarians') {
            self.props.history.push({
                pathname: '/inventory-management',
                state: { dataDetail: self.state.data }
            })

        } else {
            self.props.history.push({
                pathname: '/payment-transaction/1',
                state: { dataDetail: self.state.data }
            })
        }


    }

    showNotification(color, msg) {
        this.setState({ _notificationSystem: this.refs.notificationSystem });
        var _notificationSystem = this.refs.notificationSystem;
        var level, ikon;
        switch (color) {
            case 1:
                level = 'success';
                ikon = 'pe-7s-check';
                break;
            case 2:
                level = 'warning';
                ikon = 'pe-7s-attention';
                break;
            case 3:
                level = 'error';
                ikon = 'pe-7s-close-circle';
                break;
            case 4:
                level = 'info';
                ikon = 'pe-7s-info';
                break;
            default:
                break;
        }
        _notificationSystem.addNotification({
            title: (<span data-notify="icon" className={ikon}></span>),
            message: (
                <div>
                    {msg}
                </div>
            ),
            level: level,
            position: "tr",
            autoDismiss: 2,
        });
    }

    createAction(e) {
        e.preventDefault()
        let self = this
        const Redirect = (type) => {
            self.changepage(type)
        }
        const errors = (color, msg) => {
            self.showNotification(color, msg)
        }

        axios.request({
            url: `${apiHost}/login`,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            method: 'POST',
            data: {
                "email": self.state.username,
                "password": self.state.password
            }
        })
            .then(function (response) {
                console.log(response.data.data)
                var type = []
                for (let i in response.data.data.roles) {
                    let collect = response.data.data.roles[i]
                    type.push(collect)
                }

                localStorage.setItem("_token", response.data.data.token)
                localStorage.setItem("_userID", type[0].model_id)
                self.setState({
                    data: response.data.data
                })
                Redirect(type[0].model_type)

            })
            .catch(function (error) {
                let code = error.response.data.error.http_code
                if (code === 400) {
                    errors(3, 'E-mail or password not correct')
                } else if (code === 422 && self.state.username === '' && self.state.password !== '') {
                    errors(3, 'User id cannot be empty')
                }else if(code === 404){
                    errors(3,'Not found')
                }else {
                    errors(3, 'password cannot be empty')
                }
            });


    }


    render() {
        const { username, password } = this.state
        return (

            <Grid
                width="100%"
                height="100vh"
                templateColumns="repeat(2, 1fr)"
                autoRows="minmax(100px, auto)"
            >
                <Seo title={this.state.title} />
                <GridItem column="1">
                    <LeftCol >
                        <div>
                            {title}
                        </div>

                    </LeftCol>
                </GridItem>
                <GridItem column="2">
                    <RightCol>
                        <img src={logo} alt="logo" width="204px" height="100px" />
                        <form>
                            <div>
                                <input type="text" value={username} onChange={this.onChangeForm} required="required" />
                                <span className={style.highlight}></span><p></p>
                                <label>Email</label>
                            </div>
                            <div>
                                <input type="password" value={password} onChange={this.onChangePassword} required="required" />
                                <span className={style.highlight}></span><p></p>
                                <label>Password</label>
                            </div>
                            <footer>
                                <button onClick={(e) => { this.createAction(e) }}>Login </button>
                                <span>Back to <Link to="/"> Register </Link> Page</span>
                            </footer>
                        </form>
                        <NotificationSystem ref="notificationSystem" style={notification.style} />
                    </RightCol>
                </GridItem>
            </Grid>
            // <div className={style.registerPage}>
            //     <div className={style.rectangleCol}>
            //         <div className={style.WelcometoHighScope}>
            //             Welcome to HighScope Indonesia
            //         </div>
            //         <div className={style.logo}>
            //             <img src={logo} alt="logo" width="200px" />
            //         </div>
            //         <div>
            //             <form className={style.formPosition}>
            //                 {/* <Spinner /> */}
            //                 <div className={style.group}>
            //                     <input type="text" value={username} className={style.input} onChange={this.onChangeForm} required="required" />
            //                     <span className={style.highlight}></span><span className={style.bar}></span>
            //                     <label className={style.label}>Username</label>
            //                 </div>
            //                 <div className={style.group}>
            //                     <input type="password" className={style.input} value={password} onChange={this.onChangePassword} required="required" />
            //                     <span className={style.highlight}></span><span className={style.bar}></span>
            //                     <label className={style.label} >Password</label>
            //                 </div>
            //                 <div>
            //                     <input type="submit" className={style.btn} onClick={(e) => { this.createAction(e) }} value="Login" />
            //                 </div>
            //             </form>
            //             <div className={style.haveAccountPosition}>
            //                 {/* <span className={style.textLogin}><Link to="/forget-password"> Forget password?</Link></span> */}

            //             </div>
            //         </div>
            //     </div>


            // </div>
        )
    }
}


export default Login
