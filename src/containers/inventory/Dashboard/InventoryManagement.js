import React, { Component } from 'react'
import { NavbarMain } from '../../../components/Navbar/navbar';
import Sidebar from '../../../components/Layout/layoutInventory';
import axios from 'axios';
import { apiLibHost, apiHost } from './../../../utils/constant';
import { Redirect } from 'react-router-dom';
import { Button } from 'styled-bootstrap-components';
import Swal from './../../../components/Modal/swal';
import Check from './../../../styles/assets/check.png';
import Modal from './../../../components/Modal/return';
import ModalPick from './../../../components/Modal/pick';
import Pagination from './../../../components/pagination/index';
import Books from './../../../styles/assets/user.svg';
import Spinner from './../../../components/loader/index';
import Seo from './../../../components/Seo/Seo';
import { Input, Select } from 'antd';
import cx from 'classnames';
// import { getFullName } from './../../../utils/helper'

const style = require('./../../../styles/dashboard/dashboard.css');
const Search = Input.Search;
const Option = Select.Option;


export default class InventroryManagement extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {
                data: [],
                circulations: []
            },
            isOpen: false,
            isOpenPick: false,
            user: "",
            name: "",
            swal: false,
            swalExp: false,
            swalPick: false,
            swalReject: false,
            receipt: '',
            status: '',
            author: '',
            title: '',
            id_circulation: '',
            pageOfItems: [],
            dataname:[],
            loading: false,
            find: '',
            page: 1,
            total_page: 0,
            statusOps: [
                { value: 'RESERVED' },
                { value: 'REJECTED' },
                { value: 'PICKEDUP' },
                { value: 'EXPIRED' },
                { value: 'RETURNED' },
            ]

        }
        this.gennerateTemp = this.gennerateTemp.bind(this)
        this.getData = this.getData.bind(this)
        this.return = this.return.bind(this)
        this.pickUp = this.pickUp.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
        this.onChangePage = this.onChangePage.bind(this)
        this.getUser = this.getUser.bind(this)
        this.getBorrowedName = this.getBorrowedName.bind(this)
        this.toggleModalPickUp = this.toggleModalPickUp.bind(this)
        this.expired = this.expired.bind(this)
        this.reject = this.reject.bind(this)
        this.find = this.find.bind(this)
        this.onChange = this.onChange.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.getFullNames = this.getFullNames.bind(this)
        this.generateFooter = this.generateFooter.bind(this)
    
    }

    componentDidMount() {
        this.getData()
        this.getUser()
    }

    find(value) {
        // this.setState({ pageOfItems: pageOfItems });
        this.getData(value)
    }

    handleChange(status) {
        this.getData(status)
    }
    getData(value, status) {
        console.log(value, status)
        let self = this
        var url = '';
        if (value === undefined && status === undefined) {
            url = `${apiLibHost}/circulations`
        } else if (value === 'REJECTED' || value === 'RESERVED' || value === 'EXPIRED' || value === 'PICKEDUP' || value === 'RETURNED') {
            url = `${apiLibHost}/circulations?status=${value}`
        } else {
            url = `${apiLibHost}/circulations?number=${value}`
        }
        console.log(url)
        axios.request({
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                response.data.data.circulations.map(function (j, k) {
                    return response.data.data.circulations[k].no = (k + 1)
                })
                self.setState({
                    data: response.data.data,
                    loading: true,
                    page: response.data.meta.pagination.page,
                    total_page: response.data.meta.pagination.total_page
                })


            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }
    onChange(event) {
        this.setState({
            receipt: event.target.value
        })
    }
    getUser() {
        let self = this
        axios.request({
            url: `${apiHost}/librarians/${localStorage.getItem("_userID")}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                self.setState({
                    user: response.data.data,
                })
                localStorage.setItem("_username", response.data.data.full_name)

            })
            .catch(function (error) {
                console.log("here")
            });
    }

    getBorrowedName(user_id) {
        let self = this
        axios.request({
            url: `${apiHost}/users/${user_id}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                self.setState({
                    name: response.data.data,
                })
                console.log(response.data.data, "name")

            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }


    toggleModal(user_id, id, number, title, status, author, e) {
        this.setState({
            isOpen: !this.state.isOpen,
            receipt: number,
            title: title,
            status: status,
            author: author,
            id_circulation: id

        });
        this.getBorrowedName(user_id)

    }

    toggleModalPickUp(user_id, id, number, title, status, author, e) {
        this.setState({
            isOpenPick: !this.state.isOpenPick,
            receipt: number,
            title: title,
            status: status,
            author: author,
            id_circulation: id

        });
        this.getBorrowedName(user_id)

    }


    return(e) {
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.sendUrl()
            this.getData()
        }
        axios({
            url: `${apiLibHost}/circulations/return`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            data: {
                "id": this.state.id_circulation,
                "number": this.state.receipt
            }
        })
            .then(function (response) {
                refresh()


            })
            .catch(function (error) {

            });
    }

    pickUp(e) {
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ isOpenPick: false })
            }, 200)
            this.sendUrlPick()
            this.getData()
        }
        axios({
            url: `${apiLibHost}/circulations/pickup`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            data: {
                "id": this.state.id_circulation,
                "number": this.state.receipt
            }
        })
            .then(function (response) {
                refresh()

            })
            .catch(function (error) {

            });
    }
    reject(e) {
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ isOpenPick: false })
            }, 200)
            this.sendUrlReject()
            this.getData()
        }
        axios({
            url: `${apiLibHost}/circulations/reject`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            data: {
                "id": this.state.id_circulation,
                "number": this.state.receipt
            }
        })
            .then(function (response) {
                refresh()

            })
            .catch(function (error) {

            });
    }
    expired(id, number, e) {
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.sendUrlExp()
            this.getData()
        }
        axios({
            url: `${apiLibHost}/circulations/set-expired`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            data: {
                "id": id,
                "number": number
            }
        })
            .then(function (response) {
                refresh()

            })
            .catch(function (error) {

            });
    }
    onChangePage(page) {
        let self = this
        var url = '';
        if (this.state.receipt === '') {
            url = `${apiLibHost}/circulations?page=${page}`
        } else {
            url = `${apiLibHost}/circulations?number=${this.state.receipt}`
        }
        console.log(url)
        axios.request({
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                response.data.data.circulations.map(function (j, k) {
                    return response.data.data.circulations[k].no = (k + 1)
                })
                self.setState({
                    data: response.data.data,
                    loading: true,
                    page: response.data.meta.pagination.page,
                    total_page: response.data.meta.pagination.total_page
                })


            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }
    



    sendUrl() {
        setTimeout(() => {
            this.setState({
                swal: true
            })
        }, 500)

        setTimeout(() => {
            this.setState({
                swal: false
            })
        }, 2000)
    }

    sendUrlReject() {
        setTimeout(() => {
            this.setState({
                swalReject: true
            })
        }, 500)

        setTimeout(() => {
            this.setState({
                swalReject: false
            })
        }, 2000)
    }

    sendUrlExp() {
        setTimeout(() => {
            this.setState({
                swalExp: true
            })
        }, 500)

        setTimeout(() => {
            this.setState({
                swalExp: false
            })
        }, 2000)
    }

    sendUrlPick() {
        setTimeout(() => {
            this.setState({
                swalPick: true
            })
        }, 500)

        setTimeout(() => {
            this.setState({
                swalPick: false
            })
        }, 2000)
    }

    gennerateTemp() {
        const a = (
            this.state.data.circulations.map(function (arr, i) {
                return (<div style={{ textAlign: 'left', color: 'grey' }}>{arr.inventory.title}</div>)
            })
        )

        return a;
    }

    changepage() {
        // this.setState({ redirect: true });
        console.log("console")
        var self = this
        self.props.history.push('/login');
    }
     getFullNames(e, kode) {
         console.log(kode)
        // e.preventDefault()
        let self = this;
        let url = `${apiHost}/users/${kode}`
        // let name = 'a'
          axios.request({
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                self.setState({
                    dataname: response.data.data
                })
                console.log(self.state.dataname.full_name,"hew")

            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });

            const tmp = (
                <div>
                     {this.state.dataname.full_name}
                </div>
                
            )
            return tmp
            
      }

    generateFooter() {
        console.log(this.state.status ,"PICKEDUP")
        if (this.state.status === 'PICKEDUP') {
            const footer = (
                <div className="footer">
                    <div>
                        <input type="submit" value="Returned" style={post} className={style.btn} onClick={this.return} />
                    </div>
                </div>
            )
            return footer;
        } else {
            const footer = (
                <div>
                
                </div>
            )
            return footer;
        }
    }

    render() {
        console.log(this.state.data,"wa")
        const color = {
            backgroundColor: '#edf5ff'
        }
        const { redirect } = this.state
        if (redirect) {
            return <Redirect to="/" />;
        }
        let content;
        if (this.state.loading) {
            content = <Sidebar  >
                <div className="col-sm-12" style={margin}>
                    <div className="col-sm-4">
                        <Search
                            placeholder="Find your receipt"
                            onSearch={value => this.find(value)}
                            style={{ width: 320, marginRight: 20, height: 48 }}
                        />

                    </div>
                    <div>
                        <Select
                            allowClear={true}
                            showSearch
                            style={{ width: 300, marginTop: 10, marginRight: 20 }}
                            placeholder="Sort by status "
                            optionFilterProp="children"
                            onChange={this.handleChange}
                            onFocus={this.handleFocus}
                            onBlur={this.handleBlur}
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        >
                            {this.state.statusOps.map(function (arr, i) {
                                return <Option value={arr.value}>{arr.value}</Option>
                            })
                            }
                        </Select>
                    </div>
                </div>
                <div className={style.boxTableInv}>
                    <table className="table table-bordered table-striped">
                        <thead className="thead-dark" style={color}>
                            <tr>
                                <th className={style.fontTable} scope="col">No</th>
                                <th className={style.fontTable} scope="col">Title</th>
                                <th className={style.fontTable} scope="col">Author</th>
                                <th className={style.fontTable} scope="col">Name of Borrower</th>
                                <th className={style.fontTable} scope="col">Receipt</th>
                                <th className={style.fontTable} scope="col">Date Out</th>
                                <th className={style.fontTable} scope="col">Date In</th>
                                <th className={style.fontTable} scope="col">Status</th>
                                <th className={style.fontTable} >Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.data.circulations.map(function (array, j) {
                                    return <tr key={j}>
                                        <td className={style.valueTable} >{array.no}</td>
                                        <td className={style.valueTable}>{array.inventory.title}</td>
                                        <td className={style.valueTable}>{array.inventory.author}</td>
                                        <td className={style.valueTable}>{(e) => {this.getFullNames(array.user_id)}}</td>
                                        {/* {array.status === 'PICKEDUP' ? <td className={style.popCol} onClick={(e) => { this.toggleModal(array.user_id, array.id, array.number, array.inventory.title, array.status, array.inventory.author, e) }}><a> {array.number} </a></td> : <td className={style.valueTable}> {array.number}</td>} */}
                                        <td className={style.popCol} onClick={(e) => { this.toggleModal(array.user_id, array.id, array.number, array.inventory.title, array.status, array.inventory.author, e) }}><a> {array.number} </a></td>
                                        <td className={style.valueTable}>{array.pickup_date}</td>
                                        <td className={style.valueTable}>{array.return_date}</td>
                                        <td className={style.valueTable}>{array.status === 'PICKEDUP' ? <p className={style.badgeKuning}>{array.status}</p> : array.status === 'RETURNED' ? <p className={style.badgeHijau}>{array.status} </p> : array.status === 'RESERVED' ? <p className={style.badgeBiru}>{array.status} </p> : <p className={style.badgeRed}>{array.status} </p>}</td>
                                        <td className={style.valueTable} width="135px" > {array.status === 'PICKEDUP' || array.status === 'EXPIRED' || array.status === 'REJECTED' || array.status === 'RETURNED' ? <div><Button onClick={(e) => { this.toggleModalPickUp(array.id, array.number, e) }} secondary disabled={true}>Pick Up</Button>  <Button onClick={(e) => { this.expired(array.id, array.number, e) }} danger disabled={true}>Expired</Button></div> : <div><Button onClick={(e) => { this.toggleModalPickUp(array.user_id, array.id, array.number, array.inventory.title, array.status, array.inventory.author, e) }} secondary disabled={false}>Pick Up</Button>  <Button onClick={(e) => { this.expired(array.id, array.number, e) }} danger disabled={false}>Expired</Button></div>}</td>
                                    </tr>
                                }, this)
                            }
                        </tbody>
                    </table>
                    <div className={style.page}>
                        {/* <Pagination items={this.state.data.circulations} onChangePage={this.onChangePage} /> */}
                        <Pagination
                            margin={2}
                            page={this.state.page}
                            count={Math.ceil(this.state.total_page)}
                            onPageChange={this.onChangePage}
                        />
                    </div>

                </div>
            </Sidebar>
        } else {
            content = <Spinner />
        }

        return (
            <div className={style.dashboardPage}>
                <Seo title={this.state.user.model_type} />
                <NavbarMain data={this.state.user} onSubmit={this.changepage} />
                <div className={style.mainContent}>
                    {/* <Sidebar /> */}
                    {content}
                    <Swal show={this.state.swal} >
                        <div>
                            <img src={Check} alt="check" className={style.Path} />
                            <p className={style.swal}>Book has been Return.</p>
                        </div>
                    </Swal>
                    <Swal show={this.state.swalReject} >
                        <div>
                            <img src={Check} alt="check" className={style.Path} />
                            <p className={style.swal}>Reserve has been reject.</p>
                        </div>
                    </Swal>
                    <Swal show={this.state.swalExp} >
                        <div>
                            <img src={Check} alt="check" className={style.Path} />
                            <p className={style.swal}>Book has been expired.</p>
                        </div>
                    </Swal>
                    <Swal show={this.state.swalPick} >
                        <div>
                            <img src={Check} alt="check" className={style.Path} />
                            <p className={style.swal}>Book has been pick up.</p>
                        </div>
                    </Swal>
                    <Modal shouldCloseOnOverlayClick={true} show={this.state.isOpen} data={this.state.status} onClosePress={() => this.toggleModal()} onClose={this.toggleModal} >
                        <div>
                            <div className={style.position}>
                                <img src={Books} alt="logo" />
                                <hr />
                            </div>
                            <div className={style.infoCover}>
                                <h5>{this.state.title} </h5>
                                <p>by : {this.state.author}</p>
                            </div>
                            <div className={cx(style.data,style.inclineline)}>
                                <p>Receipt</p>
                                <p>{this.state.receipt}</p>
                            </div>
                            <div className={cx(style.nama,style.inclineline)}>
                                <p>Borrower’s Name</p>
                                <p>{this.state.name.full_name}</p>
                            </div>
                            <div className={cx(style.stts,style.inclineline)}>
                                <p>Status</p>
                                <p>{this.state.status}</p>
                            </div>
                        </div>
                        {this.generateFooter()}
                    </Modal>
                    <ModalPick shouldCloseOnOverlayClick={true} data={this.state.status} show={this.state.isOpenPick} onClosePress={() => this.toggleModalPickUp()} onClose={this.toggleModalPickUp} onReady={(e) => { this.pickUp(e) }} onReject={(e) => { this.reject(e) }} >
                    <div>
                            <div className={style.position}>
                                <img src={Books} alt="logo" />
                                <hr />
                            </div>
                            <div className={style.infoCover}>
                                <h5>{this.state.title} </h5>
                                <p>by : {this.state.author}</p>
                            </div>
                            <div className={cx(style.data,style.inclineline)}>
                                <p>Receipt</p>
                                <p>{this.state.receipt}</p>
                            </div>
                            <div className={cx(style.nama,style.inclineline)}>
                                <p>Borrower’s Name</p>
                                <p>{this.state.name.full_name}</p>
                            </div>
                            <div className={cx(style.stts,style.inclineline)}>
                                <p>Status</p>
                                <p>{this.state.status}</p>
                            </div>
                        </div>
                    </ModalPick>
                </div>
            </div>
        )
    }
}


const margin = {
    marginTop: '2em',
    marginBottom: '2em'
}

const post = {
    marginTop: 300,
    marginLeft: 150
}