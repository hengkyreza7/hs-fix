
import React, { Component } from 'react'
import Navbar from '../../../components/Navbar/navbar';
const style = require('./../../../styles/dashboard/dashboard.css');
// var ReactBsTable = require('react-bootstrap-table');
// var BootstrapTable = ReactBsTable.BootstrapTable;
// var TableHeaderColumn = ReactBsTable.TableHeaderColumn;


const products = [];

function addProducts(quantity) {
  const startId = products.length;
  for (let i = 0; i < quantity; i++) {
    const id = startId + i;
    products.push({
      id: id,
      name: 'Item name ' + id,
      price: 2100 + i
    });
  }
}

addProducts(100);
class MainPage extends Component {
  render(){
    // const options = {
    //     // page: 2,  // which page you want to show as default
    //     // sizePerPageList: [ {
    //     //   text: '5', value: 5
    //     // }, {
    //     //   text: '10', value: 10
    //     // }, {
    //     //   text: 'All', value: products.length
    //     // } ], // you can change the dropdown list for size per page
    //     // sizePerPage: 5,  // which size per page you want to locate as default
    //     pageStartIndex: 0, // where to start counting the pages
    //     paginationSize: 3,  // the pagination bar size.
    //     prePage: 'Prev', // Previous page button text
    //     nextPage: 'Next', // Next page button text
    //     firstPage: 'First', // First page button text
    //     lastPage: 'Last', // Last page button text
    //     prePageTitle: 'Go to previous', // Previous page button title
    //     nextPageTitle: 'Go to next', // Next page button title
    //     firstPageTitle: 'Go to first', // First page button title
    //     lastPageTitle: 'Go to Last', // Last page button title
    //     paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
    //     paginationPosition: 'top'  // default is bottom, top and both is all available
    //     // keepSizePerPageState: true //default is false, enable will keep sizePerPage dropdown state(open/clode) when external rerender happened
    //     // hideSizePerPage: true > You can hide the dropdown for sizePerPage
    //     // alwaysShowAllBtns: true // Always show next and previous button
    //     // withFirstAndLast: false > Hide the going to First and Last page button
    //     // hidePageListOnlyOnePage: true > Hide the page list if only one page.
    //   };
    return(
      <div className={style.dashboardPage}>
        <Navbar />
        <div className={style.mainContent}>
            <div className={style.baseBoxLeftUp} >
                <p className={style.txt}>Favorite Books</p>
                <div className={style.favBookCol}>
                    <p className={style.textFavBook}> 1.Pragmatik</p>
                </div>
                <div className={style.favBookCol}>
                     <p className={style.textFavBook}> 2. Alam dan Marga Satwa Amerika Utara</p>
                </div>
                <div className={style.favBookCol}>
                    <p className={style.textFavBook}> 3.Bayi Bayi Bersayap</p>
                </div>
                <div className={style.favBookCol}>
                     <p className={style.textFavBook}> 4. Matematika Dasar 5</p>
                </div>
                <div className={style.favBookCol}>
                     <p className={style.textFavBook}> 5. RPUL</p>
                </div>
            </div>
            <div className={style.baseBoxLeftBottom} >
                <p className={style.txt}>Favorite Activities</p>
                <div className={style.favBookCol}>
                    <p className={style.textFavBook}> 1.Pragmatik</p>
                </div>
                <div className={style.favBookCol}>
                     <p className={style.textFavBook}> 2. Alam dan Marga Satwa Amerika Utara</p>
                </div>
                <div className={style.favBookCol}>
                    <p className={style.textFavBook}> 3.Bayi Bayi Bersayap</p>
                </div>
            </div>
            <div className={style.baseBox}>
                <div className={style.groupVisitor}>
                    <p className={style.totalVisitors}>Total Visitor</p> 
                    <div className={style.visitorCol}>
                        <p className={style.textVisitors}> 230 </p>
                    </div>
                </div>
                <div className={style.groupVisitor}>
                    <p className={style.textVisitorsToday}>Total Visitor</p> 
                    <div className={style.visitorTodayCol}>
                        <p className={style.textVisitors}> 230s </p>
                    </div>
                </div>
                <div className={style.baseTable}>
               {/* <BootstrapTable data={ products } pagination={ true } options={ options }> */}
                    {/* <TableHeaderColumn dataField='id' isKey={ true }>Product ID</TableHeaderColumn>
                    <TableHeaderColumn dataField='name'>Product Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='price'>Product Price</TableHeaderColumn>
                </BootstrapTable> */}
                </div>
        
            </div>
        </div>
      </div>
      
    )
  }
}

export default MainPage;