
import React, { Component } from 'react'
import { NavbarMain } from '../../../components/Navbar/navbar';
import Sidebar from '../../../components/Layout/rightCulumnBookList';
import { Link } from 'react-router-dom';
import { apiLibHost } from './../../../utils/constant';
import axios from 'axios';
import Pagination from './../../../components/pagination/index';
import Spinner from './../../../components/loader/index';
import Search from './../../../styles/assets/src.png'
import { Notfound } from './../../../styles/notFound.js';
import { deweyClass } from './../../../utils/helper';
import Default from './../../../styles/assets/picture.svg'
const style = require('./../../../styles/dashboard/dashboard.css');




class MainPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: {
        data: [],
        inventories: [],
        status_summaries: [],
        select: "",
        text: undefined
      },
      page: 1,
      total_page: 0,
      loading: false,
      pageOfItems: [],
      subject: [
        { value: '000', label: 'Computer science, information & general works' },
        { value: '100', label: 'Philosophy & psychology' },
        { value: '200', label: 'Religion' },
        { value: '300', label: 'Social sciences' },
        { value: '400', label: 'Language' },
        { value: '500', label: 'Science' },
        { value: '600', label: 'Technology' },
        { value: '700', label: 'Arts & recreation' },
        { value: '800', label: "Literature" },
        { value: '900', label: "History & geography" }
      ],
    }
    this.getData = this.getData.bind(this)
    this.generate = this.generate.bind(this)
    this.onChangePage = this.onChangePage.bind(this)
    this.handleDropdown = this.handleDropdown.bind(this)
    this.find = this.find.bind(this)
    this.onChangeForm = this.onChangeForm.bind(this)
    const token = localStorage.getItem("_token")
    this.generateStatus = this.generateStatus.bind(this)
    if (token === null) {
      this.props.history.push('/login')
    }
  }
  componentDidMount() {
    this.getData()

  }
  onChangeForm(e, prop) {
    let dv = this.state.data
    dv[prop] = e.target.value
    this.setState({ data: dv })

  }

  getData() {
    let self = this
    var url = '';

    if (self.state.data.select === "" &&  self.state.data.text === undefined ) {
      url = `${apiLibHost}/inventories?per_page=9`
    }else if(self.state.data.select === 'N/A'){
      url = `${apiLibHost}/inventories?per_page=9`
    } else if (self.state.data.select === undefined) {
      url = `${apiLibHost}/inventories?title=${self.state.data.text}`
    } else if (self.state.data.text === undefined) {
      url = `${apiLibHost}/inventories?ddc=${parseInt(this.state.data.select.value, 10)}`
    }
    else {
      url = `${apiLibHost}/inventories?ddc=${parseInt(this.state.data.select.value, 10)}&title=${self.state.data.text}`
    }
    console.log(url)
    axios.request({
      url: url,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'GET'
    })
      .then(function (response) {
        response.data.data.inventories.map(function(j,k){
         return response.data.data.status_summaries.map(function(i,j){
            return response.data.data.inventories[k].available = response.data.data.status_summaries[j].available
          })
        })
        self.setState({
          data: response.data.data,
          page: response.data.meta.pagination.page,
          total_page: response.data.meta.pagination.total_page,
          loading: true
        })

      })
      .catch(function (error) {
        // refresh()
        console.log("here")
      });
  }

  onChangePage(page) {
    let self = this
    var url = `${apiLibHost}/inventories?per_page=9&page=${page}`
    axios.request({
      url: url,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'GET'
    })
      .then(function (response) {
        response.data.data.inventories.map(function(j,k){
         return response.data.data.status_summaries.map(function(i,j){
            return response.data.data.inventories[k].available = response.data.data.status_summaries[j].available
          })
        })
        self.setState({
          data: response.data.data,
          page: response.data.meta.pagination.page,
          total_page: response.data.meta.pagination.total_page,
          loading: true
        })

      })
      .catch(function (error) {
        // refresh()
        console.log("here")
      });
  }

  generate() {
    const temp = (
      this.state.data.status_summaries.map(function (sts, index) {
        return <h3>{index}</h3>
      })
    )
    return temp;
  }
  handleDropdown(e, prop) {
    let dv = this.state.data
    dv[prop] = e !== null ? e : 'N/A'
    this.setState({ data: dv })
    console.log("awa", this.state.data)
    this.getData()
  }

  find() {
    console.log(this.state.text,"my text")
    this.getData()
  }

  generateStatus(arr) {
    const status = (
      <h3>
        {
          this.state.data.status_summaries.map(function (sts, u) {
            if (sts.inventory_id === arr.id) {
              if (sts.available > 0) {
                return <h3> available</h3>
              } else {
                return <h3>Not available</h3>
              }
            }
            return <div> </div>
          })
        }
      </h3>
    )
    return status;
  }


  render() {
    let content;
    if (this.state.loading && this.state.data.inventories.length !== 0) {
      content = <div className={style.mains}>
        {
          this.state.data.inventories.map(function (arr, i) {
            return <Link key={i} to={"/book-detail/" + arr.id}>
              <div key={arr.i} >
                <img key={arr.i} src={arr.cover_uri === ""?Default:arr.cover_uri} alt="icon" width="60px" height="75px" />
                <p key={arr.i}>{arr.title}</p>
                <h5 key={arr.i}>{arr.author}</h5>
                <h4 key={arr.i}>{deweyClass(arr.dewey_decimal_class)}</h4>
                <h3>{arr.available > 0?'Avalaible':'Not Available'}</h3>
                {/* <h3> {this.generateStatus(arr)}</h3> */}

              </div>
            </Link>
          }, this)
        }

      </div>
    } else if (this.state.loading && this.state.data.inventories.length === 0) {
      content = <Notfound>
        <div>
          <img src={Search} alt="search" />
          <p>No Result Found</p>
          <h5>No results matching with your keyword,try different keyword</h5>
          {/* <h5> </h5> */}
        </div>
      </Notfound>
    } else {
      content = <Spinner />
    }
    // } else {
    //   content = <Spinner />
    // }

    return (
      <div className={style.dashboardPage}>
        <NavbarMain />
        <div className={style.mainContent}>
          <Sidebar data={this.state.data} onSubmit={this.find} onChangeForm={(e) => { this.onChangeForm(e, 'text') }} onChange={(e) => { this.handleDropdown(e, 'select') }} />

          <div className={style.baseBox}>
            <div style={margin}>
              <Pagination
                margin={2}
                page={this.state.page}
                count={Math.ceil(this.state.total_page)}
                onPageChange={this.onChangePage}
              />
            </div>

            {content}
          </div>

        </div>
      </div>

    )
  }
}

export default MainPage;

const margin = {
  marginTop:'2em',
  marginBottom:'2em'
}