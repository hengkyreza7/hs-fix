
import React, { Component } from 'react'
import { NavbarMain } from '../../../components/Navbar/navbar';
import Sidebar from '../../../components/Layout/rightCulumnBookList';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { apiLibHost } from './../../../utils/constant'
import { Button } from 'styled-bootstrap-components';
import Modal from './../../../components/Modal/item';
import Swal from './../../../components/Modal/swal';
import Check from './../../../styles/assets/check.png';
// import Back from './../../styles/assets/b_arrow.svg';
import Default from './../../../styles/assets/picture.svg'
import './../../../styles/global/notification';
import { deweyClass } from './../../../utils/helper'
// import Icon from './../../styles/assets/minion.png';
const style = require('./../../../styles/dashboard/dashboard.css');



class MainPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 0,
            id: this.props.match.params.id,
            barcode: '',
            swaldelete: '',
            data: {
                data: [],
                inventory: {
                    title: '',
                    summary: ''
                },
                select:"",
                subject: [
                    { value: '000', label: 'Computer science, information & general works' },
                    { value: '100', label: 'Philosophy & psychology' },
                    { value: '200', label: 'Religion' },
                    { value: '300', label: 'Social sciences' },
                    { value: '400', label: 'Language' },
                    { value: '500', label: 'Science' },
                    { value: '600', label: 'Technology' },
                    { value: '700', label: 'Arts & recreation' },
                    { value: '800', label: "Literature" },
                    { value: '900', label: "History & geography" }
                ]
            },
            swalInven:"",
            items: {
                inventory_items: []
            },
            array:{
                id:""
            },
            chars_left: 130,
            isOpen: false,
            swal:false
        }
        this.getData = this.getData.bind(this)
        this.getDataItem = this.getDataItem.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
        this.onChange = this.onChange.bind(this)
        this.handleBtnReady = this.handleBtnReady.bind(this)
        this.deleteItem = this.deleteItem.bind(this)
        this.deleteInventory = this.deleteInventory.bind(this)
        this.getDataEdit = this.getDataEdit.bind(this)
    }

    componentDidMount() {
        this.getData()
        this.getDataItem()
    }

    toggleModal() {
        this.setState({
            isOpen: !this.state.isOpen
        });

    }
    onChange(event) {
        this.setState({
            barcode: event.target.value
        })
    }
    getDataEdit() {
        this.props.history.push({
            pathname:'/edit-item/'+this.state.id
        })
   
    }

    deleteInventory(e,id){
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.changePage()
           
        }
        axios({
            url: `${apiLibHost}/inventories/${id}`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'DELETE',
        })
            .then(function (response) {
                refresh()

            })
            .catch(function (error) {
                
            });
    }

    deleteItem(id,e){
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.refreshDelete()
            this.getDataItem()
        }
        axios({
            url: `${apiLibHost}/inventory-items/${id}`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'DELETE',
        })
            .then(function (response) {
                refresh()
                

            })
            .catch(function (error) {
                
            });
    }

    getData() {
        let self = this
        axios.request({
            url: `${apiLibHost}/inventories/${this.state.id}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                self.setState({
                    data: response.data.data,
                })
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }

    getDataItem() {
        let self = this
        axios.request({
            url: `${apiLibHost}/inventory-items?inventory_id=${this.state.id}&count=0&limit=5`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                self.setState({
                    items: response.data.data,
                })
                console.log("vl", response.data.data)

            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }
    
    sendUrl() {
        setTimeout(() => {
            this.setState({
                swal: true
            })
        }, 500)

        setTimeout(() => {
            this.setState({
                swal: false
            })
        }, 2000)


    }
    refreshDelete() {
        setTimeout(() => {
            this.setState({
                swaldelete: true
            })
        }, 500)

        setTimeout(() => {
            this.setState({
                swaldelete: false
            })
        }, 2000)

    }

    changePage(){
        setTimeout(() => {
            this.setState({
                swalInven: true
            })
        }, 500)

        setTimeout(() => {
            this.props.history.push({
                pathname: '/book-list'
            })
        }, 2000)
    }

    handleBtnReady(e) {
        e.preventDefault()
        let self = this
        const refresh = () => {
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.sendUrl()
            this.getDataItem()
        }
        axios({
            url: `${apiLibHost}/inventory-items`,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            data: {
                "inventory_id": self.state.id,
                "barcode": self.state.barcode,
                "status": "OK"
            }
        })
            .then(function (response) {
                refresh()


            })
            .catch(function (error) {
                
            });
    }
    




    render() {
        const color = {
            backgroundColor: '#edf5ff'
        }

        const action = {
            marginRight: '5px',
            fontSize : '20px'
        }

        const form = {
            width: '420px',
        }

        const label = {
            marginTop: '-0px',
            marginLeft: '50px'
        }

        console.log(this.state.data.inventory)

        return (
            <div className={style.dashboardPage}>
                <NavbarMain />
                <div className={style.mainContent}>
                    <Sidebar  data={this.state.data}/>
                    <div className={style.baseBox}>
                        <Link to="/book-list"><p className={style.back}> <span icon="pe-7s-login">  </span> BACK</p></Link>
                        <div className={style.bookLogoLocation}>
                            <img src={this.state.data.inventory.cover_uri === ""?Default:this.state.data.inventory.cover_uri} alt="book" width="149" height="189" />
                            {/* <img src={Icon} alt="book" width="149" height="189" /> */}
                        </div>
                        <div>
                            <p className={style.titleBook}>{this.state.data.inventory.title}</p>
                            <p className={style.infoBook}>{this.state.data.inventory.author}<br />
                                {deweyClass(this.state.data.inventory.dewey_decimal_class)} <br />
                                ISBN {this.state.data.inventory.isbn} <br />
                            </p>
                            <p className={style.descBook}>{this.state.data.inventory.summary}.</p>
                        </div>
                        <div className={style.action}>
                              <Button style={action} onClick={(e) => {this.getDataEdit(e,this.state.id)}} white>Edit</Button>  
                              <Button style={action} onClick={(e) => {this.deleteInventory(e,this.state.id)}}  white>Delete</Button>
                            {/* <Link to="#" style={action} >Edit</Link> */}
                          
                        </div>
                        <div >
                            <p className={style.itemText}>Item Activity</p>
                        </div>
                        <div className={style.btnPost}>
                            <Button onClick={this.toggleModal} primary>Add Item</Button>
                        </div>
                        <div className={style.baseBoxTable} >
                            <table className="table table-bordered">
                                <thead className="thead-dark" style={color}>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Barcode</th>
                                        <th scope="col">Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.items.inventory_items.map(function (array, j) {
                                            return <tr key={j}>
                                                <td>{j + 1}</td>
                                                <td>{array.barcode}</td>    
                                                <td>{array.status}</td>
                                                <td> <Button value={array.id} onClick={(e) => {this.deleteItem(array.id,e)}} danger>Delete</Button></td>
                                            </tr>
                                        },this)
                                    }
                                </tbody>
                            </table>
                        </div>
                        <Modal show={this.state.isOpen} onClose={this.toggleModal} onReady={this.handleBtnReady} >
                            <div>
                                <form>
                                    <div className={style.formGroup} style={label}>
                                        <label className={style.label}>BARCODE</label>
                                        <input type="text" className="form-control" style={form} onChange={this.onChange} value={this.state.barcode} />
                                    </div>
                                </form>

                            </div>
                        </Modal>
                        <div>
                            <Swal show={this.state.swal} >
                                <div>
                                    <img src={Check} alt="check" className={style.Path} />
                                         <p className={style.swal}>Item has been added .</p>
                                </div>
                            </Swal>
                            <Swal show={this.state.swaldelete} >
                                <div>
                                    <img src={Check} alt="check" className={style.Path} />
                                         <p className={style.swal}>Item has been delete .</p>
                                </div>
                            </Swal>
                            <Swal show={this.state.swalInven} >
                                <div>
                                    <img src={Check} alt="check" className={style.Path} />
                                         <p className={style.swal}>Inventory has been delete .</p>
                                </div>
                            </Swal>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default MainPage;