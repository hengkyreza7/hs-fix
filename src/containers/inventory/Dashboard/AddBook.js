import React, { Component } from 'react';
import Swal from './../../../components/Modal/swal';
import axios from 'axios';
import { apiLibHost } from './../../../utils/constant';
import Check from './../../../styles/assets/check.png';
import { NavbarMain } from '../../../components/Navbar/navbar';
import Select from 'react-select';
// import Dropzone from 'react-dropzone';
// import { Link } from 'react-router-dom';

// import DatePicker from 'react-date-picker';

const style = require('./../../../styles/profile/profile.css');

class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false,
            swal: false,
            files: [],
            id: this.props.match.params.id,
            subject: [
                { value: '000', label: 'Computer science, information & general works' },
                { value: '100', label: 'Philosophy & psychology' },
                { value: '200', label: 'Religion' },
                { value: '300', label: 'Social sciences' },
                { value: '400', label: 'Language' },
                { value: '500', label: 'Science' },
                { value: '600', label: 'Technology' },
                { value: '700', label: 'Arts & recreation' },
                { value: '800', label: "Literature" },
                { value: '900', label: "History & geography" }
            ],
            data: {
                title: '',
                author: "",
                publisher: "",
                isbn: "",
                series_title: "",
                summary: "",
                dewey: "",
                dewey_decimal_class:"",
                inventory: {
                    title: '',
                    summary: '',
                    author: "",
                    publisher: "",
                    isbn: "",
                    series_title: "",
                    dewey: "",
                    dewey_decimal_class:""
                },

            }
          
        }
        this.onChange = this.onChange.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
        this.handleBtnReady = this.handleBtnReady.bind(this)
        this.onDrop = this.onDrop.bind(this)
        this.handleDropdown = this.handleDropdown.bind(this)
        this.getData = this.getData.bind(this)

        const token = localStorage.getItem("_token")
        if(token === null){
        this.props.history.push('/login')
        }
    }
    componentDidMount() {
        //did mount
        if(this.state.id !== undefined ){
            this.getData()
        }
        console.log("hre", this.props.data)
        
    }

    onChange(e, prop) {
        // e.preventDefault()
        if(this.state.id === undefined) {
            console.log("here ")
            let dv = this.state.data
            dv[prop] = e.target.value
            this.setState({ data: dv })
        }else{
            console.log("here else")
            let dv = this.state.data
            dv[prop] = e.target.value
            this.setState({ data: dv })
        }


    }
    handleDropdown(e, prop) {
        if(this.state.id === undefined) {
            let dv = this.state.data
            dv[prop] = e !== null ? e : 0
            this.setState({ data: dv })
        }else{
            console.log(e,"here")
         let dv = this.state.data
            dv[prop] = e.value !== null ? e.value : 0
            this.setState({ data: dv })
            console.log(this.state.data)
        }
       
    }
    toggleModal() {
        this.setState({
            isOpen: !this.state.isOpen
        });

    }

    getData() {
        let self = this
        axios.request({
            url: `${apiLibHost}/inventories/${this.state.id}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'GET'
        })
            .then(function (response) {
                console.log(response.data.data)
                self.setState({
                    data: response.data.data.inventory,
                })
            })
            .catch(function (error) {
                // refresh()
                console.log("here")
            });
    }


    onDrop(files) {
        this.setState({
            files
        });
    }

    sendUrl() {
        setTimeout(() => {
            this.setState({
                swal: true
            })
        }, 500)

        setTimeout(() => {
            this.props.history.push({
                pathname: '/book-list'
            })
        }, 2000)


    }
    handleBtnReady(e) {
        e.preventDefault()
        let self = this
        const refresh = () => {
            console.log("a")
            setTimeout(() => {
                self.setState({ isOpen: false })
            }, 200)
            this.sendUrl()
        }

        if(this.state.id === undefined){
            axios.request({
                url: `${apiLibHost}/inventories`,
                headers: { 'Content-Type': 'application/json' },
                method: 'POST',
                data: {
                    "title": this.state.data.title,
                    "author": this.state.data.author,
                    "publisher": this.state.data.publisher,
                    "isbn": this.state.data.isbn,
                    "series_title": this.state.data.series_title,
                    "summary": this.state.data.summary,
                    "dewey_decimal_class": this.state.data.dewey.value
    
                }
            })
                .then(function (response) {
                    refresh()
    
                })
                .catch(function (error) {
                    // refresh()
    
                    // this.setState({
                    //     isOpen: !this.state.isOpen
                    //   });
                });
        }else{
            axios.request({
                url: `${apiLibHost}/inventories/${this.state.id}`,
                headers: { 'Content-Type': 'application/json' },
                method: 'PATCH',
                data: {
                    "title": this.state.data.title,
                    "author": this.state.data.author,
                    "publisher": this.state.data.publisher,
                    "isbn": this.state.data.isbn,
                    "series_title": this.state.data.series_title,
                    "summary": this.state.data.summary,
                    "dewey_decimal_class": this.state.data.dewey_decimal_class
    
                }
            })
                .then(function (response) {
                    refresh()
    
                })
                .catch(function (error) {
                    // refresh()
    
                    // this.setState({
                    //     isOpen: !this.state.isOpen
                    //   });
                });
        }
        
    }

 
    render() {
        console.log(this.state.data,"hit me")
        return (
            <div className={style.profilePage} >
                <NavbarMain />
                <div className={style.mainContent}>

                    <div className={style.boxAddBook}>
                        <div>
                            <form >
                                <div className={style.formGroup}>
                                    <label className={style.label}>SUBJECT</label>
                                    <div className={style.form}>
                                        <input type="text" className="form-control" onChange={(e) =>{this.onChange(e,'title')}} value={this.state.id === undefined?this.state.data.title:this.state.data.title} />
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label}>AUTHOR</label>
                                    <div className={style.form}>
                                        <input type="text" className="form-control" onChange={(e) => this.onChange(e,'author')} value={this.state.id === undefined?this.state.data.author:this.state.data.author} />
                                    </div>
                                </div>
                                <div className="col-sm-12 center">
                                    <label className={style.labelDate}>Category</label>
                                    <div className="input-group date col-sm-12">
                                        <Select 
                                            className={style.selectCategory} 
                                            value={this.state.id === undefined?this.state.data.dewey:this.state.subject.filter(key => key.value === this.state.data.dewey_decimal_class)} 
                                            onChange={this.state.id === undefined?(e) => this.handleDropdown(e,'dewey'):(e) => this.handleDropdown(e,'dewey_decimal_class')} 
                                            options={this.state.subject} 
                                        />
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label} >Description</label>
                                    <div className={style.form}>
                                        <input type="text" className="form-control" onChange={(e) => this.onChange(e,'summary')} value={this.state.id === undefined?this.state.data.summary:this.state.data.summary} />
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label} >Publisher</label>
                                    <div className={style.form}>
                                        <input type="text" className="form-control" value={this.state.id === undefined?this.state.data.publisher:this.state.data.publisher} onChange={(e) => this.onChange(e,'publisher')} />
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label}>Item's ID /ISBN</label>
                                    <div className={style.form}>
                                        <input type="text" className="form-control" onChange={(e) => this.onChange(e,'isbn')} value={this.state.id === undefined?this.state.data.isbn:this.state.data.isbn} />
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label}>Series Title</label>
                                    <div className={style.form}>
                                        <input type="text" className="form-control" onChange={(e) => this.onChange(e,'series_title')} value={this.state.id === undefined?this.state.data.series_title:this.state.data.series_title} />
                                    </div>
                                </div>
                                {/* <div className={style.formGroup}>
                                    <label className={style.label}>Attach Item Cover</label>
                                    <div className={style.form}>
                                        <section className={style.customAttach}>
                                            <div className="dropzone">
                                                <Dropzone onDrop={this.onDrop.bind(this)} className={style.customAttach}>
                                                    <p className={style.dropText}>Drag or upload Item's cover </p>
                                                </Dropzone>
                                            </div>
                                            <aside>
                                                <ul>
                                                    {
                                                        this.state.files.map(f => <li key={f.name}>{f.name} - {f.size} bytes</li>)
                                                    }
                                                </ul>
                                            </aside>
                                        </section>
                                    </div>
                                </div> */}

                            </form>
                            <div className={style.footer}>
                                <button type="submit" className={style.btn} onClick={(e) => { this.handleBtnReady(e) }} >Save</button>
                            </div>
                            <div>
                                <Swal show={this.state.swal} >
                                    <div>
                                        <img src={Check} alt="check" className={style.Path} />
                                        <p className={style.swal}>New item has been added.</p>
                                    </div>
                                </Swal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Profile;