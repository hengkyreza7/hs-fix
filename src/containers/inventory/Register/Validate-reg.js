import React, { Component } from 'react'
import './../../../styles/scss/register.css';
import logo from './../../../styles/assets/HighScope.svg';
import PP from './../../../styles/assets/user.svg';
import Modal from './../../../components/Modal/modal';
import axios from 'axios';
import { apiHost } from './../../../utils/constant';

const style = require("./../../../styles/scss/register.css")



class Register extends Component {
    constructor(props){
        super(props)
        this.state={
            photoProfile: "",
            open:false,
            name:"",
            subject: [],
            userid:"",
            isOpen: false,
            data:this.props.location.state.detail,
            roles:[]
            
        }
        this.openModal = this.openModal.bind(this)
        this.handleBtnGoback = this.handleBtnGoback.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
        this.handleBtnReady = this.handleBtnReady.bind(this)
        this.getData = this.getData.bind(this)
     }
    
    componentDidMount(){
        this.getData()
        console.log(this.props.location.state.detail.roles,"lalalala")
    }

    openModal(){
        this.setState({
            open:true
        })
    }
    
    handleBtnGoback(){
        this.props.history.push('/')
    }
    toggleModal() {
        this.setState({
          isOpen: !this.state.isOpen
        });
    }

    handleBtnReady() {
        this.props.history.push({
            pathname: '/update-profile',
            state : {data : this.state.data, roles: this.state.roles, type:this.props.location.state.type }

        })
    }

    getData() {
        let self = this
        const model = []
        for(var i in self.state.data.roles){
            const mdl = self.state.data.roles[i]
            model.push(mdl.model_id)
        }
        console.log(model,"here")
        console.log(this.props.location.state.type,"model")

        var url = ''
        if(this.props.location.state.type === "librarians"){
            url = `${apiHost}/librarians/${this.props.location.state.detail}`
        }else{
            url = `${apiHost}/finances/${this.props.location.state.detail}`
        }

        axios.request({
            url: url,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            method: 'GET'
        })
            .then(function (response) {
                self.setState({
                    data : response.data.data,
                    roles:response.data.data.roles
                })
                console.log(response.data.data)

            })
            .catch(function (error) {
               console.log("here")
            });
    }


    render() {
        const arr = []
        for(var i in this.state.subjects){
            const a = this.state.subjects[i]
            arr.push(a.name)
        }
        return (
            <div>
                <div className={style.registerPage}> 
                <div className={style.rectangleCol}>
                    <div className={style.WelcometoHighScope}>
                       Welcome to HighScope Indonesia 
                    </div>
                    <div className={style.logo}>
                        <img src={logo} alt="logo" width="200px" />
                       

                    </div>
                    <div className={style.photoProfile}>
                        <img src={PP} alt="logo" width="100px" />
                    </div>
                        <div >
                            <p className={style.nameText}>Name  </p>
                            <p className={style.nameValueText}>{this.state.data.full_name}</p>
                            <p className={style.subjectText}>User ID</p>
                            <p className={style.subjectValueText}>{this.state.data.registration_number}</p>
                            {/* <p className={style.userIdText}>User ID</p>
                            <p className={style.userIdValueText}>{this.state.data.registration_number}</p> */}
                            <p className={style.makeSureText}>If this is you, you may edit your profile</p>
                        <div>
                            <input type="submit" className={style.btnBack} onClick={this.handleBtnGoback} value="Go Back" />
                            <input type="submit" className={style.btnUpdate}  onClick={this.toggleModal} value="Edit Profile" /> 
                        </div>
                    </div>
                </div>
            </div>
            <div>
              </div>    
              <Modal show={this.state.isOpen} onReady={this.handleBtnReady}  onClose={this.toggleModal}>
                <div>
                   <h3 className={style.MessageBox}> Are you ready to edit your profile?</h3>
                   <p className={style.MessageBox2}>If you have doubts, please Check Again.</p>
                </div>
              </Modal>
        </div>
        )
    }
}


export default Register