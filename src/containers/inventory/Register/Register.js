import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import logo from './../../../styles/assets/HighScope.svg';
import axios from 'axios';
import { apiHost } from './../../../utils/constant';
import { Grid, GridItem } from 'styled-grid-component';
import { LeftCol, RightCol } from './../../../styles/register';
import NotificationSystem from 'react-notification-system';
import InputMask from 'react-input-mask';
import Seo from './../../../components/Seo/Seo';


const style = require('./../../../styles/scss/register.css');
const notification = require('./../../../styles/global/notification');
const title = 'Welcome to HighScope Indonesia'


class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userid: "",
            dob: "",
            data: "",
            model_type: "",
            model_id: 0,
            title:'register',
            _notificationSystem: null,
        }
        this.onChangeForm = this.onChangeForm.bind(this);
        this.showNotification = this.showNotification.bind(this)
        this.handleChange = this.handleChange.bind(this);
    }

    sendUrl() {
        this.props.history.push({
            pathname: '/register-validate',
            state: { detail: this.state.model_id, type: this.state.model_type }
        })
    }
    showNotification(color, msg) {
        this.setState({ _notificationSystem: this.refs.notificationSystem });
        var _notificationSystem = this.refs.notificationSystem;
        var level, ikon;
        switch (color) {
            case 1:
                level = 'success';
                ikon = 'pe-7s-check';
                break;
            case 2:
                level = 'warning';
                ikon = 'pe-7s-attention';
                break;
            case 3:
                level = 'error';
                ikon = 'pe-7s-close-circle';
                break;
            case 4:
                level = 'info';
                ikon = 'pe-7s-info';
                break;
            default:
                break;
        }
        _notificationSystem.addNotification({
            title: (<span data-notify="icon" className={ikon}></span>),
            message: (
                <div>
                    {msg}
                </div>
            ),
            level: level,
            position: "tr",
            autoDismiss: 2,
        });
    }

    crateAction(e) {
        e.preventDefault()
        let self = this
        const refresh = () => {
            self.sendUrl()
        }

        const errors = (color, msg) => {
            self.showNotification(color, msg)
        }

        axios.request({
            url: `${apiHost}/validate`,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            method: 'POST',
            data: {
                "registration_number": self.state.userid,
                "dob": self.state.dob
            }
        })
            .then(function (response) {
                self.setState({
                    data: response.data.data
                })
                console.log(response.data.data, "this response")
                var type = []
                for (let i in response.data.data.roles) {
                    let collect = response.data.data.roles[i]
                    type.push(collect)
                }
                console.log(type[0].user_id)
                self.setState({
                    model_id: type[0].model_id,
                    model_type: type[0].model_type
                })
                refresh()
            })
            .catch(function (error) {
                let err = []
                console.log(error.response === undefined?'NA':'')
                let validate = error.response === undefined?'NA':''
                if(validate !=='NA'){
                    for (let i in Object.assign({}, error.response.data.error)) {
                        let data = Object.assign({}, error.response.data.error[i])
                        err.push(data)
                    }
                    var code = error.response.data.error.http_code
                    if (code === 422) {
                        errors(3, 'User id or date of birth incorrect')
                    }else if(code === 400 ){
                        errors(3, 'User has been validate')
                    } else {
                        errors(3, 'User id not valid')
                    }
                }else{
                    errors(3, 'User has been registered')  
                }
            

             
            });
    }



    onChangeForm(event) {
        this.setState({
            userid: event.target.value,
        })
    }

    handleChange(event) {
        this.setState({
            dob: event.target.value
        });


    }

    render() {
        const { userid, dob } = this.state
        console.log(this.state.dob)
        return (
            <Grid
                width="100%"
                height="100vh"
                templateColumns="repeat(2, 1fr)"
                autoRows="minmax(100px, auto)"
            >
                <Seo title={this.state.title} />
                <GridItem column="1">
                    <LeftCol >
                        <div>
                            {title}
                        </div>

                    </LeftCol>
                </GridItem>
                <GridItem column="2">
                    <RightCol>
                        <img src={logo} alt="logo" width="204px" height="100px" />
                        <form>
                            <div>
                                <input type="text" value={userid} onChange={this.onChangeForm}  className={style.input}  required="required" />
                                <span className={style.highlight}></span><p></p>
                                <label>User Id</label>
                            </div>
                            <div>
                                <InputMask mask="9999/99/99" placeholder="YYYY/MM/DD" value={dob}  onChange={this.handleChange} className={style.input} />
                                <span className={style.highlight}></span><p></p>
                                <label>Date of Birth</label>
                            </div>
                            <footer>
                                <button onClick={(e) => { this.crateAction(e) }}>Register </button>
                                <span>Have a Account?<Link to="/login"> Login</Link></span>
                            </footer>
                        </form>
                        <NotificationSystem ref="notificationSystem" style={notification.style} />
                    </RightCol>
                </GridItem>
            </Grid>

        )
    }
}


export default Register



