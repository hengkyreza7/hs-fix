import  React, { Component } from 'react';
import Logo from './../../styles/assets/highscope.png';
import PP from './../../styles/assets/minion.png';
import './../../styles/scss/antd.css';
const style = require('./../../styles/Announcement/Announcement.css');

class Announcement extends Component {
    constructor(props){
        super(props)
        this.state = {
            name: "",
            isOpen:false
        }
        this.onChange = this.onChange.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
    }

    componentDidMount(){

    }
    
    onChange(time, timeString) {
        console.log(time, timeString);
    }
    toggleModal() {
        this.setState({
          isOpen: !this.state.isOpen
        });
    }


    render(){
        const { name } = this.state;
        console.log(name)
        return(
            <div className={style.announPage}>
                <div className={style.navbar}>
                    <img src={Logo} alt="logo" width="180px" height="34px" />
                    <div>
                       <img src={PP} alt="logo" width="34px" height="34px" className={style.miniPhoto} />
                    </div>
                    <div className={style.name}>   
                        Milea Hasan
                    </div>
                    
                    <div className={style.menu}>
                        <div className={style.menuText}>
                               <p className={style.lineBorder}>School Announcement</p>
                        </div>
                    </div>
                </div>
                <div className={style.mainContent}>
                    <div className={style.baseBox}>
                        <form>
                                <div className={style.formGroup}>
                                    <label className={style.label}>SUBJECT</label>
                                    <div className={style.form}>
                                      <input type="text" className="form-control " />
                                    </div>
                                </div>
                                <div className={style.formGroupDate} >
                                <div className="col-sm-6 left"> 
                                    <label className={style.labelDate}>DATE</label>
                                        <div className="input-group date col-sm-12">
                                            <input type='text' className="form-control" />
                                            <span className="input-group-addon">
                                                <span className="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 right"> 
                                    <label className={style.labelDate}>TIME</label>
                                        <div  className="input-group date col-sm-7">
                                        <input type='text' className="form-control" />
                                            <span className="input-group-addon">
                                                <span className="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className={style.formGroup}>
                                    <label className={style.label}>Messages</label>
                                    <div className={style.form}>
                                      <textarea className="form-control" />
                                    </div>
                                </div>
                            </form>  
                            <button type="submit" className={style.btn} onClick={this.toggleModal} >Publish</button>
                    </div>
                </div>
                    <div>
                    
                    </div>
            </div>
        )
    }

}
export default Announcement;