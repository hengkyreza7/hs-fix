import {
 Register,
 Validate,
 Login, 
 Profile,
 Announcement,
 BookList,
 AddBook,
 BookDetail,
 Dashboard,
 InventoryManagement,
 NotFound,
 Refund,
 Registration,
 CreatePayment,
 DetailPayment,
 CreateRegistration,
 DetailRegistration,
 DetailRefund,
 DetailGroup,
 PaymentAll,
 Transaction,
 Admission,
 AdmissionNew,
 AdmissionApplicant,
 AdmissionDetail,
} from '../containers';
import {
  Navbar
} from '../components'

export const MainRoutes = [
  {
    path: '/',
    exact: true,
    component: Register,
  },
  {
    path: '/navbar',
    component: Navbar
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/register-validate',
    component: Validate
  },
  {
    path: '/update-profile',
    component: Profile
  },
  {
    path : '/create-announcement',
    component : Announcement
  },
  {
    path : '/dashboard',
    name: "Login",
    component : Dashboard
  },
  {
    path : '/book-list',
    component : BookList
  },
  {
    path : '/book-detail/:id',
    component : BookDetail
  },
  {
    path : '/inventory-management',
    component : InventoryManagement
  },
  {
    path : '/add-item',
    component : AddBook
  },
  {
    path : '/edit-item/:id',
    component : AddBook
  },
  {
    path : '/payment-transaction/:id',
    component : Transaction
  },
  {
    path : '/refund-transaction',
    component : Refund
  },
  {
    path : '/registration',
    component : Registration
  },
  {
    path : '/create-payment',
    component : CreatePayment
  },
  {
    path : '/detail-payment',
    component : DetailPayment
  },
  {
    path : '/detail-payment/:id',
    component : DetailPayment
  },
  {
    path : '/detail-registration',
    component : DetailRegistration
  },
  {
    path : '/create-registration',
    component : CreateRegistration
  },
  {
    path : '/detail-refund/:id',
    component : DetailRefund
  },
  {
    path : '/detail-group-payment/:id',
    component : DetailGroup
  },
  {
    path : '/detail-group-payment-all/:id',
    component : PaymentAll
  },
  {
    path : '/admission',
    component : Admission
  },
  {
    path : '/create-admission',
    component : AdmissionNew
  },
  {
    path : '/applicant-admission',
    component : AdmissionApplicant
  },
  {
    path : '/detail-admission',
    component : AdmissionDetail
  },
  {
    component: NotFound
  }
];