// call from webpack
const apiHost =  process.env.API_URL;
const apiLibHost= process.env.API_URL_LIB; 
const apiPaymentHost = process.env.API_URL_PAYMENT;

export {
    apiHost,
    apiPaymentHost,
    apiLibHost
}
