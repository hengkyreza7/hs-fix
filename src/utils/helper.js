//HELPER GLOBAL
import axios from 'axios'

//FUNCTION FOR CONVERT INT/NUMUERIC TO MONEY WITH FORMAT IDR
//INPUT 200000
//OUTPUT Rp. 200,000
export function formatPrice(price) {
  return new Intl.NumberFormat("ID", {
    style: "currency",
    currencyDisplay: "symbol",
    currency: "IDR"
  }).format(price);
}

// FUNCTION FOR CONVERT KEY TO LABEL 
// INPUT AP_EXAMP
// OUTPUT AP EXAMP
export function changeValue(key) {
  let label = ''
  if (key === 'ap_exam') {
    label = 'AP Exam'
  } else if (key === 'toefl') {
    label = 'TOEFL'
  } else if (key === 'assessment') {
    label = 'P&C Assessment'
  } else if (key === 'holiday_program') {
    label = 'Holiday Program'
  } else if (key === 'esses') {
    label = 'ESSES'
  } else if (key === 'late_pickup') {
    label = 'Late Pickup'
  } else if (key === 'metalevel') {
    label = 'Metalevel'
  } else if (key === 'book_pinalty') {
    label = 'Denda Buku'
  } else if (key === 'id_card') {
    label = 'Id Card'
  } else if (key === 'yearbook') {
    label = 'Yearbook'
  } else if (key === 'deposit') {
    label = 'Deposit'
  } else if (key === 'summer_program') {
    label = 'Summer Program'
  } else if (key === 'amp') {
    label = 'Amp'
  } else {
    label = key
  }

  return label
}

// FUNCTION CONVERT DATE
// INPUT = ALL FORMAT DATE
// OUTPU = DD/MM/YYYY
export function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [day, month, year].join('/');
}

//FUNCTION CONVERT DEWEY DECIMAL CLASSES
//INPUT= 000 (DEWEY KODE)
//OUTPUT = COMPUTER SCIENCE, INFORMATION & TECHNOLOGI (DEWEY VALUE/LABEL)
export function deweyClass(kode) {
  let label;

  if (kode === '000') {
    label = "Computer science, information & general works"
  } else if (kode === '100') {
    label = "Philosophy & psychology"
  } else if (kode === '200') {
    label = "Religion"
  } else if (kode === '300') {
    label = " Social sciences"
  } else if (kode === '400') {
    label = "Language"
  } else if (kode === '500') {
    label = " Science"
  } else if (kode === '600') {
    label = "Technology"
  } else if (kode === '700') {
    label = "Arts & recreation"
  } else if (kode === '800') {
    label = "Literature "
  } else if (kode === '900') {
    label = " History & geography"
  }

  return label;
}


//FUNCTION GET DATA FULL NAME BY USER ID
//INPUT = 02
//OUTPUT = CHURIAH A
export function getFullName(api,kode) {
  
  let self = this;
  let url = `${api}/users/${kode}`
  var name = []
  axios.request({
    url: url,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    method: 'GET'
})
    .then(function (response) {
        name = response.data.data
        self.setState({
            data: response.data.data
        },this)
        // console.log(this.state.data,"hew")
    })
    .catch(function (error) {
        // refresh()
        console.log("here")
    });

 
    console.log(name,"myname")

    return name
}