import styled from "styled-components";

const defaultFont = 'Nunito Sans';
const whiteColor = '#ffffff';

const Navbar = styled.div`
    width: auto !important;
    height: 105px;
    background-color: ${whiteColor};
    
   
    div {
        img {
            margin-left: 65%;
            margin-top: -4%;
            position: absolute;
            width: 34px;
            height: 34px;
            border: solid 0.55px #d6d6d6;
            border-radius: 50%;
        }
    }
    p {
        margin-left: 40%;
        margin-top: -4%;
        margin-bottom: 11px;
        width: 192px;
        height: 26px;
        font-family: ${defaultFont};
        font-size: 19px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: -0.3px;
        color: #055291;
    }

    h5 {
        margin-left:83%;
        margin-top: -2.5%;
        position: absolute;
        width:200px;
        height: 24px;
        font-family:  ${defaultFont};
        font-size: 20px;
        font-weight: 500;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #5b5b5e;
        background-color: #ffffff;
    }
    div {
        margin-left: 15%;
        margin-top:0.9%;
        input {
            margin-top: -2.4%;
            margin-left: 81%;
            position: absolute;
            width:30px;
        }
    }


`

export {
    Navbar
}