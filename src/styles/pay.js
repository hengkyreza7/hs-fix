import styled from "styled-components";

const defaultFont = 'Nunito Sans';
// const whiteColor = '#ffffff';


const Title = styled.div`
    margin-left :5%;
    margin-top : 4%;
    width: 571px;
    height: 34px;
    font-family: ${defaultFont};
    font-size: 25px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: -0.7px;
    color: #233539;
`
const Flex = styled.div`
    div {
        border : none;
        label {
            height: 19px;
            font-family: 'Nunito Sans';
            font-size: 13px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: '#333333';
            text-align: left;
        }
        p {
            margin-left : 19%;
            height: 18px;
            font-family: 'Nunito Sans';
            font-size: 13px;
            font-weight: normal;
            font-style: normal;
            font-stretch: 34px;
            line-height: normal;
            letter-spacing: normal;
            color: #333333;
        }
    }
`



export {
    Flex,
    Title
}


