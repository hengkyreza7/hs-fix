import styled from "styled-components";


const defaultFont = 'Nunito Sans';
// const whiteColor = '#ffffff';
const grey = '#88889c'

const Notfound = styled.div`
    margin-top : 5%;
    margin-left : 35%;

    img {
        width : 150px
    }
    p {
        text-align : left;
        font-family : ${defaultFont};
        color : ${grey};
        font-stretch: normal;
        font-size : 30px;
        font-weight: 900;
        margin-left :-5%;

    }
    h5 {
        text-align : left;
        font-family : ${defaultFont};
        color : ${grey};
        font-stretch: normal;
        font-size : 20px;
        font-weight: 100;
        margin-left :-30%;
    }

`   

export {
    Notfound
}

