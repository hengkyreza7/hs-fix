import styled from "styled-components";

const defaultFont = 'Nunito Sans';
const whiteColor = '#ffffff';


const LeftCol = styled.div`
    background-image: linear-gradient(42deg, #4086e8,#155de2);
    width : 98%;
    height: 100%;
        div {
            width: 430px;
            margin-top: 25%;
            margin-left: 8%;
            margin-right: 20%;
            margin-bottom: 268px;
            position: absolute;
            font-family: ${defaultFont};
            font-size: 44px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            text-align: center;
            color: ${whiteColor};
        }
            
}`

const RightCol = styled.div`
    background-image: ${whiteColor};
    width : 100%;
    height: 100%;
        
        img {
            margin-left: 10%;
            margin-top: 20%;
        }
        
        form {
            margin-top: 7%;
            
            div {
                position: relative;
                margin: 45px 0;

                ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
                    color: red;
                    opacity: 1; /* Firefox */
                }

                input,
                textarea {
                    margin-left: 10%;
                    background: none;
                    color: #5c687c;
                    font-size: 18px;
                    padding: 10px 10px 10px 5px;
                    display: block;
                    width: 360px;
                    border: none;
                    border-radius: 0;
                    border-bottom: 1px solid #c6c6c6;
                }
               
                input:focus,
                    textarea:focus {
                         outline: none;
                    }
                    input:focus ~ label, input:valid ~ label,
                    textarea:focus ~ label,
                    textarea:valid ~ label {
                        top: -14px;
                        font-size: 12px;
                        color: #bdbdbd;
                    }
                    input:focus ~ p:before,
                    textarea:focus ~ p:before {
                    width: 372px;
                }
    
                input[type="password"] {
                    letter-spacing: 0.3em;
                }
    
                input[type="date"]{
                    color: none
                    
                }
                ::-webkit-datetime-edit-year-field:not([aria-valuenow]),
                ::-webkit-datetime-edit-month-field:not([aria-valuenow]),
                ::-webkit-datetime-edit-day-field:not([aria-valuenow]) {
                    color: transparent;
                }
                label {
                    color: #c6c6c6;
                    font-size: 16px;
                    font-weight: normal;
                    position: absolute;
                    pointer-events: none;
                    left: 10%;
                    top: 10px;
                    -webkit-transition: 300ms ease all;
                    transition: 360ms ease all;
                }
                
                p {
                    position: relative;
                    display: block;
                    width: 360px;
                    left: 6.5%;
                }
                p:before {
                    content: '';
                    height: 2px;
                    width: 0;
                    bottom: 0px;
                    position: absolute;
                    background: #2196F3;
                    -webkit-transition: 300ms ease all;
                    transition: 300ms ease all;
                    left: 6.5%;
                }
            }
           
        }

        footer {
            margin-left: 10%;
            button {
                position: absolute
                margin-left: 10%;
                margin-top: 5%;
                margin-bottom : 20%
                margin: 1rem;
                background-color: #155de2;
                color: #fffdff;
                border: none;
                width: 120px;
                font-family: ${defaultFont};
                font-size: 15px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: normal;
                letter-spacing: 0.375px;
                padding: 10px 20px;
                border-radius: 3rem;
                border: none;
                padding: 0.8rem 1.2rem;
                letter-spacing: 0.06em;
                box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
                -webkit-transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
                transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
            }
            span {
                position: absolute;
                margin-left: 16%;
                margin-top: 1.5%;
                font-family: ${defaultFont};
                font-size: 12px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: normal;
                letter-spacing: 0.300000011920929px;
                color: #464646
            }
    }`


export {
    LeftCol,
    RightCol
}


