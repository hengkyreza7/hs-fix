import styled from "styled-components";


const defaultFont = 'NunitoSans';
const whiteColor = '#ffffff';

const Navbar = styled.div`
    div {
        height: 6.333em;
        background-color: ${whiteColor}

        img {
            width: 15em;
            height: 2.833em;
            margin-left : 3.25em;
            margin-top: 1.5em;
           
        }
    }
`

const Main = styled.div`
    p {
        margin-left : 287.5px;
        margin-top: 1.583em;
        height: 2.833em;
        font-family: ${defaultFont};
        font-size: 2.083em;
        font-weight: bold;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: -0.058em;
        color: #233539;
    }

    div {
        
        width:634.5px;
        height:682.5px;
        margin-left : 287.5px;
        margin-right: 287.5px;
        margin-top:30.5px;
        box-shadow: 0 2px 9px 0 rgba(0, 0, 0, 0.03);
        background-color: ${whiteColor};

        form {
            div{
                margin-top :3.6875rem;
                margin-left:29.5px;
                width:575px;
                height:45px;
                background-color: red;
                margin-right:33px;
                margin-bottom:30px;
            }
        }
    }
`
// const Title = styled.div`
//     margin-left :20%;
// `

export {
    // Title,
    Navbar,
    Main
}