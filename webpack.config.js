const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path');
const webpack = require('webpack');
const fs = require('fs');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const API_URL = {
    production: JSON.stringify('https://dev.api.highscope.gredu.co/v1'),
    development: JSON.stringify('https://dev.api.highscope.gredu.co/v1'),
    uat: JSON.stringify('https://uat.api.highscope.gredu.co')
}

const API_URL_LIB = {
    production: JSON.stringify('https://api.lib.highscope.gredu.co'),
    development: JSON.stringify('https://uat.api.lib.highscope.gredu.co'),
    uat: JSON.stringify('https://uat.api.lib.highscope.gredu.co')
}

module.exports = (env, argv) => ({
    devtool: 'source-map',
    bail: true,
    entry: [resolveApp('./src/index.js')],
    output: {
        path: resolveApp('./build'),
        pathinfo: true,
        filename: './static/js/[name].[chunkhash:8].js',
        chunkFilename: './static/js/[name].[chunkhash:8].chunk.js',
        devtoolModuleFilenameTemplate: info =>
            path.resolve(info.absoluteResourcePath),
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader",
                        options: { minimize: true }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, "css-loader"]
            },
            {
                test: /\.(ttf|woff|eot|png|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                loader: 'file-loader'
            }

        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            inject: true,
            template: "./public/index.html",
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeRedundantAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                removeStyleLinkTypeAttributes: true,
                keepClosingSlash: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true,
            },
        }),
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new ManifestPlugin({
            fileName: 'asset-manifest.json',
        }),
        new webpack.DefinePlugin({
            'process.env': {
                'API_URL': API_URL[env.TARGET_ENV],
                'API_URL_LIB': API_URL_LIB[env.TARGET_ENV]
            }
        }),

        new SWPrecacheWebpackPlugin({
            dontCacheBustUrlsMatching: /\.\w{8}\./,
            filename: 'service-worker.js',
            logger(message) {
                if (message.indexOf('Total precache size is') === 0) {
                    return;
                }
                console.log(message);
            },
            minify: true,
            navigateFallbackWhitelist: [/^(?!\/__).*/],
            staticFileGlobsIgnorePatterns: [/\.map$/, /asset-manifest\.json$/],
            stripPrefix: resolveApp('./build').replace(/\\/g, '/') + '/',
        }),
        new CopyWebpackPlugin([
            { from: './public/favicon.ico' },
        ])
    ]

    // optimization: {
    //     minimizer: [
    //         new UglifyJsPlugin({
    //             cache: true,
    //             parallel: true,
    //             uglifyOptions: {
    //                 compress: false,
    //                 ecma: 6,
    //                 mangle: true
    //             },
    //             sourceMap: true
    //         })
    //     ]
    // }
});